package br.com.twg.pep.listener;

public interface SkipMessageEmail<T> {
	
	String messageConstraintEmail(T t,  int numeroLinha);

}
