package br.com.twg.pep.processor;

import org.springframework.batch.core.StepExecution;

import br.com.twg.pep.entity.HistoricoMandato;
import br.com.twg.pep.util.ValidateUtils;

public class HistoricoMandatoProcessor implements PepLayoutProcessor<HistoricoMandato>{

	@Override
	public void processor(HistoricoMandato historico, StepExecution stepExecution) throws Exception{
		
		ValidateUtils.validarPessoaId(historico.getTipoPessoa(), historico.getCpfCnpj(), historico.getDigitoCpfCnpj(), historico.getFilial(), historico.getPessoaId(), stepExecution.getReadCount());
		
		System.out.println("HISTORICO_MANDATO --------> " + historico);		
	}
}