package br.com.twg.pep.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@IdClass(HistoricoMandatoPK.class)
@Table(name="HISTORICO_MANDATO")
public class HistoricoMandato implements Serializable {

	private static final long serialVersionUID = 7766152682824194382L;
	
	@Id
	@Column(name = "PESSOA_ID")
	private Long pessoaId;

	@Id
	@Column(name = "TIPO_PESSOA")
	private String tipoPessoa;

	@Id
	@Column(name = "CPF_CNPJ")
	private String cpfCnpj;

	@Id
	@Column(name = "DIGITO_CPF_CNPJ")
	private String digitoCpfCnpj;

	@Id
	@Column(name = "FILIAL")
	private String filial;	

	@Id
	@Column(name="ORGAO_ID")
	private Long orgaoId;
	
	@Id
	@Column(name="CARGO_ID")
	private Long cargoId;
	
	@Id
	@Column(name="DATA_NOMEACAO")
	private Date dataNomeacao;
	
	@Id
	@Column(name="DATA_CRIACAO")
	private Date dataCriacao;
	
	@Column(name="DATA_EXONERACAO")
	private Date dataExoneracao;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cargoId == null) ? 0 : cargoId.hashCode());
		result = prime * result + ((cpfCnpj == null) ? 0 : cpfCnpj.hashCode());
		result = prime * result
				+ ((dataCriacao == null) ? 0 : dataCriacao.hashCode());
		result = prime * result
				+ ((dataExoneracao == null) ? 0 : dataExoneracao.hashCode());
		result = prime * result
				+ ((dataNomeacao == null) ? 0 : dataNomeacao.hashCode());
		result = prime * result
				+ ((digitoCpfCnpj == null) ? 0 : digitoCpfCnpj.hashCode());
		result = prime * result + ((filial == null) ? 0 : filial.hashCode());
		result = prime * result + ((orgaoId == null) ? 0 : orgaoId.hashCode());
		result = prime * result
				+ ((pessoaId == null) ? 0 : pessoaId.hashCode());
		result = prime * result
				+ ((tipoPessoa == null) ? 0 : tipoPessoa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoMandato other = (HistoricoMandato) obj;
		if (cargoId == null) {
			if (other.cargoId != null)
				return false;
		} else if (!cargoId.equals(other.cargoId))
			return false;
		if (cpfCnpj == null) {
			if (other.cpfCnpj != null)
				return false;
		} else if (!cpfCnpj.equals(other.cpfCnpj))
			return false;
		if (dataCriacao == null) {
			if (other.dataCriacao != null)
				return false;
		} else if (!dataCriacao.equals(other.dataCriacao))
			return false;
		if (dataExoneracao == null) {
			if (other.dataExoneracao != null)
				return false;
		} else if (!dataExoneracao.equals(other.dataExoneracao))
			return false;
		if (dataNomeacao == null) {
			if (other.dataNomeacao != null)
				return false;
		} else if (!dataNomeacao.equals(other.dataNomeacao))
			return false;
		if (digitoCpfCnpj == null) {
			if (other.digitoCpfCnpj != null)
				return false;
		} else if (!digitoCpfCnpj.equals(other.digitoCpfCnpj))
			return false;
		if (filial == null) {
			if (other.filial != null)
				return false;
		} else if (!filial.equals(other.filial))
			return false;
		if (orgaoId == null) {
			if (other.orgaoId != null)
				return false;
		} else if (!orgaoId.equals(other.orgaoId))
			return false;
		if (pessoaId == null) {
			if (other.pessoaId != null)
				return false;
		} else if (!pessoaId.equals(other.pessoaId))
			return false;
		if (tipoPessoa == null) {
			if (other.tipoPessoa != null)
				return false;
		} else if (!tipoPessoa.equals(other.tipoPessoa))
			return false;
		return true;
	}

	public Long getPessoaId() {
		return pessoaId;
	}

	public void setPessoaId(Long pessoaId) {
		this.pessoaId = pessoaId;
	}

	public String getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getDigitoCpfCnpj() {
		return digitoCpfCnpj;
	}

	public void setDigitoCpfCnpj(String digitoCpfCnpj) {
		this.digitoCpfCnpj = digitoCpfCnpj;
	}

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

	public Long getOrgaoId() {
		return orgaoId;
	}

	public void setOrgaoId(Long orgaoId) {
		this.orgaoId = orgaoId;
	}

	public Long getCargoId() {
		return cargoId;
	}

	public void setCargoId(Long cargoId) {
		this.cargoId = cargoId;
	}

	public Date getDataNomeacao() {
		return dataNomeacao;
	}

	public void setDataNomeacao(Date dataNomeacao) {
		this.dataNomeacao = dataNomeacao;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataExoneracao() {
		return dataExoneracao;
	}

	public void setDataExoneracao(Date dataExoneracao) {
		this.dataExoneracao = dataExoneracao;
	}

	@Override
	public String toString() {
		return "HistoricoMandato [pessoaId=" + pessoaId + ", tipoPessoa="
				+ tipoPessoa + ", cpfCnpj=" + cpfCnpj + ", digitoCpfCnpj="
				+ digitoCpfCnpj + ", filial=" + filial + ", orgaoId=" + orgaoId
				+ ", cargoId=" + cargoId + ", dataNomeacao=" + dataNomeacao
				+ ", dataCriacao=" + dataCriacao + ", dataExoneracao="
				+ dataExoneracao + "]";
	}
	
}