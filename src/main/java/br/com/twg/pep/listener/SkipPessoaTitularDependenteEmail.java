package br.com.twg.pep.listener;

import org.springframework.stereotype.Component;

import br.com.twg.pep.dao.PessoaDAO;
import br.com.twg.pep.dao.PessoaTitularDependenteDAO;
import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.entity.PessoaPK;
import br.com.twg.pep.entity.PessoaTitularDependente;
import br.com.twg.pep.entity.PessoaTitularDependentePK;
@Component
public class SkipPessoaTitularDependenteEmail implements SkipMessageEmail<PessoaTitularDependente>{

	private PessoaDAO pessoaDAO;
	private PessoaTitularDependenteDAO pessoaTitularDependenteDAO;
	
	SkipPessoaTitularDependenteEmail(){}
	
	SkipPessoaTitularDependenteEmail(PessoaDAO pessoaDAO, PessoaTitularDependenteDAO pessoaTitularDependenteDAO){
		this.pessoaDAO = pessoaDAO;
		this.pessoaTitularDependenteDAO = pessoaTitularDependenteDAO;
	}
	
	@Override
	public String messageConstraintEmail(PessoaTitularDependente pessoaTitularDependente, int numeroLinha) {
			
		StringBuilder sbMsg = new StringBuilder();
		
		PessoaPK pessoaTitularPk = new PessoaPK();
		pessoaTitularPk.setCpfCnpj(pessoaTitularDependente.getPessoaTitular().getCpfCnpj());
		pessoaTitularPk.setDigitoCpfCnpj(pessoaTitularDependente.getPessoaTitular().getDigitoCpfCnpj());
		pessoaTitularPk.setFilial(pessoaTitularDependente.getPessoaTitular().getFilial());
		pessoaTitularPk.setId(pessoaTitularDependente.getPessoaTitular().getId());
		pessoaTitularPk.setTipoPessoa(pessoaTitularDependente.getPessoaTitular().getTipoPessoa());
		
		PessoaPK pessoaDependentePk = new PessoaPK();
		pessoaDependentePk.setCpfCnpj(pessoaTitularDependente.getPessoaDependente().getCpfCnpj());
		pessoaDependentePk.setDigitoCpfCnpj(pessoaTitularDependente.getPessoaDependente().getDigitoCpfCnpj());
		pessoaDependentePk.setFilial(pessoaTitularDependente.getPessoaDependente().getFilial());
		pessoaDependentePk.setId(pessoaTitularDependente.getPessoaDependente().getId());
		pessoaDependentePk.setTipoPessoa(pessoaTitularDependente.getPessoaDependente().getTipoPessoa());
		
		Pessoa pessoaTitular = pessoaDAO.buscaPessoasPorId(pessoaTitularPk);
		Pessoa pessoaDependente = null;
		PessoaTitularDependentePK pessoaTitularDependentePk = null;
		
		if(pessoaTitular == null){
			sbMsg.append("ERRO NA LINHA " + numeroLinha + " DO ARQUIVO Pep.txt \n" )
			.append("TITULAR - Erro no tipo de registro 140. Segue as informa��es abaixo: \n")
			.append("IDENTIFICADOR: " +pessoaTitularDependente.getPessoaTitular().getId()+ " \n")
			.append("CPF/CNPJ: " +pessoaTitularDependente.getPessoaTitular().getCpfCnpj()+ " \n")
			.append("FILIAL: " +pessoaTitularDependente.getPessoaTitular().getFilial()+ " \n")
			.append("DIGITO CPF/CNPJ: " +pessoaTitularDependente.getPessoaTitular().getDigitoCpfCnpj() + " \n")
			.append("TIPO PESSOA: " +pessoaTitularDependente.getPessoaTitular().getTipoPessoa())
			.append(". \nA pessoa com os dados acima n�o existe!");
		}else 
			pessoaDependente = pessoaDAO.buscaPessoasPorId(pessoaDependentePk);
			if(pessoaDependente == null){
				sbMsg.append("ERRO NA LINHA " + numeroLinha  + " DO ARQUIVO Pep.txt \n" )
				.append("DEPENDENTE - Erro no tipo de registro 140. Segue as informa��es abaixo: \n")
				.append("IDENTIFICADOR: " +pessoaTitularDependente.getPessoaDependente().getId()+ " \n")
				.append("CPF/CNPJ: " +pessoaTitularDependente.getPessoaDependente().getCpfCnpj()+ " \n")
				.append("FILIAL: " +pessoaTitularDependente.getPessoaDependente().getFilial()+ " \n")
				.append("DIGITO CPF/CNPJ: " +pessoaTitularDependente.getPessoaDependente().getDigitoCpfCnpj() + " \n")
				.append("TIPO PESSOA: " +pessoaTitularDependente.getPessoaDependente().getTipoPessoa() )
				.append(". \nA pessoa com os dados acima n�o existe!");
		}else {
			pessoaTitularDependentePk = new PessoaTitularDependentePK();
			pessoaTitularDependentePk.setPessoaDependente(pessoaTitular);
			pessoaTitularDependentePk.setPessoaDependente(pessoaDependente);
			pessoaTitularDependente = pessoaTitularDependenteDAO.buscaPessoaTitularDependentePorId(pessoaTitularDependentePk);
			}if(pessoaTitularDependentePk != null){
				sbMsg.append("DEPENDENTE - Erro no tipo de registro 140. Segue as informa��es abaixo: \n")
				.append("IDENTIFICADOR: " +pessoaTitularDependente.getPessoaDependente().getId()+ " \n")
				.append("CPF/CNPJ: " +pessoaTitularDependente.getPessoaDependente().getCpfCnpj()+ " \n")
				.append("FILIAL: " +pessoaTitularDependente.getPessoaDependente().getFilial()+ " \n")
				.append("DIGITO CPF/CNPJ: " +pessoaTitularDependente.getPessoaDependente().getDigitoCpfCnpj() + " \n")
				.append("TIPO PESSOA: " +pessoaTitularDependente.getPessoaDependente().getTipoPessoa() )
				.append(". \nO Registro j� foi cadastrado!");
				
			}
			return sbMsg.toString();
	}
}