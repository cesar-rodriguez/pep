package br.com.twg.pep.dao;

import java.util.Date;
import java.util.List;

import br.com.twg.pep.entity.HistoricoPessoa;
import br.com.twg.pep.entity.HistoricoProdutoSegurado;

public interface HistoricoProdutoSeguradoDAO {

	List<HistoricoProdutoSegurado> buscaProdutoSeguradoPor(HistoricoPessoa pessoa, Date dataCriacao);	
}
