package br.com.twg.pep.dao;

import java.util.Date;
import java.util.List;

import br.com.twg.pep.entity.HistoricoMandato;
import br.com.twg.pep.entity.HistoricoPessoa;
import br.com.twg.pep.entity.Pessoa;

public interface HistoricoMandatoDAO {
	
	void removeHistoricoMandato();
	List<HistoricoMandato> buscaHistoricoMandatoPor(Pessoa pessoa);
	List<HistoricoMandato> buscaHistoricoMandatoPor(HistoricoPessoa pessoa,Date dataCriacao);

}
