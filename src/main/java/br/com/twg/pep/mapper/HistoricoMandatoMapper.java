package br.com.twg.pep.mapper;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import br.com.twg.pep.entity.HistoricoMandato;

public class HistoricoMandatoMapper implements FieldSetMapper<HistoricoMandato> {

	private Date startTime;
	
	@Override
	public HistoricoMandato mapFieldSet(FieldSet fieldSet) throws BindException {
		HistoricoMandato historicoMandato = new HistoricoMandato ();
		//PESSOA
		historicoMandato.setTipoPessoa(fieldSet.readString("TIPO_PES"));
		historicoMandato.setCpfCnpj(fieldSet.readString("CPF_CNPJ"));
		historicoMandato.setFilial(fieldSet.readString("FILIAL"));
		historicoMandato.setDigitoCpfCnpj(fieldSet.readString("DIGITO"));
		historicoMandato.setPessoaId(fieldSet.readLong("ID_PESSOA"));
	
		//CARGO
		historicoMandato.setCargoId(fieldSet.readLong("CARGO_ID"));
		
		//ORGAO
		historicoMandato.setOrgaoId(fieldSet.readLong("ORGAO_ID"));
		
		String dataNomeacao = fieldSet.readString("DT_NOMEACAO");
		String dataExoneracao = fieldSet.readString("DT_EXONERACAO");
		
		historicoMandato.setDataNomeacao(dataNomeacao.equals("")? null: fieldSet.readDate("DT_NOMEACAO", "dd/MM/yyyy"));
		historicoMandato.setDataExoneracao(dataExoneracao.equals("")? null: fieldSet.readDate("DT_EXONERACAO", "dd/MM/yyyy"));
		historicoMandato.setDataCriacao(startTime);
		return historicoMandato;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

}
