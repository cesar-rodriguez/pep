package br.com.twg.pep.dao;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.orm.jpa.EntityManagerFactoryUtils;

import br.com.twg.pep.entity.HistoricoCargo;
import br.com.twg.pep.util.DateUtil;

public class HistoricoCargoDAOImpl implements HistoricoCargoDAO{
	
	@PersistenceUnit(unitName="PEPPU")
	private EntityManagerFactory entityManagerFactory;
	
	@Override
	public HistoricoCargo buscaCargoPorId(Long id, Date dataCriacao){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createQuery("select c from HistoricoCargo c where c.id = :id and year(c.dataCriacao) = :ano and month(c.dataCriacao) = :mes "); //colocar a data de criação
        query.setParameter("id", id);
        query.setParameter("ano", DateUtil.ano(dataCriacao));
        query.setParameter("mes", DateUtil.mes(dataCriacao));
        try{
        	return (HistoricoCargo) query.getSingleResult();
        }catch(NoResultException nre){
        	return null;
        }
	}
	
}