package br.com.twg.pep.dao;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.orm.jpa.EntityManagerFactoryUtils;

import br.com.twg.pep.entity.Orgao;
import br.com.twg.pep.util.DateUtil;

public class OrgaoDAOImpl implements OrgaoDAO {

	@PersistenceUnit(unitName="PEPPU")
	private EntityManagerFactory entityManagerFactory;
	
	@Override
	public Orgao buscaOrgaoPorId(Long id){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
		return entityManager.find(Orgao.class, id);
	}
	
	public void removeOrgaos(){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		estruturaUpdate("DELETE FROM "+schema+".ORGAO");
	}
	
	public void recuperaDadosHistoricos(){
		estruturaUpdate(queryRecuperaHistorico());
	}
	
	public void insereDadosHistoricos(Date dataHistorico){
		estruturaUpdate(queryInsereHistorico(dataHistorico));
	}
	
	private String queryInsereHistorico(Date dataHistorico){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(
				" INSERT INTO "+schema+".HISTORICO_ORGAO(ID, DATA_CRIACAO, NOME, LOGRADOURO, NUMERO, COMPLEMENTO, BAIRRO, CEP, MUNICIPIO, UF, DDD, TELEFONE, RAMAL, EMAIL, ORGAO_SUPERIOR_ID, SIGLA_ORGAO_SUPERIOR, CNPJ, DATA_EXTINCAO) ")
				.append(" SELECT ID, ")
				.append("'" + DateUtil.formataDataHistorico(dataHistorico) +"'" + ", ") 
				.append(" NOME, LOGRADOURO, NUMERO, COMPLEMENTO, BAIRRO, CEP, MUNICIPIO, UF, DDD, TELEFONE, RAMAL, EMAIL, ORGAO_SUPERIOR_ID, SIGLA_ORGAO_SUPERIOR, CNPJ, DATA_EXTINCAO ")
				.append(" FROM "+schema+".ORGAO ");

		return sbQuery.toString();
	}
	
	private String queryRecuperaHistorico(){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		StringBuilder sbInsert = new StringBuilder();
		
		sbInsert.append(" INSERT INTO "+schema+".ORGAO(ID, NOME,  LOGRADOURO, NUMERO, COMPLEMENTO, BAIRRO, CEP, MUNICIPIO, UF, DDD, TELEFONE, RAMAL, EMAIL, ORGAO_SUPERIOR_ID, SIGLA_ORGAO_SUPERIOR, CNPJ, DATA_EXTINCAO) ")
	    .append(" SELECT ID, NOME,  LOGRADOURO, NUMERO, COMPLEMENTO, BAIRRO, CEP, MUNICIPIO, UF, DDD, TELEFONE, RAMAL, EMAIL, ORGAO_SUPERIOR_ID, SIGLA_ORGAO_SUPERIOR, CNPJ, DATA_EXTINCAO ")
	    .append("  FROM "+schema+".HISTORICO_ORGAO ")
	    .append("  WHERE DATA_CRIACAO = (SELECT MAX(DATA_CRIACAO) FROM "+schema+".HISTORICO_ORGAO) ");
		
		return sbInsert.toString();
	}
	
	private void estruturaUpdate(String strUpdate){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createNativeQuery(strUpdate);
    	query.executeUpdate();	
	}
 }