package br.com.twg.pep.processor;

import org.springframework.batch.core.StepExecution;

import br.com.twg.pep.entity.PessoaTitularDependente;
import br.com.twg.pep.util.ValidateUtils;

public class PessoaTitularDependenteProcessor implements PepLayoutProcessor<PessoaTitularDependente>{

	@Override
	public void processor(PessoaTitularDependente pessoaTitularDependente, StepExecution stepExecution) throws Exception {
		
		ValidateUtils.validarPessoaId(pessoaTitularDependente.getPessoaDependente().getTipoPessoa(), 
									  pessoaTitularDependente.getPessoaDependente().getCpfCnpj(), 
									  pessoaTitularDependente.getPessoaDependente().getDigitoCpfCnpj(), 
									  pessoaTitularDependente.getPessoaDependente().getFilial(), 
									  pessoaTitularDependente.getPessoaDependente().getId(), 
									  stepExecution.getReadCount());
		ValidateUtils.validarPessoaId(pessoaTitularDependente.getPessoaTitular().getTipoPessoa(), 
									  pessoaTitularDependente.getPessoaTitular().getCpfCnpj(), 
									  pessoaTitularDependente.getPessoaTitular().getDigitoCpfCnpj(), 
									  pessoaTitularDependente.getPessoaTitular().getFilial(), 
									  pessoaTitularDependente.getPessoaTitular().getId(), 
									  stepExecution.getReadCount());
		
		System.out.println("PESSOA_TITULA_DEPENDENTE -> " + pessoaTitularDependente);
		
	}
}