package br.com.twg.pep.listener;

import java.io.IOException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.listener.SkipListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.twg.pep.dao.CargoDAO;
import br.com.twg.pep.dao.OrgaoDAO;
import br.com.twg.pep.dao.PessoaDAO;
import br.com.twg.pep.dao.PessoaTitularDependenteDAO;
import br.com.twg.pep.entity.PessoaTitularDependente;
import br.com.twg.pep.util.EmailUtil;
import br.com.twg.pep.util.PropertiesUtils;
import br.twg.bellmail.util.MailUtil;

public class SkipListener<T, S> extends SkipListenerSupport<T, S> {

	private StepExecution stepExecution; 
	
	@Autowired
	private PessoaDAO pessoaDAO;
	@Autowired
	private PessoaTitularDependenteDAO pessoaTitularDependenteDAO;
	@Autowired
	private CargoDAO cargoDAO;
	@Autowired
	private OrgaoDAO orgaoDAO;
	
	@Override
	public void onSkipInWrite(S item, Throwable throwable) {
		String strMessage = null;
		if(item instanceof PessoaTitularDependente){
			SkipPessoaTitularDependenteEmail sptde = new SkipPessoaTitularDependenteEmail(pessoaDAO, pessoaTitularDependenteDAO);
			strMessage = sptde.messageConstraintEmail((PessoaTitularDependente) item, stepExecution.getReadCount());
		}
		
		if (throwable.getCause() instanceof ConstraintViolationException) {
			String emailTitle = getEmailTitle(throwable);
			
			try {
				MailUtil.enviaEmail(emailTitle, EmailUtil.emailsTo(), EmailUtil.emailsFrom(), strMessage);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private String getEmailTitle(Throwable throwable) {
		try{	
			return PropertiesUtils.getProperty("pep.titulo.erro");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@BeforeStep
    public void retrieveInterstepData(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
    }
}