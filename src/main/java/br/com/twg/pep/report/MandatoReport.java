package br.com.twg.pep.report;

public class MandatoReport {
	
	private String dataNomeacao;
	private String dataExoneracao;
	private String codigoOrgao;
	private String codigoCargo;
	private String descricaoCargo;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((codigoCargo == null) ? 0 : codigoCargo.hashCode());
		result = prime * result
				+ ((codigoOrgao == null) ? 0 : codigoOrgao.hashCode());
		result = prime * result
				+ ((dataExoneracao == null) ? 0 : dataExoneracao.hashCode());
		result = prime * result
				+ ((dataNomeacao == null) ? 0 : dataNomeacao.hashCode());
		result = prime * result
				+ ((descricaoCargo == null) ? 0 : descricaoCargo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MandatoReport other = (MandatoReport) obj;
		if (codigoCargo == null) {
			if (other.codigoCargo != null)
				return false;
		} else if (!codigoCargo.equals(other.codigoCargo))
			return false;
		if (codigoOrgao == null) {
			if (other.codigoOrgao != null)
				return false;
		} else if (!codigoOrgao.equals(other.codigoOrgao))
			return false;
		if (dataExoneracao == null) {
			if (other.dataExoneracao != null)
				return false;
		} else if (!dataExoneracao.equals(other.dataExoneracao))
			return false;
		if (dataNomeacao == null) {
			if (other.dataNomeacao != null)
				return false;
		} else if (!dataNomeacao.equals(other.dataNomeacao))
			return false;
		if (descricaoCargo == null) {
			if (other.descricaoCargo != null)
				return false;
		} else if (!descricaoCargo.equals(other.descricaoCargo))
			return false;
		return true;
	}
	public String getDataNomeacao() {
		return dataNomeacao;
	}
	public void setDataNomeacao(String dataNomeacao) {
		this.dataNomeacao = dataNomeacao;
	}
	public String getDataExoneracao() {
		return dataExoneracao;
	}
	public void setDataExoneracao(String dataExoneracao) {
		this.dataExoneracao = dataExoneracao;
	}
	
	public String getCodigoOrgao() {
		return codigoOrgao;
	}
	public void setCodigoOrgao(String codigoOrgao) {
		this.codigoOrgao = codigoOrgao;
	}
	public String getCodigoCargo() {
		return codigoCargo;
	}
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}

	public String getDescricaoCargo() {
		return descricaoCargo;
	}
	public void setDescricaoCargo(String descricaoCargo) {
		this.descricaoCargo = descricaoCargo;
	}
	@Override
	public String toString() {
		return "MandatoReport [dataNomeacao=" + dataNomeacao
				+ ", dataExoneracao=" + dataExoneracao + ", codigoOrgao="
				+ codigoOrgao + ", codigoCargo=" + codigoCargo
				+ ", decricaoCargo=" + descricaoCargo + "]";
	}
	
	
}