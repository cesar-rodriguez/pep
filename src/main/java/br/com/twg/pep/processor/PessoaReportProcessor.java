package br.com.twg.pep.processor;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.twg.pep.dao.CancelamentoDAO;
import br.com.twg.pep.dao.CargoDAO;
import br.com.twg.pep.dao.HistoricoMandatoDAO;
import br.com.twg.pep.dao.ProdutoSeguradoDAO;
import br.com.twg.pep.dao.SinistroDAO;
import br.com.twg.pep.entity.Cancelamento;
import br.com.twg.pep.entity.Cargo;
import br.com.twg.pep.entity.HistoricoMandato;
import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.entity.ProdutoSegurado;
import br.com.twg.pep.entity.Sinistro;
import br.com.twg.pep.exception.PEPException;
import br.com.twg.pep.report.CancelamentoReport;
import br.com.twg.pep.report.MandatoReport;
import br.com.twg.pep.report.PessoaReport;
import br.com.twg.pep.report.ProdutoSeguradoReport;
import br.com.twg.pep.report.SinistroReport;

public class PessoaReportProcessor implements ItemProcessor<Pessoa, PessoaReport> {

	private static Locale ptBR = new Locale("pt", "BR");
	private static  SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
	private static DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(ptBR));
	
	@Autowired
	private HistoricoMandatoDAO historicoMandatoDao;
	
	@Autowired
	private ProdutoSeguradoDAO produtoDao;
	
	@Autowired
	private CancelamentoDAO cancelamentoDao;
	
	@Autowired
	private SinistroDAO sinistroDao;
	
	@Autowired
	private CargoDAO cargoDao;
	
	@Override
	public PessoaReport process(Pessoa pessoa) throws Exception {
		
		PessoaReport pessoaReport = createPessoaReport(pessoa);
		setMandatos(pessoaReport,  pessoa) ;
		List<ProdutoSegurado> produtos = produtoDao.buscaProdutoSeguradoPor(pessoa);
		for(ProdutoSegurado produto: produtos){
			ProdutoSeguradoReport produtoReport = createProdutosSeguradoReport(produto);
			Cancelamento cancelamento =  cancelamentoDao.buscaCancelamentoPor(produto);
			setCancelamento(produtoReport, cancelamento);
			List<Sinistro> sinistros = sinistroDao.buscaSinistroPor(produto);
			setSinistrosReport(produtoReport, sinistros);
			pessoaReport.getProdutos().add(produtoReport);
			
			System.out.println("PRODUTO ------------------> " + produtoReport);
		}
		
		System.out.println("PESSOA_REPORT ------------> " + pessoaReport);
		
		return pessoaReport;
		
	}
	
	private void setCancelamento(ProdutoSeguradoReport produtoReport, Cancelamento cancelamento){
		if (cancelamento != null) {
			CancelamentoReport cancelamentoReport = new CancelamentoReport();
			cancelamentoReport.setDataPagamento(cancelamento.getDataPagamento() == null? "": sdf.format(cancelamento.getDataPagamento()));
			cancelamentoReport.setDataVencimento(cancelamento.getDataVencimento() == null? "": sdf.format(cancelamento.getDataVencimento()));
			cancelamentoReport.setRazaoCancelamento(cancelamento.getRazaoCancelamento() == null? "": cancelamento.getRazaoCancelamento());
			cancelamentoReport.setValorReembolsado(cancelamento.getValorReembolsado() == null? "": df.format(cancelamento.getValorReembolsado()));
			produtoReport.setCancelamento(cancelamentoReport);
		}
	}
	
	private void setMandatos(PessoaReport pessoaReport, Pessoa pessoa) throws PEPException{
		
			List<HistoricoMandato> historicoMandatos = historicoMandatoDao.buscaHistoricoMandatoPor(pessoa);
		
			if(historicoMandatos != null && !historicoMandatos.isEmpty()){
				
				List<MandatoReport> mandatosReport = new ArrayList<MandatoReport>(historicoMandatos.size());
				for(HistoricoMandato historicoMandato: historicoMandatos){
					MandatoReport mandatoReport = new MandatoReport();
					mandatoReport.setCodigoCargo(historicoMandato.getCargoId() 			 == null? "": historicoMandato.getCargoId().toString());
					mandatoReport.setCodigoOrgao(historicoMandato.getOrgaoId() 			 == null? "": historicoMandato.getOrgaoId().toString());
					mandatoReport.setDataNomeacao(historicoMandato.getDataNomeacao() 	 == null? "": sdf.format(historicoMandato.getDataNomeacao()));
					mandatoReport.setDataExoneracao(historicoMandato.getDataExoneracao() == null? "": sdf.format(historicoMandato.getDataExoneracao()));
					Cargo cargo = null;
					if(historicoMandato.getCargoId() != null){
						cargo = cargoDao.buscaCargoPorId(historicoMandato.getCargoId());
					}
					mandatoReport.setDescricaoCargo(cargo == null? "": cargo.getNome());
					
					mandatosReport.add(mandatoReport);
				}
				pessoaReport.setMandatos(mandatosReport);
			}
			
	}
	
	private void setSinistrosReport(ProdutoSeguradoReport produtoReport, List<Sinistro> sinistros){
		
		if(sinistros != null && !sinistros.isEmpty()){
			
			List<SinistroReport> sinistrosReport = new ArrayList<SinistroReport>(sinistros.size());
			for(Sinistro sinistro: sinistros){
				SinistroReport sinistroReport = new SinistroReport();
				sinistroReport.setDataEntrada(sinistro.getDataEntrada() == null? "": sdf.format(sinistro.getDataEntrada()));
				sinistroReport.setNumeroSinistro(sinistro.getNumeroSinistro() == null? "": sinistro.getNumeroSinistro().toString());
				sinistroReport.setReembolsado(sinistro.getReembolsado() == null? "": sinistro.getReembolsado());
				sinistroReport.setStatus(sinistro.getStatus() == null? "": sinistro.getStatus().getDescricao());
				sinistroReport.setValorTotal(sinistro.getValorTotal() == null? "": df.format(sinistro.getValorTotal()));
				sinistroReport.setValorReembolsado(sinistro.getValorReembolsado() == null? "": df.format(sinistro.getValorReembolsado()));
				sinistrosReport.add(sinistroReport);
			}
			produtoReport.setSinistros(sinistrosReport);
		}
	}
	
	private ProdutoSeguradoReport createProdutosSeguradoReport(ProdutoSegurado produto){
		
		ProdutoSeguradoReport produtoReport = new ProdutoSeguradoReport();
		produtoReport.setStatus(produto.getStatus() 					  == null? "": produto.getStatus());
		produtoReport.setNumeroCertificado(produto.getNumeroCertificado() == null? "": produto.getNumeroCertificado());
		produtoReport.setServicoContratado(produto.getServicoContratado() == null? "": produto.getServicoContratado());
		produtoReport.setValorPremio(produto.getValorPremio() 			  == null? "" :df.format(produto.getValorPremio()));
		produtoReport.setValorProduto(produto.getValorProduto() 		  == null? "": df.format(produto.getValorProduto()));
		produtoReport.setDataEmissao(produto.getDataEmissao() 			  == null? "": sdf.format(produto.getDataEmissao()));
		produtoReport.setInicioVigencia(produto.getInicioVigencia() 	  == null? "": sdf.format(produto.getInicioVigencia()));
		produtoReport.setFimVigencia(produto.getFimVigencia()			  == null? "": sdf.format(produto.getFimVigencia()));
		produtoReport.setFabricante(produto.getFabricante() 			  == null? "": produto.getFabricante());
		produtoReport.setModelo(produto.getModelo() 					  == null? "": produto.getModelo());
		produtoReport.setModeloDealer(produto.getModeloDealer()     	  == null? "": produto.getModeloDealer());
		return produtoReport;
	}
	
	private PessoaReport createPessoaReport(Pessoa pessoa){
		
		PessoaReport pessoaReport = new PessoaReport();
		
		pessoaReport.setNome(pessoa.getNome() == null? "": pessoa.getNome());
		pessoaReport.setCpfCnpj(pessoa.getCpfCnpj() == null? "": pessoa.getCpfCnpj() + "-" + pessoa.getDigitoCpfCnpj());
		pessoaReport.setTipoPessoa(pessoa.getTipoPessoa() == null? "": pessoa.getTitularidade());
		
		//TODO precisa inserir os codigos aqui
		pessoaReport.setCodigoOrgao("");
		pessoaReport.setCodigoCargo("");
		
		
		if(pessoa.getEndereco() != null){
			String logradouro = pessoa.getEndereco().getLogradouro();
			Long numeroEndereco = pessoa.getEndereco().getNumero();
			String complemento = pessoa.getEndereco().getComplemento();
			String bairro = pessoa.getEndereco().getBairro();
			String municipio = pessoa.getEndereco().getMunicipio();
			String uf = pessoa.getEndereco().getUf();
			String cep = pessoa.getEndereco().getCep();
			pessoaReport.setLogradouro(logradouro == null? "": logradouro);
			pessoaReport.setNumero(numeroEndereco == null? "": numeroEndereco.toString());
			pessoaReport.setComplemento(complemento == null? "": complemento);
			pessoaReport.setBairro(bairro == null? "": bairro);
			pessoaReport.setMunicipio(municipio == null? "": municipio);
			pessoaReport.setUf(uf == null? "": uf);
			pessoaReport.setCep(cep == null? "": cep);
		}
		
		
		if(pessoa.getContato() != null){
			Integer ddd = pessoa.getContato().getTelefone().getDdd();
			String numeroTelefone  = pessoa.getContato().getTelefone().getNumero();
			Long ramal = pessoa.getContato().getTelefone().getRamal();
			pessoaReport.setDdd(ddd == null? "": ddd.toString());
			pessoaReport.setTelefone(numeroTelefone == null? "": numeroTelefone);	
			pessoaReport.setRamal(ramal == null? "": ramal.toString());
		}
		
		
		pessoaReport.setProdutos(new ArrayList<ProdutoSeguradoReport>());

		return pessoaReport;
	}
}