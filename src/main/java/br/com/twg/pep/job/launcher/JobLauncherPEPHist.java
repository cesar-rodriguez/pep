package br.com.twg.pep.job.launcher;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.twg.pep.util.DateUtil;

@Component
public class JobLauncherPEPHist{

	private static Log log = LogFactory.getLog(JobLauncherPEPHist.class);

	@Autowired
    private Job job;

	@Autowired
	private JobLauncher jobLauncher;

	public void executeInternal(Date dataCriacao) {
		
		JobParameters jobParameters = getJobParametersFromJobMap(dataCriacao);
		try {
			jobLauncher.run(job, jobParameters);
		}
		catch (JobExecutionException e) {
			log.error("Could not execute job.", e);
		}
		
	}
	
	private JobParameters getJobParametersFromJobMap(Date dataCriacao) {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addDate("initJob", Calendar.getInstance().getTime());
		builder.addString("strDataCriacao", DateUtil.formataDataHistorico(dataCriacao));
		builder.addDate("dataCriacao", dataCriacao);
		builder.addDate("ultimoDiaMes", DateUtil.ultimoDiaMes(dataCriacao));
		builder.addLong("ano", DateUtil.ano(dataCriacao).longValue());
		builder.addLong("mes", DateUtil.mes(dataCriacao).longValue());

		return builder.toJobParameters();
	}
}