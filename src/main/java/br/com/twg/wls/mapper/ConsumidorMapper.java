package br.com.twg.wls.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import br.com.twg.wls.dto.Consumidor;

public class ConsumidorMapper implements FieldSetMapper<Consumidor> {


	public Consumidor mapRow(ResultSet fieldSet, int arg1) throws SQLException {
		
		Consumidor consumidor = new Consumidor();
		
		consumidor.setId(fieldSet.getLong("CUS_NBR"));  
		consumidor.setNome(fieldSet.getString("NOME"));
		consumidor.setNumeroContrato(fieldSet.getString("NUMERO_CONTRATO"));
		consumidor.setRamo(fieldSet.getString("COD_RAMO"));
		return consumidor;
	}

	@Override
	public Consumidor mapFieldSet(FieldSet fieldSet) throws BindException {
		Consumidor consumidor = new Consumidor();
		
		consumidor.setId(fieldSet.readLong("id"));  
		consumidor.setNome(fieldSet.readString("nome"));
		consumidor.setNumeroContrato(fieldSet.readString("numeroContrato"));
		consumidor.setRamo(fieldSet.readString("ramo"));
		return consumidor;
	}

}
