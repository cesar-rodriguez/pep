package br.com.twg.pep.web.controler;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.twg.pep.job.launcher.JobLauncherPEPHist;

@Controller
@RequestMapping("/historico")
public class HistoricoPepController {
	
	@Autowired
	private JobLauncherPEPHist jobLauncherPEPHist;
	
	@RequestMapping(value = "/{name}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody String getMovie(@PathVariable String name, ModelMap model) {
		Calendar cal = Calendar.getInstance();
		cal.set(2015, 2, 6, 3, 0);
		jobLauncherPEPHist.executeInternal(cal.getTime());
//		model.addAttribute("movie", name);
		return "JobIniciado com sucesso para o periodo de:" + cal.getTime();
	}
	
//	@RequestMapping(value = "/", method = RequestMethod.GET)
//	public String getDefaultMovie(ModelMap model) {
//
//		model.addAttribute("movie", "this is default movie");
//		return "list";
//
//	}
}