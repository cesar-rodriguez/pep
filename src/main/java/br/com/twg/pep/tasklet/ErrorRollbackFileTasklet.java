package br.com.twg.pep.tasklet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.twg.pep.dao.CargoDAO;
import br.com.twg.pep.dao.HistoricoMandatoDAO;
import br.com.twg.pep.dao.OrgaoDAO;
import br.com.twg.pep.dao.PessoaDAO;
import br.com.twg.pep.dao.PessoaTitularDependenteDAO;

public class ErrorRollbackFileTasklet implements Tasklet {

	protected final Log logger = LogFactory.getLog(getClass());


	@Autowired
	private OrgaoDAO orgaoDao;

	@Autowired
	private CargoDAO cargoDao;
	
	@Autowired
	private PessoaDAO pessoaDao;
	
	@Autowired
	private PessoaTitularDependenteDAO pessoaTitularDependenteDao;
	
	@Autowired
	private HistoricoMandatoDAO hidtoricoMandatoDao;


	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

		hidtoricoMandatoDao.removeHistoricoMandato();
		orgaoDao.removeOrgaos();
		cargoDao.removeCargos();
		pessoaTitularDependenteDao.removePessoas();
		pessoaDao.removePessoas();

		orgaoDao.recuperaDadosHistoricos();
		cargoDao.recuperaDadosHistoricos();
		pessoaDao.recuperaDadosHistoricos();
		pessoaTitularDependenteDao.recuperaDadosHistoricos();
	
		return RepeatStatus.FINISHED;
	}	
}