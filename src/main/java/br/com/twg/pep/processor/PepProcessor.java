package br.com.twg.pep.processor;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;

import br.com.twg.pep.processor.factory.ProcessorFactory;

public class PepProcessor<T> implements ItemProcessor<T, T> {
	
	private StepExecution stepExecution;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public T process(T item) throws Exception {
		PepLayoutProcessor pepLayoutProcessor = ProcessorFactory.createInstance(item);
		pepLayoutProcessor.processor(item, stepExecution);
		return item;
	}
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		this.stepExecution = stepExecution;
	}
}