package br.com.twg.pep.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import br.com.twg.pep.entity.Contato;
import br.com.twg.pep.entity.Endereco;
import br.com.twg.pep.entity.Orgao;
import br.com.twg.pep.entity.Telefone;
import br.com.twg.pep.util.DateUtil;

@Component
public class OrgaoMapper implements FieldSetMapper<Orgao> {

	@Override
	public Orgao mapFieldSet(FieldSet fieldSet) throws BindException {
	
		Orgao orgao = new Orgao();
		Contato contato = new Contato();
		Telefone telefone = new Telefone();
		Endereco endereco = new Endereco();
		
		// endereco
		endereco.setLogradouro(fieldSet.readString("ENDERECO"));
		
		String strNumero = fieldSet.readString("NUMERO"); 
		Long numero = null;
		
		if(!strNumero.equals("")){
			try{
			numero = new Long(strNumero);
			}catch(NumberFormatException e){
				e.printStackTrace();
			}
		}
		
		endereco.setNumero(numero);
		endereco.setComplemento(fieldSet.readString("COMPLEMENTO"));
		endereco.setBairro(fieldSet.readString("BAIRRO"));
		endereco.setCep(fieldSet.readString("CEP"));
		endereco.setMunicipio(fieldSet.readString("MUNICIPIO"));
		endereco.setUf(fieldSet.readString("UF"));
		orgao.setEndereco(endereco);
		
		//contato
		telefone.setDdd(fieldSet.readString("DDD").equals("")? null: fieldSet.readInt("DDD"));
		telefone.setNumero(fieldSet.readString("TELEFONE"));
		telefone.setRamal(fieldSet.readString("RAMAL").equals("")? null: fieldSet.readLong("RAMAL"));
		contato.setTelefone(telefone);
		contato.setEmail(fieldSet.readString("EMAIL"));
		orgao.setContato(contato);
		
		orgao.setCnpj(fieldSet.readString("CNPJ"));
		String dataExtincao = fieldSet.readString("DATA_EXTINCAO");
		orgao.setDataExtincao(DateUtil.validaData(dataExtincao)? null: fieldSet.readDate("DATA_EXTINCAO", "dd/MM/yyyy"));
		orgao.setId(fieldSet.readString("ORGAO_ID").equals("")? null: fieldSet.readLong("ORGAO_ID"));
		orgao.setNome(fieldSet.readString("NOME"));
		orgao.setOrgaoSuperiorId(fieldSet.readString("ORGAO_SUPERIOR_ID").equals("")? null: fieldSet.readLong("ORGAO_SUPERIOR_ID"));
		orgao.setSiglaOrgaoSuperior(fieldSet.readString("SIGLA_ORGAO_SUPERIOR"));
		
		System.out.println("ENDERECO -----------------> " + endereco);
		System.out.println("TELEFONE -----------------> " + telefone);
		System.out.println("CONTATO ------------------> " + contato);
		System.out.println("ORGAO --------------------> " + orgao);
		
		return orgao;
	}
}