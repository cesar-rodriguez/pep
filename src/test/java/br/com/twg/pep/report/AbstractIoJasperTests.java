package br.com.twg.pep.report;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.MetaDataInstanceFactory;
import org.springframework.batch.test.StepScopeTestExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import br.com.twg.pep.entity.Pessoa;

@ContextConfiguration(locations = { "/job-launcher-db2400/simple-job-launcher-context.xml", 
									"/job-runner-context.xml"})
@TestExecutionListeners( { DependencyInjectionTestExecutionListener.class, StepScopeTestExecutionListener.class })
public abstract class AbstractIoJasperTests {

	@Autowired
	private JobLauncherTestUtils jobLauncherTestUtils;

	@Autowired
	private ItemReader<Pessoa> reader;
	
	/**
	 * Check the resulting credits correspond to inputs increased by fixed
	 * amount.
	 */
	@Test
	public void testUpdatePessoas() throws Exception {

		open(reader);
		List<Pessoa> inputs = getPessoas(reader);
		close(reader);

		JobExecution jobExecution = jobLauncherTestUtils.launchJob(getUniqueJobParameters());
		assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());
		pointReaderToOutput(reader);
		
		open(reader);
		List<Pessoa> outputs = getPessoas(reader);
		close(reader);

		assertEquals(inputs.size(), outputs.size());
		int itemCount = inputs.size();
		assertTrue(itemCount > 0);
	}

	protected JobParameters getUniqueJobParameters() {
		JobParameters jobParameters = jobLauncherTestUtils.getUniqueJobParameters();
		return jobParameters;
	}

	protected abstract void pointReaderToOutput(ItemReader<Pessoa> reader);

	private List<Pessoa> getPessoas(ItemReader<Pessoa> reader) throws Exception {
		Pessoa pessoa;
		List<Pessoa> result = new ArrayList<Pessoa>();		
		while ((pessoa = reader.read()) != null) {
			result.add(pessoa);
		}
		return result;
	}

	private void open(ItemReader<?> reader) {		
		if (reader instanceof ItemStream) {
			((ItemStream) reader).open(new ExecutionContext());
		}
	}

	private void close(ItemReader<?> reader) {
		if (reader instanceof ItemStream) {
			((ItemStream) reader).close();
		}
	}
	
	protected StepExecution getStepExecution() {
		return MetaDataInstanceFactory.createStepExecution(getUniqueJobParameters());
	}
}