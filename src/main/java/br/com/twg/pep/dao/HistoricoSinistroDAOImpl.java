package br.com.twg.pep.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.orm.jpa.EntityManagerFactoryUtils;

import br.com.twg.pep.entity.HistoricoProdutoSegurado;
import br.com.twg.pep.entity.HistoricoSinistro;
import br.com.twg.pep.util.DateUtil;

public class HistoricoSinistroDAOImpl implements HistoricoSinistroDAO{

	@PersistenceUnit(unitName="PEPPU")
	private EntityManagerFactory entityManagerFactory;
	
	@PersistenceUnit(unitName="WLSPU")
	private EntityManagerFactory entityManagerFactoryWLS;

	@Override
	public List<HistoricoSinistro> buscaSinistroPor(HistoricoProdutoSegurado produto, Date dataCriacao) {
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createQuery("select sin from HistoricoSinistro sin where sin.produto = :produto and year(sin.dataCriacao) = :ano and month(sin.dataCriacao) = :mes "); //colocar a data de criação
        query.setParameter("produto", produto);
        query.setParameter("ano", DateUtil.ano(dataCriacao));
        query.setParameter("mes", DateUtil.mes(dataCriacao));
        return  query.getResultList(); 
	}

}