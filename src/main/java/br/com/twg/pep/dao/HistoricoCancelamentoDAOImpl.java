package br.com.twg.pep.dao;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.orm.jpa.EntityManagerFactoryUtils;

import br.com.twg.pep.entity.HistoricoCancelamento;
import br.com.twg.pep.entity.HistoricoProdutoSegurado;
import br.com.twg.pep.util.DateUtil;

public class HistoricoCancelamentoDAOImpl implements HistoricoCancelamentoDAO{

	@PersistenceUnit(unitName="JDEPU")
	private EntityManagerFactory entityManagerFactoryJDE;
	
	@PersistenceUnit(unitName="PEPPU")
	private EntityManagerFactory entityManagerFactory;

	@Override
	public HistoricoCancelamento buscaCancelamentoPor(HistoricoProdutoSegurado produto, Date dataCriacao) {
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createQuery("select can from HistoricoCancelamento can where can.produto = :produto and year(can.dataCriacao) = :ano and month(can.dataCriacao) = :mes "); //colocar a data de criação"
        query.setParameter("produto", produto);
        query.setParameter("ano", DateUtil.ano(dataCriacao));
        query.setParameter("mes", DateUtil.mes(dataCriacao));
        HistoricoCancelamento cancelamento = null;
        try{
        	cancelamento = (HistoricoCancelamento) query.getSingleResult();
        }catch(NoResultException nre){
        	return null;
        }
        return cancelamento; 
	}
}