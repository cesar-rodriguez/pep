package br.com.twg.pep.item;

import java.io.IOException;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;

import org.springframework.core.io.Resource;

public interface ItemWriterJasper {
	
	void exportaArquivoJasper(final JasperPrint print, final Resource resourceExport) throws IOException, JRException;

}	