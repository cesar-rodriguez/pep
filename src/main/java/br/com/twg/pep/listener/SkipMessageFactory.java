package br.com.twg.pep.listener;

import br.com.twg.pep.entity.PessoaTitularDependente;

public class SkipMessageFactory {

	public static <T> SkipMessageEmail<?> createInstance(T item){
		
		if(item instanceof PessoaTitularDependente){
			return new SkipPessoaTitularDependenteEmail();
		}
		return null;	
	}	
}
