package br.com.twg.pep.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.orm.jpa.EntityManagerFactoryUtils;

import br.com.twg.pep.entity.Cancelamento;
import br.com.twg.pep.entity.ProdutoSegurado;
import br.com.twg.pep.util.DateUtil;

public class CancelamentoDAOImpl implements CancelamentoDAO{

	@PersistenceUnit(unitName="JDEPU")
	private EntityManagerFactory entityManagerFactoryJDE;
	
	@PersistenceUnit(unitName="PEPPU")
	private EntityManagerFactory entityManagerFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> buscaDatasPagamentoPorNumeroCertificado(List<String> numerosCertificados) {
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactoryJDE);
    	Query query = entityManager.createNativeQuery(queryJDEDatasPagamentos());
        query.setParameter("numeroContrato", numerosCertificados);
        List<Object[]>  listaResultados = query.getResultList();
        return listaResultados;
	}
	
	@Override
	public Object[] buscaDatasPagamentoPorNumeroCertificado(String numeroCertificado) {
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactoryJDE);
    	Query query = entityManager.createNativeQuery(queryJDEDataPagamento());
        query.setParameter("numeroContrato", numeroCertificado);
        Object[] resultado = null;
        try{
        	resultado = (Object[]) query.getSingleResult();
        }catch(NoResultException nre){
        	return null;
        }
        return resultado;
	}
	
	public String queryJDEDatasPagamentos(){
		
		String schemaJDE = (String) entityManagerFactoryJDE.getProperties().get("hibernate.default_schema");
	
		StringBuilder sb = new StringBuilder();
		
		sb.append(" SELECT ")
	    .append("TRIM("+schemaJDE+".F0411.RPVINV) AS NUMERO_CONTRATO, ")                                                  
	    .append("DATE(DIGITS (CAST(("+schemaJDE+".f0411.RPDDJ + 1900000)  AS  DEC(7,0)))) AS DT_VENCTO, ")                                
	    .append("DATE(DIGITS (CAST(("+schemaJDE+".f0413.RMDMTJ + 1900000) AS DEC(7,0)))) AS DT_PAGTO ")
	    .append("FROM "+schemaJDE+".F0411 ") 
	    .append("INNER JOIN "+schemaJDE+".F0414 ON ("+schemaJDE+".F0411.RPDOC = "+schemaJDE+".F0414.RNDOC AND "+schemaJDE+".F0411.RPSFX = "+schemaJDE+".F0414.RNSFX AND "+schemaJDE+".F0411.RPKCO = "+schemaJDE+".F0414.RNKCO) ")
	    .append("INNER JOIN "+schemaJDE+".F0413 ON ("+schemaJDE+".F0414.RNPYID = "+schemaJDE+".F0413.RMPYID) ")
	    .append("WHERE RPCO = '00002' ")             
	    .append("AND   RNDCTM IN ( 'PT', 'PN') ")  
	    .append("AND   RPVINV IN ( :numeroContrato ) "); 
		
		return sb.toString();
	}
	
	public String queryJDEDataPagamento(){
		
		String schemaJDE = (String) entityManagerFactoryJDE.getProperties().get("hibernate.default_schema");
	
		StringBuilder sb = new StringBuilder();
		
		sb.append(" SELECT ")
	    .append("TRIM("+schemaJDE+".F0411.RPVINV) AS NUMERO_CONTRATO, ")                                                  
	    .append("DATE(DIGITS (CAST(("+schemaJDE+".f0411.RPDDJ + 1900000)  AS  DEC(7,0)))) AS DT_VENCTO, ")                                
	    .append("DATE(DIGITS (CAST(("+schemaJDE+".f0413.RMDMTJ + 1900000) AS DEC(7,0)))) AS DT_PAGTO ")
	    .append("FROM "+schemaJDE+".F0411 ") 
	    .append("INNER JOIN "+schemaJDE+".F0414 ON ("+schemaJDE+".F0411.RPDOC = "+schemaJDE+".F0414.RNDOC AND "+schemaJDE+".F0411.RPSFX = "+schemaJDE+".F0414.RNSFX AND "+schemaJDE+".F0411.RPKCO = "+schemaJDE+".F0414.RNKCO) ")
	    .append("INNER JOIN "+schemaJDE+".F0413 ON ("+schemaJDE+".F0414.RNPYID = "+schemaJDE+".F0413.RMPYID) ")
	    .append("WHERE RPCO = '00002' ")             
	    .append("AND   RNDCTM IN ( 'PT', 'PN') ")  
	    .append("AND   RPVINV =   :numeroContrato  "); 
		
		return sb.toString();
	}
	
	public void removeCancelamentos(){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		estruturaAtualizacao("DELETE FROM "+schema+".CANCELAMENTO");
	}
	
	public void moveDadosParaHistoricos(Date horanInicioJob){
		estruturaAtualizacao(insertSelectHistorico(horanInicioJob));	
	}
	
	private String insertSelectHistorico(Date dataHistorico){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		StringBuilder sbInsert = new StringBuilder();
		sbInsert.append(" INSERT INTO "+schema+".HISTORICO_CANCELAMENTO (ID, DATA_CRIACAO, PRODUTO_SEGURADO_ID, VALOR_REEMBOLSADO, RAZAO_CANCELAMENTO, DATA_VENCIMENTO, DATA_PAGAMENTO) ")
				.append(" SELECT ID, ")
				.append( "'"+ DateUtil.formataDataHistorico(dataHistorico) + "'" + ", ")
				.append(" PRODUTO_SEGURADO_ID, VALOR_REEMBOLSADO, RAZAO_CANCELAMENTO, DATA_VENCIMENTO, DATA_PAGAMENTO ")                 
				.append(" FROM "+schema+".CANCELAMENTO ");
		
		return sbInsert.toString();
	}
	
	private void estruturaAtualizacao(String strUpdate){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createNativeQuery(strUpdate);
    	query.executeUpdate();	
	}

	@Override
	public Cancelamento buscaCancelamentoPor(ProdutoSegurado produto) {
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createQuery("select can from Cancelamento can where can.produto = :produto");
        query.setParameter("produto", produto);
        Cancelamento cancelamento = null;
        try{
        	cancelamento = (Cancelamento) query.getSingleResult();
        }catch(NoResultException nre){
        	return null;
        }
        return cancelamento; 
	}
}