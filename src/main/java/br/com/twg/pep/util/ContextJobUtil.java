package br.com.twg.pep.util;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;

public class ContextJobUtil {

	public static ExecutionContext getContextoExecucaoJob(StepExecution stepExecution){
		JobExecution jobExecution = stepExecution.getJobExecution();
		return jobExecution.getExecutionContext();
	}
}