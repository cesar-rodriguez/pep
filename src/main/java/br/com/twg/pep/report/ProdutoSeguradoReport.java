package br.com.twg.pep.report;

import java.util.List;

public class ProdutoSeguradoReport {
	
	private String status;
	private String numeroCertificado;
	private String servicoContratado;
	private String valorPremio;
	private String valorProduto;
	private String dataEmissao;
	private String inicioVigencia;
	private String fimVigencia;
	private String fabricante;
	private String modelo;
	private String modeloDealer;
	private CancelamentoReport cancelamento;
	private List<SinistroReport> sinistros;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataEmissao == null) ? 0 : dataEmissao.hashCode());
		result = prime * result
				+ ((fabricante == null) ? 0 : fabricante.hashCode());
		result = prime * result
				+ ((fimVigencia == null) ? 0 : fimVigencia.hashCode());
		result = prime * result
				+ ((inicioVigencia == null) ? 0 : inicioVigencia.hashCode());
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result
				+ ((modeloDealer == null) ? 0 : modeloDealer.hashCode());
		result = prime
				* result
				+ ((numeroCertificado == null) ? 0 : numeroCertificado
						.hashCode());
		result = prime
				* result
				+ ((servicoContratado == null) ? 0 : servicoContratado
						.hashCode());
		result = prime * result
				+ ((sinistros == null) ? 0 : sinistros.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result
				+ ((valorPremio == null) ? 0 : valorPremio.hashCode());
		result = prime * result
				+ ((valorProduto == null) ? 0 : valorProduto.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoSeguradoReport other = (ProdutoSeguradoReport) obj;
		if (dataEmissao == null) {
			if (other.dataEmissao != null)
				return false;
		} else if (!dataEmissao.equals(other.dataEmissao))
			return false;
		if (fabricante == null) {
			if (other.fabricante != null)
				return false;
		} else if (!fabricante.equals(other.fabricante))
			return false;
		if (fimVigencia == null) {
			if (other.fimVigencia != null)
				return false;
		} else if (!fimVigencia.equals(other.fimVigencia))
			return false;
		if (inicioVigencia == null) {
			if (other.inicioVigencia != null)
				return false;
		} else if (!inicioVigencia.equals(other.inicioVigencia))
			return false;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		if (modeloDealer == null) {
			if (other.modeloDealer != null)
				return false;
		} else if (!modeloDealer.equals(other.modeloDealer))
			return false;
		if (numeroCertificado == null) {
			if (other.numeroCertificado != null)
				return false;
		} else if (!numeroCertificado.equals(other.numeroCertificado))
			return false;
		if (servicoContratado == null) {
			if (other.servicoContratado != null)
				return false;
		} else if (!servicoContratado.equals(other.servicoContratado))
			return false;
		if (sinistros == null) {
			if (other.sinistros != null)
				return false;
		} else if (!sinistros.equals(other.sinistros))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (valorPremio == null) {
			if (other.valorPremio != null)
				return false;
		} else if (!valorPremio.equals(other.valorPremio))
			return false;
		if (valorProduto == null) {
			if (other.valorProduto != null)
				return false;
		} else if (!valorProduto.equals(other.valorProduto))
			return false;
		return true;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNumeroCertificado() {
		return numeroCertificado;
	}
	public void setNumeroCertificado(String numeroCertificado) {
		this.numeroCertificado = numeroCertificado;
	}
	public String getServicoContratado() {
		return servicoContratado;
	}
	public void setServicoContratado(String servicoContratado) {
		this.servicoContratado = servicoContratado;
	}
	public String getValorPremio() {
		return valorPremio;
	}
	public void setValorPremio(String valorPremio) {
		this.valorPremio = valorPremio;
	}
	public String getValorProduto() {
		return valorProduto;
	}
	public void setValorProduto(String valorProduto) {
		this.valorProduto = valorProduto;
	}
	public String getDataEmissao() {
		return dataEmissao;
	}
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public String getInicioVigencia() {
		return inicioVigencia;
	}
	public void setInicioVigencia(String inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}
	public String getFimVigencia() {
		return fimVigencia;
	}
	public void setFimVigencia(String fimVigencia) {
		this.fimVigencia = fimVigencia;
	}
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getModeloDealer() {
		return modeloDealer;
	}
	public void setModeloDealer(String modeloDealer) {
		this.modeloDealer = modeloDealer;
	}
	public List<SinistroReport> getSinistros() {
		return sinistros;
	}
	public void setSinistros(List<SinistroReport> sinistros) {
		this.sinistros = sinistros;
	}
	public CancelamentoReport getCancelamento() {
		return cancelamento;
	}
	public void setCancelamento(CancelamentoReport cancelamento) {
		this.cancelamento = cancelamento;
	}
	@Override
	public String toString() {
		return "ProdutoSeguradoReport [status=" + status
				+ ", numeroCertificado=" + numeroCertificado
				+ ", servicoContratado=" + servicoContratado + ", valorPremio="
				+ valorPremio + ", valorProduto=" + valorProduto
				+ ", dataEmissao=" + dataEmissao + ", inicioVigencia="
				+ inicioVigencia + ", fimVigencia=" + fimVigencia
				+ ", fabricante=" + fabricante + ", modelo=" + modelo
				+ ", modeloDealer=" + modeloDealer + ", cancelamento="
				+ cancelamento + ", sinistros=" + sinistros + "]";
	}
}