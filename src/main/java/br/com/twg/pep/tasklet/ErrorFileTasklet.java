package br.com.twg.pep.tasklet;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import br.com.twg.pep.exception.PEPException;

public class ErrorFileTasklet implements Tasklet{

	protected final Log logger = LogFactory.getLog(getClass());

	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

		if (!existeTresArquivos()) {
			throw new PEPException("N�o foram encontrados os arquivos para carga dos dados!");
		}
		return RepeatStatus.FINISHED;
	}
	
	private boolean existeTresArquivos(){
		File fileOrgaos = new File("\\\\BRSA2K8FS01\\pep\\Orgaos.txt");
		File fileCargos = new File("\\\\BRSA2K8FS01\\pep\\Cargos.txt");
		File filePEP = new File("\\\\BRSA2K8FS01\\pep\\PEP.txt");
		if(fileOrgaos.exists() 
				&& fileCargos.exists()
				&& filePEP.exists()) {
			return true;
			}
	
		return false;
	}
}