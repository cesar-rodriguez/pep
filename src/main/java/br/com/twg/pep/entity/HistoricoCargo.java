package br.com.twg.pep.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name="HISTORICO_CARGO")
public class HistoricoCargo implements Serializable{

	private static final long serialVersionUID = 8671450789534660433L;

	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name="NOME")
	private String nome;	

	@Column(name="DATA_EXTINCAO")
	private Date dataExtincao;
	
	@Column(name="TRATAMENTO_MASCULINO")
	private String tratamentoMasculino;
	
	@Column(name="TRATAMENTO_FEMININO")
	private String tratamentoFeminio;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CRIACAO")
	private Date dataCriacao;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataExtincao == null) ? 0 : dataExtincao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime
				* result
				+ ((tratamentoFeminio == null) ? 0 : tratamentoFeminio
						.hashCode());
		result = prime
				* result
				+ ((tratamentoMasculino == null) ? 0 : tratamentoMasculino
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoCargo other = (HistoricoCargo) obj;
		if (dataExtincao == null) {
			if (other.dataExtincao != null)
				return false;
		} else if (!dataExtincao.equals(other.dataExtincao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (tratamentoFeminio == null) {
			if (other.tratamentoFeminio != null)
				return false;
		} else if (!tratamentoFeminio.equals(other.tratamentoFeminio))
			return false;
		if (tratamentoMasculino == null) {
			if (other.tratamentoMasculino != null)
				return false;
		} else if (!tratamentoMasculino.equals(other.tratamentoMasculino))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataExtincao() {
		return dataExtincao;
	}

	public void setDataExtincao(Date dataExtincao) {
		this.dataExtincao = dataExtincao;
	}

	public String getTratamentoMasculino() {
		return tratamentoMasculino;
	}

	public void setTratamentoMasculino(String tratamentoMasculino) {
		this.tratamentoMasculino = tratamentoMasculino;
	}

	public String getTratamentoFeminio() {
		return tratamentoFeminio;
	}

	public void setTratamentoFeminio(String tratamentoFeminio) {
		this.tratamentoFeminio = tratamentoFeminio;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	@Override
	public String toString() {
		return "HistoricoCargo [id=" + id + ", nome=" + nome
				+ ", dataExtincao=" + dataExtincao + ", tratamentoMasculino="
				+ tratamentoMasculino + ", tratamentoFeminio="
				+ tratamentoFeminio + ", dataCriacao=" + dataCriacao + "]";
	}

	
}