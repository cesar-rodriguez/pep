package br.com.twg.pep.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	private static final SimpleDateFormat FORMATA_DATA_HORA = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat FORMATA_DATA = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat FORMATA_BR_DATA = new SimpleDateFormat("dd-MM-yyyy");

	public static String formataDataHistorico(Date data){
		return FORMATA_DATA_HORA.format(data);
	}
	
	public static String formataBrData(Date data){
		return FORMATA_BR_DATA.format(data);
	}
	
	public static String formataData(Date data){
		return FORMATA_DATA.format(data);
	}
	
	public static boolean validaData(String dateToValidate){
		if(dateToValidate == null){
			return true;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(false);
		try {
			sdf.parse(dateToValidate);
		} catch (ParseException e) {
			return true;
		} 
		return false;
	}
	
	public static Date ultimoDiaMes(Date data){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		return calendar.getTime();
	}
	
	public static Integer ano(Date data){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		return calendar.get(Calendar.YEAR);
	}
	
	public static Integer mes(Date data){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		return calendar.get(Calendar.MONTH) + 1;
	}
}