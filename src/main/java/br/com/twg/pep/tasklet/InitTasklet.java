package br.com.twg.pep.tasklet;

import java.io.IOException;
import java.util.Date;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.twg.pep.dao.CancelamentoDAO;
import br.com.twg.pep.dao.CargoDAO;
import br.com.twg.pep.dao.OrgaoDAO;
import br.com.twg.pep.dao.PessoaDAO;
import br.com.twg.pep.dao.PessoaTitularDependenteDAO;
import br.com.twg.pep.dao.ProdutoSeguradoDAO;
import br.com.twg.pep.dao.SinistroDAO;
import br.com.twg.pep.util.ContextJobUtil;
import br.com.twg.pep.util.PropertiesUtils;

public class InitTasklet implements Tasklet{
	
	//Carrega arquivos de propriedades
	static{
		try {
			PropertiesUtils.getLoadProperties("config-bellMail/bellMail.properties");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Autowired
	private OrgaoDAO orgaoDao;
	@Autowired
	private CargoDAO cargoDao;
	@Autowired
	private PessoaTitularDependenteDAO pessoaTitularDependenteDao;
	@Autowired
	private PessoaDAO pessoaDao;
	@Autowired
	private CancelamentoDAO cancelamentoDao;
	@Autowired
	private SinistroDAO sinistroDao;
	@Autowired
	private ProdutoSeguradoDAO produtoSeguradoDAO;
	
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		Date startTimeJob = chunkContext.getStepContext().getStepExecution().getJobExecution().getStartTime();
		ExecutionContext jobContext = ContextJobUtil.getContextoExecucaoJob(chunkContext.getStepContext().getStepExecution());
		jobContext.put("initDateJob", startTimeJob);
		cancelamentoDao.removeCancelamentos();	
		sinistroDao.removeSinistros();
		produtoSeguradoDAO.removeProdutoSegurado();
		orgaoDao.removeOrgaos();
		cargoDao.removeCargos();
		pessoaTitularDependenteDao.removePessoas();
		pessoaDao.removePessoas();
		
		return RepeatStatus.FINISHED;
	}
}