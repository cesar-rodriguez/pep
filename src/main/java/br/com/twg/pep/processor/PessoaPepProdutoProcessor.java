package br.com.twg.pep.processor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.twg.pep.dao.ProdutoSeguradoDAO;
import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.entity.ProdutoSegurado;
import br.com.twg.pep.enumerador.CompanyEnum;

public class PessoaPepProdutoProcessor implements ItemProcessor<Pessoa, List<ProdutoSegurado>> {

	@Autowired
	private ProdutoSeguradoDAO produtoSeguradoDao;
	
	@Override
	public List<ProdutoSegurado> process(Pessoa pessoa) throws Exception {
		
		String cpfCnpj = pessoa.getCpfCnpj() + pessoa.getDigitoCpfCnpj();
		
		List<ProdutoSegurado> produtos = new ArrayList<ProdutoSegurado>();
		
		for(CompanyEnum comapnyEnum: CompanyEnum.values()){
			System.out.println("PARAMETROS ---------------> " );
			System.out.println("COMPANY ------------------> " + comapnyEnum.getCompanyId().shortValue());
			System.out.println("CPF CNPJ -----------------> " + cpfCnpj);
			
			List<ProdutoSegurado> produtosSegurado = produtoSeguradoDao.buscaProdutosSeguradoPor(comapnyEnum.getCompanyId().shortValue(), cpfCnpj);
			List<ProdutoSegurado> produtosSeguradoCancelados = produtoSeguradoDao.buscaProdutosSeguradoCanceladoPor(comapnyEnum.getCompanyId().shortValue(), cpfCnpj);
			List<ProdutoSegurado> produtosSeguradoExpirados = produtoSeguradoDao.buscaProdutosSeguradoExpiradosPor(comapnyEnum.getCompanyId().shortValue(), cpfCnpj);
			if(produtosSegurado != null && !produtosSegurado.isEmpty()){
				produtos.addAll(produtosSegurado);
			}
			if(produtosSeguradoCancelados != null && !produtosSeguradoCancelados.isEmpty()){
				produtos.addAll(produtosSeguradoCancelados);
			}
			if(produtosSeguradoExpirados != null && !produtosSeguradoExpirados.isEmpty()){
				produtos.addAll(produtosSeguradoExpirados);	
			}
			if(produtos != null && !produtos.isEmpty()){
				System.out.println("TAMANHO LISTA ------------> " + produtos.size());
				System.out.println("PRODUTOS SEGURADOS--------> " + produtos);
				for(ProdutoSegurado produto: produtos){
					produto.setPessoa(pessoa);	
				}
			}
		}
		return produtos;
	}
}