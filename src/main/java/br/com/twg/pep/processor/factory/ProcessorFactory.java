package br.com.twg.pep.processor.factory;

import br.com.twg.pep.entity.HistoricoMandato;
import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.entity.PessoaTitularDependente;
import br.com.twg.pep.processor.HistoricoMandatoProcessor;
import br.com.twg.pep.processor.PepLayoutProcessor;
import br.com.twg.pep.processor.PessoaProcessor;
import br.com.twg.pep.processor.PessoaTitularDependenteProcessor;

public class ProcessorFactory {
	
	public static <T> PepLayoutProcessor<?> createInstance(T item){
		
		if(item instanceof Pessoa){
			return new PessoaProcessor();
		}else if(item instanceof HistoricoMandato){
			return new HistoricoMandatoProcessor();
		}else if(item instanceof PessoaTitularDependente){
			return new PessoaTitularDependenteProcessor();
		}
		return null;	
	}
}