package br.com.twg.pep.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import org.springframework.stereotype.Component;

@Component
@Embeddable
public class HistoricoContato implements Serializable{
	
	private static final long serialVersionUID = -3775462051641567252L;

	@Embedded
	private HistoricoTelefone telefone;
	
	@Column(name="EMAIL")
	private String email;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((telefone == null) ? 0 : telefone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoContato other = (HistoricoContato) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		return true;
	}

	public HistoricoTelefone getTelefone() {
		return telefone;
	}

	public void setTelefone(HistoricoTelefone telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "HistoricoContato [telefone=" + telefone + ", email=" + email
				+ "]";
	}
}