package br.com.twg.pep.dao;

import java.util.Date;

import br.com.twg.pep.entity.Cargo;

public interface CargoDAO {
	void removeCargos();
	void recuperaDadosHistoricos();
	void insereDadosHistoricos(Date horanInicioJob);
	Cargo buscaCargoPorId(Long id);
}
