package br.com.twg.pep.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name="HISTORICO_CANCELAMENTO")
public class HistoricoCancelamento implements Serializable{

	private static final long serialVersionUID = -7603007907456003766L;

	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Transient
	private String numeroCertificado;
	
	@Column(name="VALOR_REEMBOLSADO")
	private Double valorReembolsado;
	
	@Column(name="RAZAO_CANCELAMENTO")
	private String razaoCancelamento;
	
	//data do vencimento para pagamento
	@Temporal(TemporalType.DATE)
	@Column(name="DATA_VENCIMENTO")
	private Date dataVencimento;
	
	//data efetiva do pagamento
	@Temporal(TemporalType.DATE)
	@Column(name="DATA_PAGAMENTO")
	private Date dataPagamento;
	
	@OneToOne(fetch=FetchType.LAZY, targetEntity = HistoricoProdutoSegurado.class)
	@JoinColumn(name = "PRODUTO_SEGURADO_ID", referencedColumnName = "ID")
	private HistoricoProdutoSegurado produto;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CRIACAO")
	private Date dataCriacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroCertificado() {
		return numeroCertificado;
	}

	public void setNumeroCertificado(String numeroCertificado) {
		this.numeroCertificado = numeroCertificado;
	}

	public Double getValorReembolsado() {
		return valorReembolsado;
	}

	public void setValorReembolsado(Double valorReembolsado) {
		this.valorReembolsado = valorReembolsado;
	}

	public String getRazaoCancelamento() {
		return razaoCancelamento;
	}

	public void setRazaoCancelamento(String razaoCancelamento) {
		this.razaoCancelamento = razaoCancelamento;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public HistoricoProdutoSegurado getProduto() {
		return produto;
	}

	public void setProduto(HistoricoProdutoSegurado produto) {
		this.produto = produto;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	@Override
	public String toString() {
		return "HistoricoCancelamento [id=" + id + ", numeroCertificado="
				+ numeroCertificado + ", valorReembolsado=" + valorReembolsado
				+ ", razaoCancelamento=" + razaoCancelamento
				+ ", dataVencimento=" + dataVencimento + ", dataPagamento="
				+ dataPagamento + ", produto=" + produto + ", dataCriacao="
				+ dataCriacao + "]";
	}
	
}