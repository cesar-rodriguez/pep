package br.com.twg.pep.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;

import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.entity.PessoaPK;
import br.com.twg.pep.util.DateUtil;

public class PessoaDAOImpl implements PessoaDAO{

	@PersistenceUnit(unitName="PEPPU")
	private EntityManagerFactory entityManagerFactory;
    
    @SuppressWarnings("unchecked")
	public List<Pessoa> buscaPessoasPorCpfCnpj(Set<String> listaCpfCnpj) throws DataAccessException {
    	EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createQuery("select p from Pessoa p where concat(p.cpfCnpj, p.digitoCpfCnpj) in (:listaCpfCnpj)");
        query.setParameter("listaCpfCnpj", listaCpfCnpj);
        List<Pessoa> listaResultados = (List<Pessoa>)query.getResultList();
        return listaResultados;
    }
    
    @SuppressWarnings("unchecked")
	public List<Pessoa> buscaTodasPessoas() throws DataAccessException {
    	EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createQuery("select p from Pessoa p");
    	List<Pessoa> listaResultados = (List<Pessoa>)query.getResultList();
        return listaResultados;
    }
    
    public void removePessoas(){
    	String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		estruturaAtualizacao("DELETE FROM "+schema+".PESSOA");
	}
    
    private String queryInsereHistorico(Date dataHistorico){
    	String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(" INSERT INTO "+schema+".HISTORICO_PESSOA(ID, TIPO_PESSOA, CPF_CNPJ, DIGITO_CPF_CNPJ, FILIAL, DATA_CRIACAO, TITULARIDADE, NOME, DATA_NASCIMENTO, SEXO, LOGRADOURO, NUMERO, COMPLEMENTO, BAIRRO, CEP, MUNICIPIO, UF, DDD, TELEFONE, RAMAL, EMAIL) ")
				.append(" SELECT ID, TIPO_PESSOA, CPF_CNPJ, DIGITO_CPF_CNPJ, FILIAL, " )
				.append("'" + DateUtil.formataDataHistorico(dataHistorico) +"'" + ", ")
				.append("TITULARIDADE, NOME, DATA_NASCIMENTO, SEXO, LOGRADOURO, NUMERO, COMPLEMENTO, BAIRRO, CEP, MUNICIPIO, UF, DDD, TELEFONE, RAMAL, EMAIL ")
				.append(" FROM "+schema+".PESSOA  ");

		return sbQuery.toString();
    }
	
    @Override
	public void insereDadosHistoricos(Date dataHistorico){
    	estruturaAtualizacao(queryInsereHistorico(dataHistorico));
	}
	
	public void recuperaDadosHistoricos(){
		estruturaAtualizacao(queryRecuperaDadosHistorico());
	}
	
	private String queryRecuperaDadosHistorico(){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		StringBuilder sbInsert = new StringBuilder();
		sbInsert.append(" INSERT INTO "+schema+".PESSOA (ID, TIPO_PESSOA, CPF_CNPJ, DIGITO_CPF_CNPJ, FILIAL, TITULARIDADE, NOME, DATA_NASCIMENTO, SEXO, LOGRADOURO, NUMERO, COMPLEMENTO, BAIRRO, CEP, MUNICIPIO, UF, DDD, TELEFONE, RAMAL, EMAIL) ")
				.append(" SELECT ID, TIPO_PESSOA, CPF_CNPJ, DIGITO_CPF_CNPJ, FILIAL, TITULARIDADE, NOME, DATA_NASCIMENTO, SEXO, LOGRADOURO, NUMERO, COMPLEMENTO, BAIRRO, CEP, MUNICIPIO, UF, DDD, TELEFONE, RAMAL, EMAIL ")                 
				.append(" FROM "+schema+".HISTORICO_PESSOA ")
				.append(" WHERE DATA_CRIACAO = (SELECT MAX(DATA_CRIACAO) FROM "+schema+".HISTORICO_PESSOA) ");
		
		return sbInsert.toString();
	}	
	
	private void estruturaAtualizacao(String strUpdate){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createNativeQuery(strUpdate);
    	query.executeUpdate();	
	}
	
	@SuppressWarnings("unchecked")
	public List<Pessoa> buscaPessoasSemProdutos(){
    	EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	
    	Query query = entityManager.createQuery(" select p from Pessoa p left join p.produtos prds where prds.id is null ");
        List<Pessoa> listaResultados = (List<Pessoa>)query.getResultList();
        return listaResultados;
    }
	
	public Pessoa buscaPessoasPorId(PessoaPK pessoaPk){
    	EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	return  entityManager.find(Pessoa.class, pessoaPk);
    }
}	