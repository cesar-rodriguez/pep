package br.com.twg.pep.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name="ORGAO")
public class Orgao implements Serializable{

	private static final long serialVersionUID = 3380271991954797900L;

	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name="NOME")
	private String nome;

	private Endereco endereco;
	
	private Contato contato;
	
	@Column(name="ORGAO_SUPERIOR_ID")
	private Long orgaoSuperiorId;
	
	@Column(name="SIGLA_ORGAO_SUPERIOR")
	private String siglaOrgaoSuperior;
	
	@Column(name="CNPJ")
	private String cnpj;
	
	@Column(name="DATA_EXTINCAO")
	private Date dataExtincao;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result + ((contato == null) ? 0 : contato.hashCode());
		result = prime * result
				+ ((dataExtincao == null) ? 0 : dataExtincao.hashCode());
		result = prime * result
				+ ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((orgaoSuperiorId == null) ? 0 : orgaoSuperiorId.hashCode());
		result = prime
				* result
				+ ((siglaOrgaoSuperior == null) ? 0 : siglaOrgaoSuperior
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Orgao other = (Orgao) obj;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (contato == null) {
			if (other.contato != null)
				return false;
		} else if (!contato.equals(other.contato))
			return false;
		if (dataExtincao == null) {
			if (other.dataExtincao != null)
				return false;
		} else if (!dataExtincao.equals(other.dataExtincao))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (orgaoSuperiorId == null) {
			if (other.orgaoSuperiorId != null)
				return false;
		} else if (!orgaoSuperiorId.equals(other.orgaoSuperiorId))
			return false;
		if (siglaOrgaoSuperior == null) {
			if (other.siglaOrgaoSuperior != null)
				return false;
		} else if (!siglaOrgaoSuperior.equals(other.siglaOrgaoSuperior))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	@Embedded
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Contato getContato() {
		return contato;
	}

	@Embedded
	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public Long getOrgaoSuperiorId() {
		return orgaoSuperiorId;
	}

	public void setOrgaoSuperiorId(Long orgaoSuperiorId) {
		this.orgaoSuperiorId = orgaoSuperiorId;
	}

	public String getSiglaOrgaoSuperior() {
		return siglaOrgaoSuperior;
	}

	public void setSiglaOrgaoSuperior(String siglaOrgaoSuperior) {
		this.siglaOrgaoSuperior = siglaOrgaoSuperior;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Date getDataExtincao() {
		return dataExtincao;
	}

	public void setDataExtincao(Date dataExtincao) {
		this.dataExtincao = dataExtincao;
	}

	@Override
	public String toString() {
		return "Orgao [id=" + id + ", nome=" + nome + ", endereco=" + endereco
				+ ", contato=" + contato + ", orgaoSuperiorId="
				+ orgaoSuperiorId + ", siglaOrgaoSuperior="
				+ siglaOrgaoSuperior + ", cnpj=" + cnpj + ", dataExtincao="
				+ dataExtincao + "]";
	}
}