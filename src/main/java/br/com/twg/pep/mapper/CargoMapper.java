package br.com.twg.pep.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import br.com.twg.pep.entity.Cargo;
import br.com.twg.pep.util.DateUtil;

public class CargoMapper implements FieldSetMapper<Cargo> {

	@Override
	public Cargo mapFieldSet(FieldSet fieldSet) throws BindException {
		Cargo cargo = new Cargo();
		
		cargo.setId(fieldSet.readString("CARGO_ID").equals("")? null: fieldSet.readLong("CARGO_ID")); 
		cargo.setNome(fieldSet.readString("NOME"));
		String dataExtincao = fieldSet.readString("DATA_EXTINCAO");
		cargo.setDataExtincao(DateUtil.validaData(dataExtincao)? null: fieldSet.readDate("DATA_EXTINCAO", "dd/MM/yyyy"));
		cargo.setTratamentoFeminio(fieldSet.readString("TRATAMENTO_FEMININO"));
		cargo.setTratamentoMasculino(fieldSet.readString("TRATAMENTO_MASCULINO"));
		
		System.out.println("CARGO --------------------> " + cargo);

		return cargo;
	}

}
