package br.com.twg.pep.tasklet;

import java.util.Date;
import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.twg.pep.dao.CancelamentoDAO;
import br.com.twg.pep.dao.PessoaDAO;
import br.com.twg.pep.dao.ProdutoSeguradoDAO;
import br.com.twg.pep.dao.SinistroDAO;
import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.util.EmailUtil;
import br.com.twg.pep.util.PropertiesUtils;
import br.twg.bellmail.util.MailUtil;

/**
 * @author brcrodriguez
 *
 */
public class DestroyTasklet implements Tasklet {

	@Autowired
	private CancelamentoDAO cancelamentoDao;
	@Autowired
	private SinistroDAO sinistroDao;
	@Autowired
	private ProdutoSeguradoDAO produtoSeguradoDAO;
	@Autowired
	private PessoaDAO pessoaDao;
	
	private Date startTime;
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
				
		String msg = getEmailMessage();
		
		if(msg != null && !msg.equals("")){
			String assunto = PropertiesUtils.getProperty("pep.assunto.pepSemProduto");
			
			MailUtil.enviaEmail( assunto, EmailUtil.emailsTo() , EmailUtil.emailsFrom() , msg);
		}
		
		cancelamentoDao.moveDadosParaHistoricos(startTime);
		cancelamentoDao.removeCancelamentos();
	
		sinistroDao.moveDadosParaHistoricos(startTime);
		sinistroDao.removeSinistros();
		
		produtoSeguradoDAO.moveDadosParaHistoricos(startTime);
		produtoSeguradoDAO.removeProdutoSegurado();
		
		return RepeatStatus.FINISHED;
	}
	
	private String pepSemProdutos(){
		List<Pessoa> pessoas = pessoaDao.buscaPessoasSemProdutos();
		StringBuilder sbPessoas = new StringBuilder();
		
		for(Pessoa pessoa: pessoas){
			sbPessoas.append("NOME: ")
					 .append(pessoa.getNome())
					 .append(" \n ");
		}
		return sbPessoas.toString();
	}
	
	private String getEmailMessage(){
		String cabecalho = "As pessoas abaixo n�o tem produtos no WLS: \n ";
		String pepSemProduto = pepSemProdutos();
		if(pepSemProduto == null || pepSemProduto.equals("")){
			return null;
		}
		String msg = cabecalho + pepSemProdutos();
		return msg;
	}
	
		public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

}