package br.com.twg.pep.enumerador;

public enum CompanyEnum {

	BW(3L), AUTO(4L);

	private Long companyId;

	private CompanyEnum(final Long companyId) {
		this.companyId = companyId;
	}
	public Long getCompanyId() {
		return companyId;
	}
}