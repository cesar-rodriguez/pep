package br.com.twg.pep.itemwriter.jasper.factory;

import org.springframework.batch.item.ItemWriter;

import br.com.twg.pep.itemwriter.jasper.ItemWriterJasperFile;

public class ItemWriterJasperFactory {

	public static <T> ItemWriter<T> createInstance(T type){
		return new ItemWriterJasperFile<T>();
	}
}