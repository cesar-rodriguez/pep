package br.com.twg.pep.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class PessoaTitularDependentePK implements Serializable{
	
	private static final long serialVersionUID = 9080409248127841617L;

	private Pessoa pessoaDependente;
	
	private Pessoa pessoaTitular;
	
	private String grauRelacionamento;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((grauRelacionamento == null) ? 0 : grauRelacionamento
						.hashCode());
		result = prime
				* result
				+ ((pessoaDependente == null) ? 0 : pessoaDependente.hashCode());
		result = prime * result
				+ ((pessoaTitular == null) ? 0 : pessoaTitular.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaTitularDependentePK other = (PessoaTitularDependentePK) obj;
		if (grauRelacionamento == null) {
			if (other.grauRelacionamento != null)
				return false;
		} else if (!grauRelacionamento.equals(other.grauRelacionamento))
			return false;
		if (pessoaDependente == null) {
			if (other.pessoaDependente != null)
				return false;
		} else if (!pessoaDependente.equals(other.pessoaDependente))
			return false;
		if (pessoaTitular == null) {
			if (other.pessoaTitular != null)
				return false;
		} else if (!pessoaTitular.equals(other.pessoaTitular))
			return false;
		return true;
	}

	public Pessoa getPessoaDependente() {
		return pessoaDependente;
	}

	public void setPessoaDependente(Pessoa pessoaDependente) {
		this.pessoaDependente = pessoaDependente;
	}

	public Pessoa getPessoaTitular() {
		return pessoaTitular;
	}

	public void setPessoaTitular(Pessoa pessoaTitular) {
		this.pessoaTitular = pessoaTitular;
	}

	public String getGrauRelacionamento() {
		return grauRelacionamento;
	}

	public void setGrauRelacionamento(String grauRelacionamento) {
		this.grauRelacionamento = grauRelacionamento;
	}
}