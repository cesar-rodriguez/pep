package br.com.twg.pep.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

import br.com.twg.pep.enumerador.StatusEnum;

@Component
@Entity
@Table(name="SINISTRO")
public class Sinistro implements Serializable{

	private static final long serialVersionUID = -2192210413674207616L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Long id;
	
	@Transient
	private String numeroCertificadoTransient;
	
	@Column(name="NUMERO_SINISTRO")
	private Integer numeroSinistro;
	
	@Enumerated(EnumType.STRING)
	@Column(name="STATUS")
	private StatusEnum status;
	
	@Column(name="DATA_ENTRADA")
	private Date dataEntrada;
	
	@Column(name="VALOR_TOTAL")
	private Double valorTotal;
	
	@Column(name="REEMBOLSADO")
	private String reembolsado;
	
	@Column(name="VALOR_REEMBOLSADO")
	private Double valorReembolsado;
	
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = ProdutoSegurado.class)
	@JoinColumn(name = "PRODUTO_SEGURADO_ID", referencedColumnName = "ID")
	private ProdutoSegurado produto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumeroCertificadoTransient() {
		return numeroCertificadoTransient;
	}

	public void setNumeroCertificadoTransient(String numeroCertificadoTransient) {
		this.numeroCertificadoTransient = numeroCertificadoTransient;
	}

	public Integer getNumeroSinistro() {
		return numeroSinistro;
	}

	public void setNumeroSinistro(Integer numeroSinistro) {
		this.numeroSinistro = numeroSinistro;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public Date getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getReembolsado() {
		return reembolsado;
	}

	public void setReembolsado(String reembolsado) {
		this.reembolsado = reembolsado;
	}

	public ProdutoSegurado getProduto() {
		return produto;
	}

	public void setProduto(ProdutoSegurado produto) {
		this.produto = produto;
	}

	public Double getValorReembolsado() {
		return valorReembolsado;
	}

	public void setValorReembolsado(Double valorReembolsado) {
		this.valorReembolsado = valorReembolsado;
	}

	@Override
	public String toString() {
		return "Sinistro [id=" + id + ", numeroCertificadoTransient="
				+ numeroCertificadoTransient + ", numeroSinistro="
				+ numeroSinistro + ", status=" + status + ", dataEntrada="
				+ dataEntrada + ", valorTotal=" + valorTotal + ", reembolsado="
				+ reembolsado + ", valorReembolsado=" + valorReembolsado
				+ ", produto=" + produto + "]";
	}
}