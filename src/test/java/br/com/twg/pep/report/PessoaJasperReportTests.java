package br.com.twg.pep.report;

import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"/jobs/io/cargaOrgao.xml", "/jobs/io/cargaCargo.xml",  "/jobs/io/sinistro.xml", "/jobs/io/cancelamento.xml", "/jobs/io/pepReport.xml", "/jobs/io/produtoSegurado.xml", "/jobs/io/cargaPessoa.xml", "/jobs/ioJasperPessoa.xml", "/job-launcher-db2400/simple-job-launcher-context.xml", "/job-runner-context.xml"})
public class PessoaJasperReportTests {

	@Autowired
	private JobLauncherTestUtils jobLauncherTestUtils;

//	@Test
	public void testJobLaunch() throws Exception {
		jobLauncherTestUtils.launchJob();
	}	
}