﻿/*
*   Query para extrair os todos customer no layout do oficio xxxx
*   Não passar parametros
*   Rodar essa query no Toad, pois retorna muito dados
* 	Tamanho +2GB/ Tempo cerca de 3hs
*
*/
export to 'R:\export\JURIDICO\find_all_customer_by_company_code_oficio\results\find_all_customer_by_company_code_oficio.csv' of del	
SELECT CUS_NBR,  
       TRIM(CUS_FIRST_NAME) || ' ' || ( CASE WHEN CUS_MIDDLE_NAME IS NULL THEN '' ELSE TRIM(CUS_MIDDLE_NAME)|| ' ' END) || RTRIM(CUS_LAST_NAME) AS NOME,
       CDT_CUS_CONT_NBR AS NUMERO_CONTRATO,
        (select (CASE WHEN pol.pol_company_code = 3 THEN '0195' 
            WHEN pol.pol_company_code = 4 AND pol.pol_lob_code = 24  THEN '0524' 
            WHEN pol.pol_company_code = 4 AND pol.pol_lob_code = 42  THEN '0542' 
            WHEN pol.pol_company_code = 4 AND pol.pol_lob_code = 71  THEN '0171'
            END)
        from wfs.policy pol where roe.roe_company_code = pol.pol_company_code AND pol.pol_id = chh.chh_pol_nbr
        ) AS COD_RAMO
FROM WFS.CUSTOMER CUS
INNER JOIN WFS.CONTRACT_HEADER ON (CUS_COMPANY_CODE = CHH_COMPANY_CODE AND CHH_CUS_NBR = CUS_NBR)
INNER JOIN WFS.CONTRACT_DETAIL CDT ON (CDT_COMPANY_CODE = CHH_COMPANY_CODE AND CHH_NBR = CDT_CHD_NBR)
WHERE CUS_COMPANY_CODE IN(3, 4)
ORDER BY CUS.CUS_NBR;
