package br.com.twg.pep.report;

import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/jobs/io/peCustomer.xml", "/jobs/ioPessoasPE.xml", "/transaction-manager/transaction-manager-db2400pep.xml"})
@DirtiesContext
public class PessoaMundialmenteExpostas extends AbstractJobTests {}