package br.com.twg.pep.processor;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.batch.item.ItemProcessor;

import br.com.twg.pep.entity.Cancelamento;
import br.com.twg.pep.entity.ProdutoSegurado;

public class ProdutoSeguradoCanceladoProcessor implements ItemProcessor<Object[], ProdutoSegurado> {

	@Override
	public ProdutoSegurado process(Object[] item) throws Exception {
		
		ProdutoSegurado produtoSegurado = new ProdutoSegurado();
		
		produtoSegurado.setCpfCnpjTransient((String)item[1]);
		produtoSegurado.setStatus(((Character)item[2]).toString());
		produtoSegurado.setNumeroCertificado(((String)item[3]));
		produtoSegurado.setServicoContratado((String)item[4]);
		produtoSegurado.setValorPremio(((BigDecimal)item[5]).doubleValue());
		produtoSegurado.setValorProduto(((BigDecimal)item[6]).doubleValue());
		produtoSegurado.setDataEmissao((Date)item[7]);
		produtoSegurado.setInicioVigencia((Date)item[8]);
		produtoSegurado.setFimVigencia((Date)item[9]);
		produtoSegurado.setFabricante((String)item[10]);
		produtoSegurado.setModelo((String)item[11]);
		produtoSegurado.setModeloDealer((String)item[12]);
		
		Cancelamento cancelamento = new Cancelamento();
		
		cancelamento.setNumeroCertificado(((String)item[3]));
		cancelamento.setRazaoCancelamento((String)item[13]);
		cancelamento.setValorReembolsado(((BigDecimal)item[14]).doubleValue());
		cancelamento.setProduto(produtoSegurado);
		produtoSegurado.setCancelamento(cancelamento);
		
		System.out.println("PRODUTO_SEGURADO ---------> " + produtoSegurado);
		System.out.println("CANCELEMANTO -------------> " + cancelamento);
		
		return produtoSegurado;
	}
}