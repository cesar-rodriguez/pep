package br.com.twg.pep.itemwriter.jasper;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;

import br.com.twg.pep.item.ItemWriterJasper;
import br.com.twg.pep.itemwriter.jasper.factory.TypeJasperReportFactory;

public class ItemWriterJasperFile <T> implements ItemWriter<T>, InitializingBean {
		
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	
	private Resource recursoJrxml;
	
	private String caminhoExportacaoXLS;
	
	private String caminhoExportacaoBackupXLS;
	
	private String caminhoRelatorio;
	
	private String nomeArquivoExportacao;
	
	private Resource recursoXLS;
	
	private Resource recursoBackupXLS;
	
	private String extencoes;
	

	@Override
	public void afterPropertiesSet() throws Exception {

		Assert.notNull(recursoJrxml, "Por favor insira um path + arquivo.jrxml para ser compilado.");
		Assert.notNull(extencoes, "Por favor insira uma extensao para o arquivo.");
		
	}
	
	private boolean criaDiretorio(File arquivo ){
		if(!arquivo.exists()){
			return arquivo.mkdir();
		}
		return true;
	}

	@Override
	public void write(List<? extends T> pessoas) throws Exception {
		
		Date dataCorrente = new Date(System.currentTimeMillis());
 		String caminho = caminhoExportacaoXLS + caminhoRelatorio + "\\" + sdf.format(dataCorrente); 
		String caminhoBackup = caminhoExportacaoBackupXLS + caminhoRelatorio + "\\" + sdf.format(dataCorrente);
		
		File arquivo = new File(caminho);
		File arquivoBackup = new File(caminhoBackup);
		
		criaDiretorio(arquivo);
		criaDiretorio(arquivoBackup);
		
		JasperReport relatorioPai = JasperCompileManager.compileReport(recursoJrxml.getInputStream());		
		JasperPrint print = JasperFillManager.fillReport(relatorioPai, null, new JRBeanCollectionDataSource(pessoas));
		for(String extensao: extencoes.split(",")){
			String nomeArquivo = nomeArquivoExportacao + "." + extensao.trim();
			recursoXLS = new FileSystemResource(caminho + "\\" + nomeArquivo);
			recursoBackupXLS = new FileSystemResource(caminhoBackup  + "\\" + nomeArquivo);
			ItemWriterJasper itemWriterJasper = TypeJasperReportFactory.getInstance(extensao);
			itemWriterJasper.exportaArquivoJasper(print, recursoXLS);
			itemWriterJasper.exportaArquivoJasper(print, recursoBackupXLS);
		}
	}

	public Resource getRecursoJrxml() {
		return recursoJrxml;
	}

	public void setRecursoJrxml(Resource recursoJrxml) {
		this.recursoJrxml = recursoJrxml;
	}

	public String getCaminhoExportacaoXLS() {
		return caminhoExportacaoXLS;
	}

	public void setCaminhoExportacaoXLS(String caminhoExportacaoXLS) {
		this.caminhoExportacaoXLS = caminhoExportacaoXLS;
	}

	public String getCaminhoExportacaoBackupXLS() {
		return caminhoExportacaoBackupXLS;
	}

	public void setCaminhoExportacaoBackupXLS(String caminhoExportacaoBackupXLS) {
		this.caminhoExportacaoBackupXLS = caminhoExportacaoBackupXLS;
	}

	public String getCaminhoRelatorio() {
		return caminhoRelatorio;
	}

	public void setCaminhoRelatorio(String caminhoRelatorio) {
		this.caminhoRelatorio = caminhoRelatorio;
	}

	public String getNomeArquivoExportacao() {
		return nomeArquivoExportacao;
	}

	public void setNomeArquivoExportacao(String nomeArquivoExportacao) {
		this.nomeArquivoExportacao = nomeArquivoExportacao;
	}

	public Resource getRecursoXLS() {
		return recursoXLS;
	}

	public void setRecursoXLS(Resource recursoXLS) {
		this.recursoXLS = recursoXLS;
	}

	public Resource getRecursoBackupXLS() {
		return recursoBackupXLS;
	}

	public void setRecursoBackupXLS(Resource recursoBackupXLS) {
		this.recursoBackupXLS = recursoBackupXLS;
	}

	public String getExtencoes() {
		return extencoes;
	}

	public void setExtencoes(String extencoes) {
		this.extencoes = extencoes;
	}
}