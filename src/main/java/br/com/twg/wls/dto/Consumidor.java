package br.com.twg.wls.dto;

import java.io.Serializable;


/**
 * @author brcrodriguez
 *
 */
public class Consumidor{

	private Long id;
	
	private String nome;
	
	private String nomePlanilha;

	// Para esse campo DTO deve usar uma classe de contrato
	private String ramo;

	// Para esse campo DTO deve usar uma classe de contrato
	private String numeroContrato;
	
	private double grauProximidade;
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((numeroContrato == null) ? 0 : numeroContrato.hashCode());
		result = prime * result + ((ramo == null) ? 0 : ramo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Consumidor other = (Consumidor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (numeroContrato == null) {
			if (other.numeroContrato != null)
				return false;
		} else if (!numeroContrato.equals(other.numeroContrato))
			return false;
		if (ramo == null) {
			if (other.ramo != null)
				return false;
		} else if (!ramo.equals(other.ramo))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Consumidor [id=" + id + ", nome=" + nome + ", nomePlanilha="
				+ nomePlanilha + ", ramo=" + ramo + ", numeroContrato="
				+ numeroContrato + ", grauProximidade=" + grauProximidade + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public String getNomePlanilha() {
		return nomePlanilha;
	}

	public void setNomePlanilha(String nomePlanilha) {
		this.nomePlanilha = nomePlanilha;
	}

	public double getGrauProximidade() {
		return grauProximidade;
	}

	public void setGrauProximidade(double grauProximidade) {
		this.grauProximidade = grauProximidade;
	}
}