package br.com.twg.pep.util;

import br.com.twg.pep.exception.PEPException;

public class ValidateUtils {
	
	private static final String ERRO_LINHA = "Ocorreu um erro na linha: "; 
	
	public static void validateTipoPessoa(String tipoPessoa,  int numeroLinha) throws PEPException{
		if(tipoPessoa != null && !"F".equals(tipoPessoa.trim()) && !"J".equals(tipoPessoa.trim())){
			throw new PEPException(ERRO_LINHA +numeroLinha +". \n"+"O campo Tipo Pessoa est� incorreto. Valor = " + tipoPessoa + ". Por favor insira F ou J");
		}
	}
	
	public static void validateId(String id, int numeroLinha) throws PEPException{
		try{
			Long.valueOf(id);
		}catch(Exception e){
			throw new PEPException(ERRO_LINHA +numeroLinha +". \n"+"O campo ID est� incorreto. Valor = " + id + "Por favor insira um valor num�rico e inteiro v�lido");
		}
	}
	
	public static void validateObrigatorio(Object valor, String nomeCampo, int numeroLinha) throws PEPException{
		if(valor == null || valor.equals("")){
			throw new PEPException(ERRO_LINHA +numeroLinha +". \n"+"O campo "+ nomeCampo + " � obrigat�rio!. Por favor insira um valor v�lido");
		}
	}
	
	public static void validarPessoaId(String tipoPessoa, String cpfCnpj, String digitoCpfCnpj, String filial, Long pessoaId, int numeroLinha) throws PEPException{
		validateObrigatorio(tipoPessoa, tipoPessoa.toString(), numeroLinha);
		validateObrigatorio(cpfCnpj, cpfCnpj.toString(), numeroLinha);
		validateObrigatorio(digitoCpfCnpj, digitoCpfCnpj.toString(), numeroLinha);
		validateObrigatorio(filial, filial.toString(), numeroLinha);
		validateObrigatorio(pessoaId, pessoaId.toString(), numeroLinha);
		validateTipoPessoa(tipoPessoa, numeroLinha);
	}
}