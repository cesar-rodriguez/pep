package br.com.twg.pep.dao;

import java.util.Date;
import java.util.List;

import org.springframework.dao.DataAccessException;

import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.entity.ProdutoSegurado;

public interface ProdutoSeguradoDAO {

	List<ProdutoSegurado> buscaProdutoSeguradoPorNumeroContrato(List<String> listaNumeroCertificado) throws DataAccessException ;
	void removeProdutoSegurado();
	void moveDadosParaHistoricos(Date horaInicioJob);
	List<ProdutoSegurado> buscaProdutosSeguradoPor(short companyCode, String cpfCnpj);
	List<ProdutoSegurado> salvar(List<ProdutoSegurado> produtos);
	ProdutoSegurado salvar(ProdutoSegurado produto);
	List<ProdutoSegurado> buscaProdutosSeguradoCanceladoPor(short shortValue, String cpfCnpj);
	List<ProdutoSegurado> buscaProdutosSeguradoExpiradosPor(short companyCode, String cpfCnpj);
	List<ProdutoSegurado> buscaProdutoSeguradoPor(Pessoa pessoa);	
}
