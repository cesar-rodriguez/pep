package br.com.twg.pep.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@IdClass(HistoricoPessoaTitularDependentePK.class)
@Table(name = "HISTORICO_PESSOA_TITULAR_DEPENDENTE")
public class HistoricoPessoaTitularDependente implements Serializable{
	
	private static final long serialVersionUID = -1024800760769495991L;

	@Id
	@ManyToOne(fetch=FetchType.LAZY, targetEntity = HistoricoPessoa.class)
    @JoinColumns({@JoinColumn(name = "DEPENDENTE_ID"		     , referencedColumnName = "ID"),
        		  @JoinColumn(name = "DEPENDENTE_TIPO_PESSOA"    , referencedColumnName = "TIPO_PESSOA"),
        		  @JoinColumn(name = "DEPENDENTE_CPF_CNPJ"       , referencedColumnName = "CPF_CNPJ"),
        		  @JoinColumn(name = "DEPENDENTE_DIGITO_CPF_CNPJ", referencedColumnName = "DIGITO_CPF_CNPJ"),
        		  @JoinColumn(name = "DEPENDENTE_FILIAL"         , referencedColumnName = "FILIAL"),
    				})
	private HistoricoPessoa pessoaDependente;
	
	@Id
	@ManyToOne(fetch=FetchType.LAZY, targetEntity = HistoricoPessoa.class)
    @JoinColumns({@JoinColumn(name = "TITULAR_ID"		      , referencedColumnName = "ID"),
        		  @JoinColumn(name = "TITULAR_TIPO_PESSOA"    , referencedColumnName = "TIPO_PESSOA"),
        		  @JoinColumn(name = "TITULAR_CPF_CNPJ"       , referencedColumnName = "CPF_CNPJ"),
        		  @JoinColumn(name = "TITULAR_DIGITO_CPF_CNPJ", referencedColumnName = "DIGITO_CPF_CNPJ"),
        		  @JoinColumn(name = "TITULAR_FILIAL"         , referencedColumnName = "FILIAL"),
    				})
	private HistoricoPessoa pessoaTitular;
	
	@Column(name="NOME_TITULAR")
	private String nomeTitular;
	
	@Id
	@Column(name="GRAU_RELACIONAMENTO")
	private String grauRelacionamento;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((grauRelacionamento == null) ? 0 : grauRelacionamento
						.hashCode());
		result = prime * result
				+ ((nomeTitular == null) ? 0 : nomeTitular.hashCode());
		result = prime
				* result
				+ ((pessoaDependente == null) ? 0 : pessoaDependente.hashCode());
		result = prime * result
				+ ((pessoaTitular == null) ? 0 : pessoaTitular.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoPessoaTitularDependente other = (HistoricoPessoaTitularDependente) obj;
		if (grauRelacionamento == null) {
			if (other.grauRelacionamento != null)
				return false;
		} else if (!grauRelacionamento.equals(other.grauRelacionamento))
			return false;
		if (nomeTitular == null) {
			if (other.nomeTitular != null)
				return false;
		} else if (!nomeTitular.equals(other.nomeTitular))
			return false;
		if (pessoaDependente == null) {
			if (other.pessoaDependente != null)
				return false;
		} else if (!pessoaDependente.equals(other.pessoaDependente))
			return false;
		if (pessoaTitular == null) {
			if (other.pessoaTitular != null)
				return false;
		} else if (!pessoaTitular.equals(other.pessoaTitular))
			return false;
		return true;
	}

	public HistoricoPessoa getPessoaDependente() {
		return pessoaDependente;
	}

	public void setPessoaDependente(HistoricoPessoa pessoaDependente) {
		this.pessoaDependente = pessoaDependente;
	}

	public HistoricoPessoa getPessoaTitular() {
		return pessoaTitular;
	}

	public void setPessoaTitular(HistoricoPessoa pessoaTitular) {
		this.pessoaTitular = pessoaTitular;
	}

	public String getNomeTitular() {
		return nomeTitular;
	}

	public void setNomeTitular(String nomeTitular) {
		this.nomeTitular = nomeTitular;
	}

	public String getGrauRelacionamento() {
		return grauRelacionamento;
	}

	public void setGrauRelacionamento(String grauRelacionamento) {
		this.grauRelacionamento = grauRelacionamento;
	}

	@Override
	public String toString() {
		return "HistoricoPessoaTitularDependente [pessoaDependente="
				+ pessoaDependente + ", pessoaTitular=" + pessoaTitular
				+ ", nomeTitular=" + nomeTitular + ", grauRelacionamento="
				+ grauRelacionamento + "]";
	}
	
}