package br.com.twg.pep.dao;

import java.util.Date;

import br.com.twg.pep.entity.HistoricoCancelamento;
import br.com.twg.pep.entity.HistoricoProdutoSegurado;

public interface HistoricoCancelamentoDAO {

	HistoricoCancelamento buscaCancelamentoPor(HistoricoProdutoSegurado produto, Date dataCriacao);
	
}
