package br.com.twg.pep.report;


public class SinistroReport {
	
	private String numeroSinistro;
	private String status;
	private String dataEntrada;
	private String valorTotal;
	private String reembolsado;
	private String valorReembolsado;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataEntrada == null) ? 0 : dataEntrada.hashCode());
		result = prime * result
				+ ((numeroSinistro == null) ? 0 : numeroSinistro.hashCode());
		result = prime * result
				+ ((reembolsado == null) ? 0 : reembolsado.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime
				* result
				+ ((valorReembolsado == null) ? 0 : valorReembolsado.hashCode());
		result = prime * result
				+ ((valorTotal == null) ? 0 : valorTotal.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SinistroReport other = (SinistroReport) obj;
		if (dataEntrada == null) {
			if (other.dataEntrada != null)
				return false;
		} else if (!dataEntrada.equals(other.dataEntrada))
			return false;
		if (numeroSinistro == null) {
			if (other.numeroSinistro != null)
				return false;
		} else if (!numeroSinistro.equals(other.numeroSinistro))
			return false;
		if (reembolsado == null) {
			if (other.reembolsado != null)
				return false;
		} else if (!reembolsado.equals(other.reembolsado))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (valorReembolsado == null) {
			if (other.valorReembolsado != null)
				return false;
		} else if (!valorReembolsado.equals(other.valorReembolsado))
			return false;
		if (valorTotal == null) {
			if (other.valorTotal != null)
				return false;
		} else if (!valorTotal.equals(other.valorTotal))
			return false;
		return true;
	}
	public String getNumeroSinistro() {
		return numeroSinistro;
	}
	public void setNumeroSinistro(String numeroSinistro) {
		this.numeroSinistro = numeroSinistro;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDataEntrada() {
		return dataEntrada;
	}
	public void setDataEntrada(String dataEntrada) {
		this.dataEntrada = dataEntrada;
	}
	public String getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(String valorTotal) {
		this.valorTotal = valorTotal;
	}
	public String getReembolsado() {
		return reembolsado;
	}
	public void setReembolsado(String reembolsado) {
		this.reembolsado = reembolsado;
	}
	public String getValorReembolsado() {
		return valorReembolsado;
	}
	public void setValorReembolsado(String valorReembolsado) {
		this.valorReembolsado = valorReembolsado;
	}
	@Override
	public String toString() {
		return "SinistroReport [numeroSinistro=" + numeroSinistro + ", status="
				+ status + ", dataEntrada=" + dataEntrada + ", valorTotal="
				+ valorTotal + ", reembolsado=" + reembolsado
				+ ", valorReembolsado=" + valorReembolsado + "]";
	}
	
}