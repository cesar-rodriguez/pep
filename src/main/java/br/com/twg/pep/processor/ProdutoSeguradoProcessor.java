package br.com.twg.pep.processor;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.batch.item.ItemProcessor;

import br.com.twg.pep.entity.ProdutoSegurado;

public class ProdutoSeguradoProcessor implements ItemProcessor<Object[], ProdutoSegurado> {
	
	@Override
	public ProdutoSegurado process(Object[] produtoSeguradoObj) throws Exception {
	
		ProdutoSegurado produtoSegurado = new ProdutoSegurado();
		
		produtoSegurado.setCpfCnpjTransient((String)produtoSeguradoObj[0]);
		produtoSegurado.setStatus(produtoSeguradoObj[1].toString());
		produtoSegurado.setNumeroCertificado(((String)produtoSeguradoObj[2]));
		produtoSegurado.setServicoContratado((String)produtoSeguradoObj[3]);
		produtoSegurado.setValorPremio(((BigDecimal)produtoSeguradoObj[4]).doubleValue());
		produtoSegurado.setValorProduto(((BigDecimal)produtoSeguradoObj[5]).doubleValue());
		produtoSegurado.setDataEmissao((Date)produtoSeguradoObj[6]);
		produtoSegurado.setInicioVigencia((Date)produtoSeguradoObj[7]);
		produtoSegurado.setFimVigencia((Date)produtoSeguradoObj[8]);
		produtoSegurado.setFabricante((String)produtoSeguradoObj[9]);
		produtoSegurado.setModelo((String)produtoSeguradoObj[10]);
		produtoSegurado.setModeloDealer((String)produtoSeguradoObj[11]);
		
		System.out.println("PRODUTO_SEGURADO ---------> " + produtoSegurado);
	
		return produtoSegurado;
	}
}