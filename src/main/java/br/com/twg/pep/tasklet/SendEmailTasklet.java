package br.com.twg.pep.tasklet;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import br.com.twg.pep.util.DateUtil;
import br.com.twg.pep.util.EmailUtil;
import br.com.twg.pep.util.PropertiesUtils;
import br.twg.bellmail.util.MailUtil;

public class SendEmailTasklet implements Tasklet {
	
	private Date startTime;
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		File filePEPReportXls = new File("\\\\BRSA2K8FS01\\pep\\relatorio WLS\\"+ DateUtil.formataBrData(startTime) + "\\relatorioPEP.xls");
		File filePEPReportPdf = new File("\\\\BRSA2K8FS01\\pep\\relatorio WLS\\"+ DateUtil.formataBrData(startTime) + "\\relatorioPEP.pdf");
		String assunto = PropertiesUtils.getProperty("pep.assunto.pepReport");
		try{
			MailUtil.enviaEmail(assunto , EmailUtil.emailsTo(), EmailUtil.emailsFrom(), getEmailMessage(filePEPReportXls, filePEPReportPdf));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RepeatStatus.FINISHED;
	}
	
	private String getEmailMessage(File filePEPReportXls, File filePEPReportPdf){
		try{
		if(filePEPReportXls.exists() && filePEPReportPdf.exists()){
			return PropertiesUtils.getProperty("pep.corpo.sucesso");
		}
			return PropertiesUtils.getProperty("pep.corpo.erro");
		}catch(IOException e){
			e.printStackTrace();
		}
		return null;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
}