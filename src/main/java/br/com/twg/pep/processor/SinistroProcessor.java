package br.com.twg.pep.processor;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.batch.item.ItemProcessor;

import br.com.twg.pep.entity.Sinistro;
import br.com.twg.pep.enumerador.StatusEnum;

public class SinistroProcessor implements ItemProcessor<Object[], Sinistro> {

	@Override
	public Sinistro process(Object[] item) throws Exception {
		Sinistro sinistro = new Sinistro();
		sinistro.setNumeroCertificadoTransient(item[0] == null? null: (String)item[0]);
		sinistro.setNumeroSinistro(item[1] == null? null: (Integer)item[1]);
		sinistro.setStatus(item[2] == null? null: StatusEnum.buscaPorStatusId((Short)item[2]));
		sinistro.setValorTotal(item[3] == null? null: ((BigDecimal)item[3]).doubleValue());
		sinistro.setDataEntrada(item[4] == null? null: (Date)item[4]);
		sinistro.setReembolsado(item[5] == null? null: (String)item[5]);
		sinistro.setValorReembolsado(item[6] == null? null: ((BigDecimal)item[6]).doubleValue());
		System.out.println("SINISTRO -----------------> " + sinistro);
		return sinistro;
	}
}