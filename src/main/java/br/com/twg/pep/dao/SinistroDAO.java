package br.com.twg.pep.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.twg.pep.entity.ProdutoSegurado;
import br.com.twg.pep.entity.Sinistro;
@Component(value="sinistroDao")
public interface SinistroDAO {
	void removeSinistros();
	void moveDadosParaHistoricos(Date horaInicioJob);
	List<Sinistro> buscaSinistroPor(short companyCode, String numeroContrato);
	List<Sinistro> salvar(List<Sinistro> sinistros);
	Sinistro salvar(Sinistro sinistro);
	List<Sinistro> buscaSinistroPor(ProdutoSegurado produto);
}
