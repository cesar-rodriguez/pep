package br.com.twg.pep.processor;

import org.springframework.batch.core.StepExecution;

public interface PepLayoutProcessor<T> {
	
	public void processor(T t, StepExecution stepExecution) throws Exception;

}
