package br.com.twg.pep.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

@Component
@Entity
@SqlResultSetMapping(name="compositeHistkey", 
					entities=@EntityResult(entityClass=br.com.twg.pep.entity.HistoricoProdutoSegurado.class,
    fields = {
            @FieldResult(name="id", column = "ID"),
            @FieldResult(name="cpfCnpjTransient", column = "cpfCnpjTransient"),
            @FieldResult(name="status", column = "STATUS"),
            @FieldResult(name="numeroCertificado", column = "NUMERO_CERTIFICADO"),
            @FieldResult(name="servicoContratado", column = "SERVICO_CONTRATADO"),
            @FieldResult(name="valorPremio", column = "VALOR_PREMIO"),
            @FieldResult(name="valorProduto", column = "VALOR_PRODUTO"),
            @FieldResult(name="dataEmissao", column = "DATA_EMISSAO"),
            @FieldResult(name="inicioVigencia", column = "INICIO_VIGENCIA"),
            @FieldResult(name="fimVigencia", column = "FIM_VIGENCIA"),
            @FieldResult(name="fabricante", column = "FABRICANTE"),
            @FieldResult(name="modelo", column = "MODELO"),
            @FieldResult(name="modeloDealer", column = "MODELO_DEALER"),
            
            @FieldResult(name="pessoa.id", column = "PESSOA_ID"), 
            @FieldResult(name="pessoa.tipoPessoa", column = "TIPO_PESSOA"),
            @FieldResult(name="pessoa.cpfCnpj", column = "CPF_CNPJ"),
            @FieldResult(name="pessoa.digitoCpfCnpj", column = "DIGITO_CPF_CNPJ"),
            @FieldResult(name="pessoa.filial", column = "FILIAL"),

            }))


@Table(name="HISTORICO_PRODUTO_SEGURADO")
public class HistoricoProdutoSegurado implements Serializable{

	private static final long serialVersionUID = -1275294371554382311L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;
	
	@Transient
	private String cpfCnpjTransient;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="NUMERO_CERTIFICADO")
	private String numeroCertificado;
	
	@Column(name="SERVICO_CONTRATADO")
	private String servicoContratado;
	
	@Column(name="VALOR_PREMIO")
	private Double valorPremio;
	
	@Column(name="VALOR_PRODUTO")
	private Double valorProduto;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DATA_EMISSAO")
	private Date dataEmissao;
	
	@Temporal(TemporalType.DATE)
	@Column(name="INICIO_VIGENCIA")
	private Date inicioVigencia;
	
	@Temporal(TemporalType.DATE)
	@Column(name="FIM_VIGENCIA")
	private Date fimVigencia;
	
	@Column(name="FABRICANTE")
	private String fabricante;
	
	@Column(name="MODELO")
	private String modelo;
	
	@Column(name="MODELO_DEALER")
	private String modeloDealer;
	
    @ManyToOne( fetch=FetchType.LAZY, targetEntity = HistoricoPessoa.class)
    @JoinColumns({@JoinColumn(name = "PESSOA_ID", referencedColumnName = "ID"),
        		  @JoinColumn(name = "TIPO_PESSOA", referencedColumnName = "TIPO_PESSOA"),
        		  @JoinColumn(name = "CPF_CNPJ", referencedColumnName = "CPF_CNPJ"),
        		  @JoinColumn(name = "DIGITO_CPF_CNPJ", referencedColumnName = "DIGITO_CPF_CNPJ"),
        		  @JoinColumn(name = "FILIAL", referencedColumnName = "FILIAL"),
    				})
	private HistoricoPessoa pessoa;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy="produto", fetch=FetchType.LAZY, targetEntity = HistoricoCancelamento.class)
    private HistoricoCancelamento cancelamento;
    
	@OneToMany(fetch = FetchType.LAZY, mappedBy="produto")
    private Set<HistoricoSinistro> sinistros;
	
	@Column(name = "DATA_CRIACAO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCriacao;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpfCnpjTransient() {
		return cpfCnpjTransient;
	}

	public void setCpfCnpjTransient(String cpfCnpjTransient) {
		this.cpfCnpjTransient = cpfCnpjTransient;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNumeroCertificado() {
		return numeroCertificado;
	}

	public void setNumeroCertificado(String numeroCertificado) {
		this.numeroCertificado = numeroCertificado;
	}

	public String getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(String servicoContratado) {
		this.servicoContratado = servicoContratado;
	}

	public Double getValorPremio() {
		return valorPremio;
	}

	public void setValorPremio(Double valorPremio) {
		this.valorPremio = valorPremio;
	}

	public Double getValorProduto() {
		return valorProduto;
	}

	public void setValorProduto(Double valorProduto) {
		this.valorProduto = valorProduto;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Date getInicioVigencia() {
		return inicioVigencia;
	}

	public void setInicioVigencia(Date inicioVigencia) {
		this.inicioVigencia = inicioVigencia;
	}

	public Date getFimVigencia() {
		return fimVigencia;
	}

	public void setFimVigencia(Date fimVigencia) {
		this.fimVigencia = fimVigencia;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getModeloDealer() {
		return modeloDealer;
	}

	public void setModeloDealer(String modeloDealer) {
		this.modeloDealer = modeloDealer;
	}

	

	public HistoricoPessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(HistoricoPessoa pessoa) {
		this.pessoa = pessoa;
	}

	public HistoricoCancelamento getCancelamento() {
		return cancelamento;
	}

	public void setCancelamento(HistoricoCancelamento cancelamento) {
		this.cancelamento = cancelamento;
	}

	public Set<HistoricoSinistro> getSinistros() {
		return sinistros;
	}

	public void setSinistros(Set<HistoricoSinistro> sinistros) {
		this.sinistros = sinistros;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	@Override
	public String toString() {
		return "HistoricoProdutoSegurado [id=" + id + ", cpfCnpjTransient="
				+ cpfCnpjTransient + ", status=" + status
				+ ", numeroCertificado=" + numeroCertificado
				+ ", servicoContratado=" + servicoContratado + ", valorPremio="
				+ valorPremio + ", valorProduto=" + valorProduto
				+ ", dataEmissao=" + dataEmissao + ", inicioVigencia="
				+ inicioVigencia + ", fimVigencia=" + fimVigencia
				+ ", fabricante=" + fabricante + ", modelo=" + modelo
				+ ", modeloDealer=" + modeloDealer + ", pessoa=" + pessoa
				+ ", cancelamento=" + cancelamento + ", sinistros=" + sinistros
				+ "]";
	}

}