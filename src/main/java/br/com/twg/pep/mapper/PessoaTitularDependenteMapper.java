package br.com.twg.pep.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.entity.PessoaTitularDependente;

public class PessoaTitularDependenteMapper implements FieldSetMapper<PessoaTitularDependente> {

	@Override
	public PessoaTitularDependente mapFieldSet(FieldSet fieldSet) throws BindException {
		
		PessoaTitularDependente pessoaTitularDependente = new PessoaTitularDependente();
		
		//DEPENDENTE
		pessoaTitularDependente.setPessoaDependente(new Pessoa());
		pessoaTitularDependente.getPessoaDependente().setTipoPessoa(fieldSet.readString("DEPENDENTE_TIPO_PES"));
		pessoaTitularDependente.getPessoaDependente().setCpfCnpj(fieldSet.readString("DEPENDENTE_CPF_CNPJ"));
		pessoaTitularDependente.getPessoaDependente().setFilial(fieldSet.readString("DEPENDENTE_FILIAL"));
		pessoaTitularDependente.getPessoaDependente().setDigitoCpfCnpj(fieldSet.readString("DEPENDENTE_DIGITO"));
		pessoaTitularDependente.getPessoaDependente().setId(fieldSet.readString("DEPENDENTE_PESSOA_ID").equals("")? null: fieldSet.readLong("DEPENDENTE_PESSOA_ID"));
		
		//TITULAR
		pessoaTitularDependente.setPessoaTitular(new Pessoa());
		pessoaTitularDependente.getPessoaTitular().setTipoPessoa(fieldSet.readString("TITULAR_TIPO_PES"));
		pessoaTitularDependente.getPessoaTitular().setCpfCnpj(fieldSet.readString("TITULAR_CPF_CNPJ"));
		pessoaTitularDependente.getPessoaTitular().setFilial(fieldSet.readString("TITULAR_FILIAL"));
		pessoaTitularDependente.getPessoaTitular().setDigitoCpfCnpj(fieldSet.readString("TITULAR_DIGITO"));
		pessoaTitularDependente.getPessoaTitular().setId(fieldSet.readString("TITULAR_PESSOA_ID").equals("")? null: fieldSet.readLong("TITULAR_PESSOA_ID"));
		
		pessoaTitularDependente.setNomeTitular(fieldSet.readString("NOME_TITULAR"));
		pessoaTitularDependente.setGrauRelacionamento(fieldSet.readString("GRAU_RELACIONAMENTO"));	
		
		System.out.println("PESSOA_TITULAR_DEPENDENTE-> " + pessoaTitularDependente);
		
		return pessoaTitularDependente;	
	}
}