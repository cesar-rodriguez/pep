package br.com.twg.pep.itemwriter.jasper.factory;

import br.com.twg.pep.item.ItemWriterJasper;
import br.com.twg.pep.itemwriter.jasper.ItemWriterJasperPDF;
import br.com.twg.pep.itemwriter.jasper.ItemWriterJasperXLS;

public class TypeJasperReportFactory {

	public static ItemWriterJasper getInstance(String tipoArquivo) {
		if (tipoArquivo.trim().equalsIgnoreCase("pdf")) {
			return new ItemWriterJasperPDF();
		} else if (tipoArquivo.trim().equalsIgnoreCase("xls")) {
			return new ItemWriterJasperXLS();
		} 
		return null;
	}
}