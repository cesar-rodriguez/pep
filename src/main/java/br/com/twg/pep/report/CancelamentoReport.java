package br.com.twg.pep.report;

public class CancelamentoReport {
	
		private String valorReembolsado;
		private String razaoCancelamento;
		private String dataVencimento;
		private String dataPagamento;
	
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((dataPagamento == null) ? 0 : dataPagamento.hashCode());
			result = prime * result
					+ ((dataVencimento == null) ? 0 : dataVencimento.hashCode());
			result = prime
					* result
					+ ((razaoCancelamento == null) ? 0 : razaoCancelamento
							.hashCode());
			result = prime
					* result
					+ ((valorReembolsado == null) ? 0 : valorReembolsado.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CancelamentoReport other = (CancelamentoReport) obj;
			if (dataPagamento == null) {
				if (other.dataPagamento != null)
					return false;
			} else if (!dataPagamento.equals(other.dataPagamento))
				return false;
			if (dataVencimento == null) {
				if (other.dataVencimento != null)
					return false;
			} else if (!dataVencimento.equals(other.dataVencimento))
				return false;
			if (razaoCancelamento == null) {
				if (other.razaoCancelamento != null)
					return false;
			} else if (!razaoCancelamento.equals(other.razaoCancelamento))
				return false;
			if (valorReembolsado == null) {
				if (other.valorReembolsado != null)
					return false;
			} else if (!valorReembolsado.equals(other.valorReembolsado))
				return false;
			return true;
		}
		public String getValorReembolsado() {
			return valorReembolsado;
		}
		public void setValorReembolsado(String valorReembolsado) {
			this.valorReembolsado = valorReembolsado;
		}
		public String getRazaoCancelamento() {
			return razaoCancelamento;
		}
		public void setRazaoCancelamento(String razaoCancelamento) {
			this.razaoCancelamento = razaoCancelamento;
		}
		public String getDataVencimento() {
			return dataVencimento;
		}
		public void setDataVencimento(String dataVencimento) {
			this.dataVencimento = dataVencimento;
		}
		public String getDataPagamento() {
			return dataPagamento;
		}
		public void setDataPagamento(String dataPagamento) {
			this.dataPagamento = dataPagamento;
		}
		@Override
		public String toString() {
			return "CancelamentoReport [valorReembolsado=" + valorReembolsado
					+ ", razaoCancelamento=" + razaoCancelamento
					+ ", dataVencimento=" + dataVencimento + ", dataPagamento="
					+ dataPagamento + "]";
		}
}

