package br.com.twg.pep.util;

import java.io.IOException;

public class EmailUtil {

	public static String emailsFrom(){
		try {
			return PropertiesUtils.getProperty("pep.emailFrom");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String[] emailsTo(){
		String emails = null;
		try {
			emails = PropertiesUtils.getProperty("pep.emailsTo");
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(emails == null){
			return null;
		}
		return emails.split(",");
	}
	
}
