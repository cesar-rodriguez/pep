package br.com.twg.pep.dao;

import java.util.Date;

import br.com.twg.pep.entity.PessoaTitularDependente;
import br.com.twg.pep.entity.PessoaTitularDependentePK;

public interface PessoaTitularDependenteDAO {

	 void removePessoas();
	 void recuperaDadosHistoricos();
	 void insereDadosHistoricos(Date horaInicioJob);
	 PessoaTitularDependente buscaPessoaTitularDependentePorId(PessoaTitularDependentePK pessoaTitularDependentePk);
}
