package br.com.twg.wls.processor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.util.StringUtils;

import br.com.twg.wls.dto.Consumidor;

import com.aliasi.spell.JaroWinklerDistance;

public class ConsumidorProcessor implements ItemProcessor<Consumidor, Consumidor>{

	private static int print = 10000;
	private static int index;
	private String path;
	
	@Override
	public Consumidor process(Consumidor consumidor) throws Exception {
		String[] arrNames = StringUtils.commaDelimitedListToStringArray(extrairNomesPlanilha());
		double maxValue = 0.99;
		double minlValue = 0.89;
		
		if((index % print) == 0){
			System.out.println("Cliente " + index + " = " + consumidor.toString());
			
		}
		index++;
//		return proximidadeRecursivo(consumidor, maxValue, minlValue);
		return procuraConsumidor(consumidor, minlValue, arrNames);
	}
	
	private Consumidor proximidadeRecursivo(Consumidor consumidor, double value, double minlValue) throws IOException{
		
		if(value < minlValue){
			return null;
		}
		
		Consumidor consumidorAux = procuraConsumidorExcel(consumidor, value, null);
		if(consumidorAux != null){
			consumidorAux.setGrauProximidade(value);
			return consumidorAux;
		}
		
		value = value - 0.01;

		return proximidadeRecursivo(consumidor, value, minlValue);
	}
	
	public Consumidor procuraConsumidor(Consumidor consumidor, double value, String[] arrNames) throws IOException{
		Consumidor consumidorAux = procuraConsumidorExcel(consumidor, value, arrNames);
		if(consumidorAux != null){
			consumidorAux.setGrauProximidade(value);
			return consumidorAux;
		}
		return null;
	}
	
	public Consumidor procuraConsumidorExcel(Consumidor consumidor, double value, String[] arrNames) throws IOException{
		
		for(String nomePlanilha: arrNames){
			if(proximidade(consumidor.getNome(), nomePlanilha) > value){
				consumidor.setNomePlanilha(nomePlanilha);
				return consumidor;
			}
		}
		return null;
	}
	
	private double proximidade(String nomeConsumidor, String nomeBaseDados){
		JaroWinklerDistance jaroWinkler = JaroWinklerDistance.JARO_WINKLER_DISTANCE;
		return jaroWinkler.proximity(nomeConsumidor, nomeBaseDados);
	}
	
	private String extrairNomesPlanilha() throws IOException {

		File file = new File(path);
		FileInputStream fis = new FileInputStream(file);
		List<String> list = new ArrayList<String>();
		XSSFSheet sheet = UtilsPoiExcel.readXLSXAllSheets(fis);

		XSSFRow row;
		XSSFCell cell;
		Iterator rows = sheet.rowIterator();
		while (rows.hasNext()) {
			row = (XSSFRow) rows.next();
			Iterator cells = row.cellIterator();
			int c = 0;
			while (cells.hasNext()) {
				cell = (XSSFCell) cells.next();
				if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
					list.add(cell.getStringCellValue() + " ");
				}
				// A linha abaixo é apenas para fins de esclarecimento
				if (c == 0) {
					c++;
					break;
				}
			}
		}
		return StringUtils.collectionToCommaDelimitedString(list);
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	
	
	
}
