package br.com.twg.pep.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import br.com.twg.pep.entity.Contato;
import br.com.twg.pep.entity.Endereco;
import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.entity.Telefone;
import br.com.twg.pep.util.DateUtil;

public class PessoaMapper implements FieldSetMapper<Pessoa> {

	@Override
	public Pessoa mapFieldSet(FieldSet fieldSet) throws BindException {
		
		if (fieldSet == null){
			return null;
		}
		
		Pessoa pessoa = new Pessoa();
		Endereco endereco = null;
		//endereco
		String strEndereco = fieldSet.readString("ENDERECO");
		
		if(strEndereco != null && !strEndereco.equals("")){
			endereco = new Endereco();
			endereco.setLogradouro(strEndereco);
			
			String strNumero = fieldSet.readString("NUMERO"); 
			Long numero = null;
			if(!strNumero.equals("")){
				try{
				numero = new Long(strNumero);
				}catch(NumberFormatException e){
					e.printStackTrace();
				}
			}
			endereco.setNumero(numero);
			endereco.setComplemento(fieldSet.readString("COMPLEMENTO"));
			endereco.setBairro(fieldSet.readString("BAIRRO"));
			endereco.setCep(fieldSet.readString("CEP"));
			endereco.setMunicipio(fieldSet.readString("MUNICIPIO"));
			endereco.setUf(fieldSet.readString("UF"));
			pessoa.setEndereco(endereco);
		}
			
		//contato
		String strTelefone = fieldSet.readString("TELEFONE");
		Contato contato = null;
		Telefone telefone = null;
		if(strTelefone != null && !strTelefone.equals("")){
			contato = new Contato();
			telefone = new Telefone();
			telefone.setDdd(fieldSet.readString("DDD").equals("")? null: fieldSet.readInt("DDD"));
			telefone.setNumero(strTelefone);
			telefone.setRamal(fieldSet.readString("RAMAL").equals("")? null: fieldSet.readLong("RAMAL"));
			contato.setTelefone(telefone);
			contato.setEmail(fieldSet.readString("EMAIL"));
			pessoa.setContato(contato);
		}
		
		//PK Composta
		pessoa.setTipoPessoa(fieldSet.readString("TIPO_PES"));
		pessoa.setCpfCnpj(fieldSet.readString("CPF_CNPJ"));
		pessoa.setFilial(fieldSet.readString("FILIAL"));
		pessoa.setDigitoCpfCnpj(fieldSet.readString("DIGITO"));
		pessoa.setId(fieldSet.readString("ID_PESSOA").equals("")? null: fieldSet.readLong("ID_PESSOA"));
				
		
		//dados titular
		pessoa.setTitularidade(fieldSet.readString("IDENTIFICACAO"));
		pessoa.setNome(fieldSet.readString("NOME"));
		String dataNascimento = fieldSet.readString("DT_NAS");
		pessoa.setDataNascimento(DateUtil.validaData(dataNascimento)? null: fieldSet.readDate("DT_NAS", "dd/MM/yyyy"));
		pessoa.setSexo(fieldSet.readString("SEXO"));

		

		return pessoa;
	}
}