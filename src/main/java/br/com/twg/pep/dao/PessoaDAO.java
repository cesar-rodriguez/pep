package br.com.twg.pep.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.entity.PessoaPK;
@Component
public interface PessoaDAO {

	 List<Pessoa> buscaPessoasPorCpfCnpj(Set<String> listaCpfCnpj) throws DataAccessException ;
	 void removePessoas();
	 void recuperaDadosHistoricos();
	 List<Pessoa> buscaTodasPessoas() throws DataAccessException;
	 void insereDadosHistoricos(Date dataHistorico);
	 List<Pessoa> buscaPessoasSemProdutos();
	 Pessoa buscaPessoasPorId(PessoaPK pessoaPk);
}