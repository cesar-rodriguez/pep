package br.com.twg.pep.processor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.twg.pep.dao.SinistroDAO;
import br.com.twg.pep.entity.ProdutoSegurado;
import br.com.twg.pep.entity.Sinistro;
import br.com.twg.pep.enumerador.CompanyEnum;

public class ProdutoSinistroProcessor  implements ItemProcessor<ProdutoSegurado, List<Sinistro>> {

	@Autowired
	private SinistroDAO sinistroDao;
	
	@Override
	public List<Sinistro> process(ProdutoSegurado produto) throws Exception {

		List<Sinistro> sinistros = null;
		List<Sinistro> sinistrosAux = new ArrayList<Sinistro>();
		for(CompanyEnum comapnyEnum: CompanyEnum.values()){
			System.out.println("PARAMETROS ---------------> " );
			System.out.println("COMPANY ------------------> " + comapnyEnum.getCompanyId().shortValue());
			System.out.println("NUMERO CERTIFICADO -------> " + produto.getNumeroCertificado());
			sinistros = sinistroDao.buscaSinistroPor(comapnyEnum.getCompanyId().shortValue(), produto.getNumeroCertificado());
			
			if(sinistros != null && !sinistros.isEmpty()){
				System.out.println("TAMANHO LISTA ------------> " + sinistros.size());
				System.out.println("PRODUTOS SEGURADOS--------> " + sinistros);
				for(Sinistro sinistro: sinistros){
					sinistro.setProduto(produto);	
					sinistrosAux.add(sinistro);
				}
			}
		}
		return sinistrosAux;
	}	
}
