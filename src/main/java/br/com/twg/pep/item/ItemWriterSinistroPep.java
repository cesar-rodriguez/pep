package br.com.twg.pep.item;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.twg.pep.dao.SinistroDAO;
import br.com.twg.pep.entity.Sinistro;

public class ItemWriterSinistroPep implements ItemWriter<List<Sinistro>>{
	
	@Autowired
	private SinistroDAO dao;

	@Override
	public void write(List<? extends List<Sinistro>> items) throws Exception {
		List<Sinistro> auxSinistros = new ArrayList<Sinistro>();
		
		for(List<Sinistro> sinistros: items){
			if(sinistros != null && !sinistros.isEmpty()){
				auxSinistros.addAll(sinistros);
			}
		}
		if(auxSinistros != null && !auxSinistros.isEmpty()){
			dao.salvar(auxSinistros);
		}
	}
}