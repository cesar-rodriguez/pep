package br.com.twg.pep.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;

import br.com.twg.pep.entity.Cancelamento;
import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.entity.ProdutoSegurado;
import br.com.twg.pep.processor.ProdutoSeguradoProcessor;
import br.com.twg.pep.util.DateUtil;

public class ProdutoSeguradoDAOImpl implements ProdutoSeguradoDAO {

	@PersistenceUnit(unitName="PEPPU")
	private EntityManagerFactory entityManagerFactory;
	
	@PersistenceUnit(unitName="WLSPU")
	private EntityManagerFactory entityManagerFactoryWLS;
	
	@Autowired
	private CancelamentoDAO cancelamentoDao;
	
	@Override
	public ProdutoSegurado salvar(ProdutoSegurado produto){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
		return entityManager.merge(produto);
	}
	
	@Override
	public List<ProdutoSegurado>salvar(List<ProdutoSegurado> produtos){
		List<ProdutoSegurado> auxProdutos = new ArrayList<ProdutoSegurado>();
		for(ProdutoSegurado produto: produtos){
			auxProdutos.add(salvar(produto));
		}
		return auxProdutos;
	}
	
	private void estruturaAtualizacao(String strUpdate){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createNativeQuery(strUpdate);
    	query.executeUpdate();	
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ProdutoSegurado> buscaProdutoSeguradoPorNumeroContrato(List<String> listaNumeroContrato) throws DataAccessException {
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createQuery("select ps from ProdutoSegurado ps where ps.numeroCertificado in (:listaNumeroContrato)");
        query.setParameter("listaNumeroContrato", listaNumeroContrato);
        List<ProdutoSegurado> listaResultados = (List<ProdutoSegurado>)query.getResultList();
        return listaResultados;
	}

	@Override
	public void removeProdutoSegurado() {
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		estruturaAtualizacao("DELETE FROM "+schema+".PRODUTO_SEGURADO");
	}

	@Override
	public void moveDadosParaHistoricos(Date horaInicioJob) {
		estruturaAtualizacao(insertSelectHistorico(horaInicioJob));
	}
	
	private String insertSelectHistorico(Date horaInicioJob){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		
		StringBuilder sbInsert = new StringBuilder();
		sbInsert.append(" INSERT INTO "+schema+".HISTORICO_PRODUTO_SEGURADO(ID, DATA_CRIACAO, TIPO_PESSOA, CPF_CNPJ, DIGITO_CPF_CNPJ, FILIAL, STATUS, NUMERO_CERTIFICADO, SERVICO_CONTRATADO, VALOR_PREMIO, VALOR_PRODUTO, DATA_EMISSAO, INICIO_VIGENCIA, FIM_VIGENCIA, FABRICANTE, MODELO, MODELO_DEALER) ")
				.append(" SELECT ID, ")
				.append("'" + DateUtil.formataDataHistorico(horaInicioJob) +"'" + ", ")
				 .append(" TIPO_PESSOA, CPF_CNPJ, DIGITO_CPF_CNPJ, FILIAL, STATUS, NUMERO_CERTIFICADO, SERVICO_CONTRATADO, VALOR_PREMIO, VALOR_PRODUTO, DATA_EMISSAO, INICIO_VIGENCIA, FIM_VIGENCIA, FABRICANTE, MODELO, MODELO_DEALER ")
				.append(" FROM "+schema+".PRODUTO_SEGURADO ");
		
		return sbInsert.toString();
	}
	
	@Override
	public List<ProdutoSegurado> buscaProdutosSeguradoPor(short companyCode, String cpfCnpj){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactoryWLS);
		String schema = (String) entityManager.getEntityManagerFactory().getProperties().get("hibernate.default_schema");
		Query query = entityManager.createNativeQuery(produtoSeguradoNativeQuery(schema));
        query.setParameter("company", companyCode);
        query.setParameter("cpfCnpj", cpfCnpj);
        List<Object[]> listaResultados = (List<Object[]>)query.getResultList();
        if(listaResultados != null && !listaResultados.isEmpty()){
        	List<ProdutoSegurado> produtos = new ArrayList<ProdutoSegurado>();
        	ProdutoSeguradoProcessor processor = new ProdutoSeguradoProcessor();
        	for(Object[] objProduto: listaResultados){
        		try {
					produtos.add(processor.process(objProduto));
				} catch (Exception e) {
					e.printStackTrace();
				}
        	}
        	return produtos;
        }else{
        	return null;	
        }  
	}
	
	private String produtoSeguradoNativeQuery(String schema) {

		StringBuilder query = new StringBuilder();

		   query.append(" SELECT ")
		        .append(" CAST(NULL AS INTEGER) AS ID, ")
		        .append(" TRIM(CUS_ID) AS cpfCnpjTransient, ")
				.append(" CDT.CDT_STATUS AS STATUS, ")
				.append(" CDT.CDT_CUS_CONT_NBR AS NUMERO_CERTIFICADO,  ")
				.append(" CDT.CDT_PROD_ID AS SERVICO_CONTRATADO,  ")
				.append(" CDT.CDT_WRT_PUR_PRC AS VALOR_PREMIO, ")
				.append(" CDT.CDT_PROD_PUR_AMT AS VALOR_PRODUTO,  ")
				.append(" CDT.CDT_WARR_PUR_DATE AS DATA_EMISSAO, ")
				.append(" CDT.CDT_LIAB_BEGIN_DATE AS INICIO_VIGENCIA, ")
				.append(" CDT.CDT_LIAB_END_DATE AS FIM_VIGENCIA, ")
				.append(" MFG.MFG_NAT_MFG_NAME AS FABRICANTE, ")
				.append(" MDL.MDL_NAT_NAME AS MODELO, ")
				.append(" CDT.CDT_ORIG_DESC AS MODELO_DEALER  ")
				.append(" FROM "+schema+".CUSTOMER AS CUS  ")
				.append(" INNER JOIN "+schema+".CONTRACT_HEADER        CHH ON (CHH.CHH_COMPANY_CODE = CUS.CUS_COMPANY_CODE AND CHH.CHH_CUS_NBR = CUS_NBR) ")
				.append(" INNER JOIN "+schema+".CONTRACT_DETAIL        CDT ON (CDT.CDT_COMPANY_CODE = CHH.CHH_COMPANY_CODE and CDT.CDT_CHD_NBR = CHH.CHH_NBR) ")
				.append(" INNER JOIN "+schema+".MODEL                  MDL ON (MDL.MDL_CODE = CDT.CDT_MDL_CODE AND MDL.MDL_COMPANY_CODE = CDT.CDT_COMPANY_CODE AND MDL.MDL_CODE = CDT.CDT_MDL_CODE) ")
				.append(" INNER JOIN "+schema+".MFG                    MFG ON (MFG.MFG_COMPANY_CODE = MDL.MDL_COMPANY_CODE AND MFG.MFG_CODE = MDL.MDL_MFG_CODE AND CDT.CDT_MFG_CODE = MFG.MFG_CODE)  ")
				.append(" WHERE CDT.CDT_STATUS = 'A' ")
				.append(" AND CDT.CDT_COMPANY_CODE = :company ")
				.append(" AND CUS.CUS_ID = :cpfCnpj ")
		   		.append(" FOR FETCH ONLY WITH UR ");

		return query.toString();
	}

	@Override
	public List<ProdutoSegurado> buscaProdutosSeguradoCanceladoPor(short companyCode, String cpfCnpj) {
		
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactoryWLS);
		String schema = (String) entityManager.getEntityManagerFactory().getProperties().get("hibernate.default_schema");
		Query query = entityManager.createNativeQuery(produtoSeguradoCanceladoQuery(schema));
        query.setParameter("company", companyCode);
        query.setParameter("cpfCnpj", cpfCnpj);
        List<Object[]> listaResultados = (List<Object[]>)query.getResultList();
      
        if(listaResultados != null && !listaResultados.isEmpty()){
        	List<ProdutoSegurado> produtos = new ArrayList<ProdutoSegurado>();
        	for(Object[] objProduto: listaResultados){
        		try {
					produtos.add(convertCancelado(objProduto));
				} catch (Exception e) {
					e.printStackTrace();
				}
        	}
        	return produtos;
        }else{
        	return null;	
        }
	}
	
	
	public ProdutoSegurado convertCancelado(Object[] produtoSeguradoObj) throws Exception {
		
		ProdutoSegurado produtoSegurado = new ProdutoSegurado();
		
		produtoSegurado.setCpfCnpjTransient((String)produtoSeguradoObj[1]);
		produtoSegurado.setStatus(((Character)produtoSeguradoObj[2]).toString());
		produtoSegurado.setNumeroCertificado(((String)produtoSeguradoObj[3]));
		produtoSegurado.setServicoContratado((String)produtoSeguradoObj[4]);
		produtoSegurado.setValorPremio(((BigDecimal)produtoSeguradoObj[5]).doubleValue());
		produtoSegurado.setValorProduto(((BigDecimal)produtoSeguradoObj[6]).doubleValue());
		produtoSegurado.setDataEmissao((Date)produtoSeguradoObj[7]);
		produtoSegurado.setInicioVigencia((Date)produtoSeguradoObj[8]);
		produtoSegurado.setFimVigencia((Date)produtoSeguradoObj[9]);
		produtoSegurado.setFabricante((String)produtoSeguradoObj[10]);
		produtoSegurado.setModelo((String)produtoSeguradoObj[11]);
		produtoSegurado.setModeloDealer((String)produtoSeguradoObj[12]);
		
		Cancelamento cancelamento = new Cancelamento();
		cancelamento.setRazaoCancelamento((String)produtoSeguradoObj[13]);
		cancelamento.setValorReembolsado(((BigDecimal)produtoSeguradoObj[14]).doubleValue());
		
		Object[] cancelamentoData = cancelamentoDao.buscaDatasPagamentoPorNumeroCertificado(produtoSegurado.getNumeroCertificado());
		if(cancelamentoData != null){
			cancelamento.setDataVencimento(cancelamentoData[1] == null? null: (Date)cancelamentoData[1]);
			cancelamento.setDataPagamento(cancelamentoData[2] == null? null: (Date) cancelamentoData[2]);
		}
		
		produtoSegurado.setCancelamento(cancelamento);
		cancelamento.setProduto(produtoSegurado);
		
		System.out.println("PRODUTO_SEGURADO ---------> " + produtoSegurado);
		System.out.println("CANCELAMENTO -------------> " + produtoSegurado.getCancelamento());
	
		return produtoSegurado;
	}
	
	
	private String produtoSeguradoCanceladoQuery(String schema) {
		
		StringBuilder query = new StringBuilder();
		
		query.append(" SELECT ")
		.append(" CAST(NULL AS INTEGER) AS id, ")
		.append(" TRIM(CUS_ID) AS cpfCnpjTransient, ")
		.append(" CDT.CDT_STATUS AS STATUS, ")
		.append(" CDT.CDT_CUS_CONT_NBR AS NUMERO_CERTIFICADO, ")
		.append(" CDT.CDT_PROD_ID AS SERVICO_CONTRATADO,  ")
		.append(" CDT.CDT_WRT_PUR_PRC AS VALOR_PREMIO, ")
		.append(" CDT.CDT_PROD_PUR_AMT AS VALOR_PRODUTO, ")
		.append(" CDT.CDT_WARR_PUR_DATE AS DATA_EMISSAO, ")
		.append(" CDT.CDT_LIAB_BEGIN_DATE AS INICIO_VIGENCIA, ")
		.append(" CDT.CDT_LIAB_END_DATE AS FIM_VIGENCIA, ")
		.append(" MFG.MFG_NAT_MFG_NAME AS FABRICANTE, ")
		.append(" MDL.MDL_NAT_NAME AS MODELO, ")
		.append(" CDT.CDT_ORIG_DESC AS MODELO_DEALER, ")
		.append(" CRN.CRN_NAT_DESC AS MOTIVO_CANCELAMENTO, ")
		.append(" ATR.ATR_AMOUNT AS VALOR_REEMBOLSO ")
		.append(" FROM " + schema + ".CUSTOMER AS CUS  ")
		.append(" INNER JOIN " + schema + ".CONTRACT_HEADER        CHH ON (CHH.CHH_COMPANY_CODE = CUS.CUS_COMPANY_CODE AND CHH.CHH_CUS_NBR = CUS_NBR) ")
		.append(" INNER JOIN " + schema + ".CONTRACT_DETAIL        CDT ON (CDT.CDT_COMPANY_CODE = CHH.CHH_COMPANY_CODE and CDT.CDT_CHD_NBR = CHH.CHH_NBR) ")
		.append(" INNER JOIN " + schema + ".MODEL                  MDL ON (MDL.MDL_CODE = CDT.CDT_MDL_CODE AND MDL.MDL_COMPANY_CODE = CDT.CDT_COMPANY_CODE AND MDL.MDL_CODE = CDT.CDT_MDL_CODE) ")
		.append(" INNER JOIN " + schema + ".MFG                    MFG ON (MFG.MFG_COMPANY_CODE = MDL.MDL_COMPANY_CODE AND MFG.MFG_CODE = MDL.MDL_MFG_CODE AND CDT.CDT_MFG_CODE = MFG.MFG_CODE) ")
		.append(" INNER JOIN " + schema + ".CANCELLATION_REASON CRN ON (CRN.CRN_COMPANY_CODE = CDT.CDT_COMPANY_CODE AND CRN.CRN_CODE = CDT.CDT_CRN_CODE) ")
		.append(" INNER JOIN " + schema + ".ACCOUNTING_TRANSACTION ATR ON (ATR.ATR_COMPANY_CODE = CDT.CDT_COMPANY_CODE AND ATR.ATR_CHD_NBR = CDT.CDT_CHD_NBR AND ATR.ATR_CDT_SEQ = CDT.CDT_SEQ AND ATR_ACTG_CAT_CODE = 9 AND ATR_ACTG_SUB_CAT_CODE = 2) ")
		.append(" WHERE CDT.CDT_STATUS = 'C' ")
		.append(" AND CDT.CDT_COMPANY_CODE = :company ")
		.append(" AND CDT_CAN_DATE BETWEEN  (CURRENT_DATE - 1 MONTHS) AND CURRENT_DATE ")
		.append(" AND CUS.CUS_ID = :cpfCnpj ")
		.append(" FOR FETCH ONLY WITH UR ");
		return query.toString();
	}
	
	@Override
	public List<ProdutoSegurado> buscaProdutosSeguradoExpiradosPor(short companyCode, String cpfCnpj){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactoryWLS);
		String schema = (String) entityManager.getEntityManagerFactory().getProperties().get("hibernate.default_schema");
		Query query = entityManager.createNativeQuery(produtoSeguradoExpiradosQuery(schema));
        query.setParameter("company", companyCode);
        query.setParameter("cpfCnpj", cpfCnpj);
        List<Object[]> listaResultados = (List<Object[]>)query.getResultList();
        if(listaResultados != null && !listaResultados.isEmpty()){
        	List<ProdutoSegurado> produtos = new ArrayList<ProdutoSegurado>();
        	ProdutoSeguradoProcessor processor = new ProdutoSeguradoProcessor();
        	for(Object[] objProduto: listaResultados){
        		try {
					produtos.add(processor.process(objProduto));
				} catch (Exception e) {
					e.printStackTrace();
				}
        	}
        	return produtos;
        }else{
        	return null;	
        }
	}
	
	private String produtoSeguradoExpiradosQuery(String schema) {
		
		StringBuilder query = new StringBuilder();
		query.append(" SELECT ")
        .append(" TRIM(CUS_ID) AS cpfCnpjTransient, ")
		.append(" CDT.CDT_STATUS AS STATUS, ")
		.append(" CDT.CDT_CUS_CONT_NBR AS NUMERO_CERTIFICADO,  ")
		.append(" CDT.CDT_PROD_ID AS SERVICO_CONTRATADO,  ")
		.append(" CDT.CDT_WRT_PUR_PRC AS VALOR_PREMIO, ")
		.append(" CDT.CDT_PROD_PUR_AMT AS VALOR_PRODUTO,  ")
		.append(" CDT.CDT_WARR_PUR_DATE AS DATA_EMISSAO, ")
		.append(" CDT.CDT_LIAB_BEGIN_DATE AS INICIO_VIGENCIA, ")
		.append(" CDT.CDT_LIAB_END_DATE AS FIM_VIGENCIA, ")
		.append(" MFG.MFG_NAT_MFG_NAME AS FABRICANTE, ")
		.append(" MDL.MDL_NAT_NAME AS MODELO, ")
		.append(" CDT.CDT_ORIG_DESC AS MODELO_DEALER  ")
		.append(" FROM "+schema+".CUSTOMER AS CUS  ")
		.append(" INNER JOIN "+schema+".CONTRACT_HEADER        CHH ON (CHH.CHH_COMPANY_CODE = CUS.CUS_COMPANY_CODE AND CHH.CHH_CUS_NBR = CUS_NBR) ")
		.append(" INNER JOIN "+schema+".CONTRACT_DETAIL        CDT ON (CDT.CDT_COMPANY_CODE = CHH.CHH_COMPANY_CODE and CDT.CDT_CHD_NBR = CHH.CHH_NBR) ")
		.append(" INNER JOIN "+schema+".MODEL                  MDL ON (MDL.MDL_CODE = CDT.CDT_MDL_CODE AND MDL.MDL_COMPANY_CODE = CDT.CDT_COMPANY_CODE AND MDL.MDL_CODE = CDT.CDT_MDL_CODE) ")
		.append(" INNER JOIN "+schema+".MFG                    MFG ON (MFG.MFG_COMPANY_CODE = MDL.MDL_COMPANY_CODE AND MFG.MFG_CODE = MDL.MDL_MFG_CODE AND CDT.CDT_MFG_CODE = MFG.MFG_CODE)  ")
		.append(" WHERE CDT.CDT_STATUS = 'E' ")
		.append(" AND CDT.CDT_COMPANY_CODE = :company ")
		.append(" AND CDT_LIAB_END_DATE BETWEEN  (CURRENT_DATE - 1 YEARS) AND CURRENT_DATE ") 
		.append(" AND CUS.CUS_ID = :cpfCnpj ")
		.append(" FOR FETCH ONLY WITH UR ");
		
		return query.toString();
	}

	@Override
	public List<ProdutoSegurado> buscaProdutoSeguradoPor(Pessoa pessoa) {
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createQuery("select ps from ProdutoSegurado ps where ps.pessoa = :pessoa");
        query.setParameter("pessoa", pessoa);
        List<ProdutoSegurado> listaResultados = (List<ProdutoSegurado>)query.getResultList();
        return listaResultados;
	}
}