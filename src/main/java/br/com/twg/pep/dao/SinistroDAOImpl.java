package br.com.twg.pep.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.orm.jpa.EntityManagerFactoryUtils;

import br.com.twg.pep.entity.ProdutoSegurado;
import br.com.twg.pep.entity.Sinistro;
import br.com.twg.pep.processor.SinistroProcessor;
import br.com.twg.pep.util.DateUtil;

public class SinistroDAOImpl implements SinistroDAO{

	@PersistenceUnit(unitName="PEPPU")
	private EntityManagerFactory entityManagerFactory;
	
	@PersistenceUnit(unitName="WLSPU")
	private EntityManagerFactory entityManagerFactoryWLS;
	
	@Override
	public Sinistro salvar(Sinistro sinistro){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
		return entityManager.merge(sinistro);
	}
	
	@Override
	public List<Sinistro>salvar(List<Sinistro> sinistros){
		List<Sinistro> auxSinistros = new ArrayList<Sinistro>();
		for(Sinistro sinistro: sinistros){
			auxSinistros.add(salvar(sinistro));
		}
		return auxSinistros;
	}
	
	private void estruturaAtualizacao(String strAtualizacao){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createNativeQuery(strAtualizacao);
    	query.executeUpdate();	
	}
	
	public void removeSinistros(){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		estruturaAtualizacao("DELETE FROM "+schema+".SINISTRO");
	}
	
	public void moveDadosParaHistoricos(Date dataHistorico){
		estruturaAtualizacao(insertSelectHistorico(dataHistorico));
	}
	
	private String insertSelectHistorico(Date dataHistorico){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		
		StringBuilder sbInsert = new StringBuilder();
		sbInsert.append(" INSERT INTO "+schema+".HISTORICO_SINISTRO(ID, DATA_CRIACAO, PRODUTO_SEGURADO_ID, NUMERO_SINISTRO, STATUS, DATA_ENTRADA, VALOR_TOTAL, REEMBOLSADO, VALOR_REEMBOLSADO) ")
				.append(" SELECT ID, ")
				.append( "'"+DateUtil.formataDataHistorico(dataHistorico) + "'" +", ")
				.append(" PRODUTO_SEGURADO_ID, NUMERO_SINISTRO, STATUS, DATA_ENTRADA, VALOR_TOTAL, REEMBOLSADO, VALOR_REEMBOLSADO ") 
				.append(" FROM "+schema+".SINISTRO ");
		
		return sbInsert.toString();
	}
	
	@Override
	public List<Sinistro> buscaSinistroPor(short companyCode, String numeroContrato){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactoryWLS);
		String schema = (String) entityManager.getEntityManagerFactory().getProperties().get("hibernate.default_schema");
		Query query = entityManager.createNativeQuery(nativeQuery(schema));
        query.setParameter("company", companyCode);
        query.setParameter("numeroContrato", numeroContrato);
        List<Object[]> listaResultados = (List<Object[]>)query.getResultList();
        if(listaResultados != null && !listaResultados.isEmpty()){
        	List<Sinistro> sinistros = new ArrayList<Sinistro>();
        	SinistroProcessor processor = new SinistroProcessor();
        	for(Object[] objProduto: listaResultados){
        		try {
        			sinistros.add(processor.process(objProduto));
				} catch (Exception e) {
					e.printStackTrace();
				}
        	}
        	return sinistros;
        }else{
        	return null;	
        }
	}
	
	private String nativeQuery(String schema) {

		StringBuilder query = new StringBuilder();

		query.append(" SELECT ")
				.append(" CDT.CDT_CUS_CONT_NBR AS NUMERO_CERTIFICADO, ")
				.append(" ROH.ROH_ID       AS NUMERO_SINISTRO, ")
				.append(" ROH.ROH_ROS_CODE AS CODIGO_STATUS_SINISTRO, ")
				.append(" SUM(RWD_REQUEST_AMOUNT + RWD_ADJUST_AMOUNT + RWD_TAX_AMOUNT) AS TOTAL_SINISTRO, ")
				.append(" RED.RED_DATE     AS BREAKDOWN_DATE, ")
				.append(" CASE ")
				.append(" WHEN REEMBOLSO.TOTAL_REEMBOLSO IS NULL  THEN 'N�O' ")
				.append(" ELSE 'SIM' ")
				.append(" END AS REEMBOLSO_SEGURADO , ")
				.append(" REEMBOLSO.TOTAL_REEMBOLSO ")
				.append(" FROM  " + schema + ".CUSTOMER CUS  ")
				.append(" INNER JOIN  " + schema + ".CONTRACT_HEADER    CHH ON (CHH.CHH_COMPANY_CODE = CUS.CUS_COMPANY_CODE AND CHH.CHH_CUS_NBR = CUS.CUS_NBR ) ")
				.append(" INNER JOIN  " + schema + ".CONTRACT_DETAIL    CDT ON (CDT.CDT_COMPANY_CODE = CHH.CHH_COMPANY_CODE AND CDT.CDT_CHD_NBR = CHH.CHH_NBR) ")
				.append(" INNER JOIN  " + schema + ".RO_HEADER          ROH ON (ROH.ROH_COMPANY_CODE = CDT.CDT_COMPANY_CODE AND ROH.ROH_CHD_NBR = CDT.CDT_CHD_NBR AND ROH.ROH_CDT_SEQ = CDT.CDT_SEQ) ")
				.append(" INNER JOIN  " + schema + ".RO_EVENT           ROE ON (ROE.ROE_ROH_ID = ROH.ROH_ID)")
				.append(" INNER  JOIN  " + schema + ".RO_EVENT_DATE      RED ON (RED.RED_ROE_ID = ROE.ROE_ID  AND RED.RED_RDT_CODE = 2 ) ")
				.append(" LEFT JOIN  " + schema + ".RO_WARRANTY_DETAIL RWD ON (RWD.RWD_ROE_ID = ROE.ROE_ID) ")
				.append(" LEFT JOIN  ( ")
				.append(" SELECT ROE.ROE_COMPANY_CODE,  ROE.ROE_ROH_ID, ROE.ROE_CLI_NBR, SUM(RWD_REQUEST_AMOUNT + RWD_ADJUST_AMOUNT + RWD_TAX_AMOUNT) AS TOTAL_REEMBOLSO ")
				.append(" FROM " + schema + ".RO_EVENT           ROE ")
				.append(" INNER JOIN  " + schema + ".RO_WARRANTY_DETAIL RWD ON (RWD.RWD_ROE_ID = ROE.ROE_ID) ")
				.append(" WHERE (ROE.ROE_CLI_NBR =  242 OR ROE.ROE_CLI_NBR = 128) ")
				.append(" GROUP BY  ROE.ROE_COMPANY_CODE,  ROE.ROE_ROH_ID, ROE_CLI_NBR ")
				.append(" ) AS REEMBOLSO ON (REEMBOLSO.ROE_COMPANY_CODE = ROE.ROE_COMPANY_CODE AND REEMBOLSO.ROE_ROH_ID = ROE.ROE_ROH_ID AND REEMBOLSO.ROE_CLI_NBR = ROE.ROE_CLI_NBR) ")
				.append(" WHERE CDT.CDT_STATUS IN ('A', 'C')  ")
				.append(" AND CDT.CDT_COMPANY_CODE = :company ")
				.append(" AND CDT.CDT_CUS_CONT_NBR = :numeroContrato ")
				.append(" GROUP BY CDT.CDT_CUS_CONT_NBR, ROH.ROH_ID, ROH.ROH_ROS_CODE, RED.RED_DATE, REEMBOLSO.TOTAL_REEMBOLSO ")
				.append(" FOR FETCH ONLY WITH UR ");

		return query.toString();
	}

	@Override
	public List<Sinistro> buscaSinistroPor(ProdutoSegurado produto) {
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createQuery("select sin from Sinistro sin where sin.produto = :produto");
        query.setParameter("produto", produto);
        return  query.getResultList(); 
	}

}