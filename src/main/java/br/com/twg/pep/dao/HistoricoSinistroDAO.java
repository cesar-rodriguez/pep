package br.com.twg.pep.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.twg.pep.entity.HistoricoProdutoSegurado;
import br.com.twg.pep.entity.HistoricoSinistro;
@Component(value="historicoSinistroDao")
public interface HistoricoSinistroDAO {
	List<HistoricoSinistro> buscaSinistroPor(HistoricoProdutoSegurado produto, Date dataCriacao);
}
