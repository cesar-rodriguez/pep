package br.com.twg.pep.dao;

import java.util.Date;

import br.com.twg.pep.entity.HistoricoCargo;

public interface HistoricoCargoDAO {
	HistoricoCargo buscaCargoPorId(Long id, Date dataCriacao);
}
