package br.com.twg.pep.tasklet;

import java.io.File;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

public class RemoveFilesTasklet implements Tasklet {

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		new File("\\\\BRSA2K8FS01\\pep\\Orgaos.txt").delete();
		new File("\\\\BRSA2K8FS01\\pep\\Cargos.txt").delete();
		new File("\\\\BRSA2K8FS01\\pep\\PEP.txt").delete();
	
		return RepeatStatus.FINISHED;
	}
}