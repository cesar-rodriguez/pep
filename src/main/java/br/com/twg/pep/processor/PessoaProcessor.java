package br.com.twg.pep.processor;

import org.springframework.batch.core.StepExecution;

import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.util.ValidateUtils;

public class PessoaProcessor implements PepLayoutProcessor<Pessoa>{

	public void processor(Pessoa pessoa, StepExecution stepExecution) throws Exception {
		ValidateUtils.validarPessoaId(pessoa.getTipoPessoa(), pessoa.getCpfCnpj(), pessoa.getDigitoCpfCnpj(), pessoa.getFilial(), pessoa.getId(), stepExecution.getReadCount());
		alteraCEP(pessoa);
		System.out.println("PESSOA -------------------> " + pessoa);
	}
	
	private void alteraCEP(Pessoa pessoa){
		if(pessoa.getEndereco() != null 
				&& pessoa.getEndereco().getCep() != null
				&& pessoa.getEndereco().getCep().trim().equals("")){
			pessoa.getEndereco().setCep(null);
		}
	}
}
