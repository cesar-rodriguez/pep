package br.com.twg.pep.aspect;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.hibernate.exception.ConstraintViolationException;

import br.com.twg.pep.exception.PEPException;
import br.com.twg.pep.util.EmailUtil;
import br.com.twg.pep.util.PropertiesUtils;
import br.twg.bellmail.util.MailUtil;

public class ErrorAspect {
	
	public void sendEMailAfterThrowing(JoinPoint joinPoint, Throwable throwable) {

		if(throwable.getCause() instanceof ConstraintViolationException){
			return;
		}
		
		try {		
			String emailTitle = getEmailTitle(throwable);
			String caused = getCausedException(throwable);
			String message = getEmailMessage(throwable.getClass(), caused);
			
			MailUtil.enviaEmail(emailTitle, EmailUtil.emailsTo(), EmailUtil.emailsFrom(), message);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getEmailTitle(Throwable throwable) {
		try {
			if (throwable instanceof PEPException) {
				return PropertiesUtils.getProperty("pep.titulo.aviso");
			}
			return PropertiesUtils.getProperty("pep.titulo.erro");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private String getCausedException(Throwable throwable){
		if(throwable.getCause() == null){
			return throwable.getMessage();
		}
		return throwable.getCause().getMessage();
	}
	
	private String getEmailMessage(Class<? extends Throwable> classThrowable, String caused){
		StringBuilder sbMessage = new StringBuilder();
		sbMessage.append(" O problema ")
				 .append(classThrowable) 
		         .append(" ocorreu em ")
		         .append(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()))
		         .append(". Causa: ")
				 .append(caused);
		
		return sbMessage.toString();
	}
}