package br.com.twg.pep.exception;

public class PEPException extends Exception {

	private static final long serialVersionUID = -7694653536475643402L;
 
	private Object[] params;
 
    public PEPException(final String message) {
        super(message);
    }

    public PEPException(final String message, final Object... params) {
        super(message);
        this.params = params;
    }

    public PEPException(final Throwable cause) {
        super(cause);
    }

    public Object[] getParams() {
        return params;
    }

}
