package br.com.twg.pep.dao;

import java.util.Date;
import java.util.List;

import br.com.twg.pep.entity.Cancelamento;
import br.com.twg.pep.entity.ProdutoSegurado;

public interface CancelamentoDAO {
	
	List<Object[]> buscaDatasPagamentoPorNumeroCertificado(List<String> numerosCertificados);
	Object[] buscaDatasPagamentoPorNumeroCertificado(String numeroCertificado);
	void removeCancelamentos();
	void moveDadosParaHistoricos(Date horaInicioJob);
	Cancelamento buscaCancelamentoPor(ProdutoSegurado produto);
	
}
