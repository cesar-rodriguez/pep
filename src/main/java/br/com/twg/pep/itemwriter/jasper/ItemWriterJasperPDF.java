package br.com.twg.pep.itemwriter.jasper;

import java.io.File;
import java.io.IOException;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

import org.springframework.core.io.Resource;

import br.com.twg.pep.item.ItemWriterJasper;

public class ItemWriterJasperPDF implements ItemWriterJasper{

	@Override
	public void exportaArquivoJasper(final JasperPrint print, final Resource resourceExport) throws IOException, JRException {
		
		File arquivoExportado =  resourceExport.getFile();
		JasperExportManager.exportReportToPdfFile(print, arquivoExportado.getAbsolutePath());
	}
}
