package br.com.twg.pep.dao;

import java.util.Date;

import br.com.twg.pep.entity.Orgao;

public interface OrgaoDAO {
	
	void recuperaDadosHistoricos();
	void removeOrgaos();
	void insereDadosHistoricos(Date dataHistorico);
	Orgao buscaOrgaoPorId(Long id);

}
