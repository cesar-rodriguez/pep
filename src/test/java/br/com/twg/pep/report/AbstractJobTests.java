package br.com.twg.pep.report;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Date;

import org.junit.Test;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(locations = { "/job-launcher-db2400/simple-job-launcher-context.xml", "/job-runner-context.xml" })
public abstract class AbstractJobTests {

	@Autowired
	private JobLauncherTestUtils jobLauncherTestUtils;
	
	@Test
	public void testJob() throws Exception {
		assertEquals(BatchStatus.COMPLETED, jobLauncherTestUtils.launchJob(getJobParametersFromJobMap()).getStatus());
	}
	
	private JobParameters getJobParametersFromJobMap() throws IOException {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addLong("param", new Date().getTime());
		return builder.toJobParameters();
	}
}
