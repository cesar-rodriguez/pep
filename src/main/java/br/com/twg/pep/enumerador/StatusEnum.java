package br.com.twg.pep.enumerador;

public enum StatusEnum {
	
	OPEN					 (1 ,  "Aberto"						 , "A"),
	PENDING					 (2,  "Pendente"					 , "P"),
//	COMPLETED_PAYMENT_PENDING(3,  "Pagamento Conclu�do Pendente" , "PCP"),
	PAID					 (4,  "Pago"						 , "PG"),
	PAYMENT_ORDERED			 (5,  "Pagamento Requerido"		 	 , "PR"),
	PARTIALLY_PAID			 (6,  "Parcialmente Pago"			 , "PP"),
	REJECTED				 (7,  "Rejeitado"					 , "R"),
//	REJECTED_UNDER_SIR		 (8,  ""							 , ""),
	CLOSED					 (11, "Fechado"					 	 , "F");
	
	private Integer statusId;
	private String descricao;
	private String sigla;
	
	private StatusEnum(final Integer statusId,final String descricao, final String sigla) {
		this.statusId = statusId;
		this.descricao = descricao;
		this.sigla = sigla;
	}

	public static StatusEnum buscaPorStatusId(final Short statusId) {
		for (StatusEnum s : StatusEnum.values()) {
			if (s.statusId == statusId.intValue()) {
				return s;
			}
		}
		return null;
	}
	
	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}