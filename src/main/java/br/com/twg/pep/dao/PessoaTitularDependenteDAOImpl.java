package br.com.twg.pep.dao;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.orm.jpa.EntityManagerFactoryUtils;

import br.com.twg.pep.entity.PessoaTitularDependente;
import br.com.twg.pep.entity.PessoaTitularDependentePK;
import br.com.twg.pep.util.DateUtil;

public class PessoaTitularDependenteDAOImpl implements PessoaTitularDependenteDAO{
	
	@PersistenceUnit(unitName="PEPPU")
	private EntityManagerFactory entityManagerFactory;
	
	public void removePessoas(){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		estruturaAtualizacao("DELETE FROM "+schema+".PESSOA_TITULAR_DEPENDENTE");
	}
	
	public void recuperaDadosHistoricos(){
		estruturaAtualizacao(queryRecuperaHistorico());
	}
	
	public void insereDadosHistoricos(Date horaInicioJob){
		estruturaAtualizacao(queryInsereHistorico(horaInicioJob));
	}
	
	private String queryInsereHistorico(Date horaInicioJob){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append(
				" INSERT INTO "+schema+".HISTORICO_PESSOA_TITULAR_DEPENDENTE(DEPENDENTE_ID, DEPENDENTE_TIPO_PESSOA, DEPENDENTE_CPF_CNPJ, DEPENDENTE_DIGITO_CPF_CNPJ, DEPENDENTE_FILIAL, TITULAR_ID, TITULAR_TIPO_PESSOA, TITULAR_CPF_CNPJ, TITULAR_DIGITO_CPF_CNPJ, TITULAR_FILIAL, DATA_CRIACAO, NOME_TITULAR, GRAU_RELACIONAMENTO) ")
				.append(" SELECT DEPENDENTE_ID, DEPENDENTE_TIPO_PESSOA, DEPENDENTE_CPF_CNPJ, DEPENDENTE_DIGITO_CPF_CNPJ, DEPENDENTE_FILIAL, TITULAR_ID, TITULAR_TIPO_PESSOA, TITULAR_CPF_CNPJ, TITULAR_DIGITO_CPF_CNPJ, TITULAR_FILIAL, ")
				.append("'" + DateUtil.formataDataHistorico(horaInicioJob) +"'" + ", ")
				.append("NOME_TITULAR, GRAU_RELACIONAMENTO ")
				.append(" FROM "+schema+".PESSOA_TITULAR_DEPENDENTE ");

		return sbQuery.toString();
	}
	
	private String queryRecuperaHistorico(){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		
		StringBuilder sbInsert = new StringBuilder();
		sbInsert.append(" INSERT INTO "+schema+".PESSOA_TITULAR_DEPENDENTE (DEPENDENTE_ID, DEPENDENTE_TIPO_PESSOA, DEPENDENTE_CPF_CNPJ, DEPENDENTE_DIGITO_CPF_CNPJ, DEPENDENTE_FILIAL, TITULAR_ID, TITULAR_TIPO_PESSOA, TITULAR_CPF_CNPJ, TITULAR_DIGITO_CPF_CNPJ, TITULAR_FILIAL, NOME_TITULAR, GRAU_RELACIONAMENTO) ")
				.append(" SELECT DEPENDENTE_ID, DEPENDENTE_TIPO_PESSOA, DEPENDENTE_CPF_CNPJ, DEPENDENTE_DIGITO_CPF_CNPJ, DEPENDENTE_FILIAL, TITULAR_ID, TITULAR_TIPO_PESSOA, TITULAR_CPF_CNPJ, TITULAR_DIGITO_CPF_CNPJ, TITULAR_FILIAL, NOME_TITULAR, GRAU_RELACIONAMENTO ")
				.append(" FROM "+schema+".HISTORICO_PESSOA_TITULAR_DEPENDENTE ")
				.append(" WHERE DATA_CRIACAO = (SELECT MAX(DATA_CRIACAO) FROM "+schema+".HISTORICO_PESSOA_TITULAR_DEPENDENTE) ");
		
		return sbInsert.toString();
	}
	
	public PessoaTitularDependente buscaPessoaTitularDependentePorId(PessoaTitularDependentePK pessoaTitularDependentePk){
    	EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	return  entityManager.find(PessoaTitularDependente.class, pessoaTitularDependentePk);
    }

	private void estruturaAtualizacao(String strAtualizacao){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createNativeQuery(strAtualizacao);
    	query.executeUpdate();	
	}
}