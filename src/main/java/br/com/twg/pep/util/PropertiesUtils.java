package br.com.twg.pep.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtils {

		private static Properties props;

		public static Properties getLoadProperties(String propFileName) throws IOException {
			InputStream stream = PropertiesUtils.class.getResourceAsStream("/" + propFileName);
			if (stream == null) {
				throw new RuntimeException("N�o foi possivel carregar o arquivo: " + propFileName);
			}

			if (props == null) {
				props = new Properties();
				try {
					props.load(stream);
				} catch (IOException e) {
					throw new RuntimeException("N�o foi possivel carregar o arquivo: " + propFileName + "\n" + e.getMessage());
				}
			}
			return props;
		}
		
		public static String getProperty(String chave) throws IOException {
			return props.getProperty(chave);
		}
		

		public static String getProperty(String propFileName, String chave) throws IOException {
			return getLoadProperties(propFileName).getProperty(chave);
		}
		
}
