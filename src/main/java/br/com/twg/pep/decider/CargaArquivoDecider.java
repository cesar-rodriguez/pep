package br.com.twg.pep.decider;

import java.io.File;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;

public class CargaArquivoDecider implements JobExecutionDecider {

	@Override
	public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
		
		if (!existeTresArquivos()) {
			return new FlowExecutionStatus("COMPLETED WITH SKIPS");
		} else {
			return new FlowExecutionStatus(ExitStatus.COMPLETED.getExitCode());
		}
	}
	
	private boolean existeTresArquivos(){
		File fileOrgaos = new File("\\\\BRSA2K8FS01\\pep\\Orgaos.txt");
		File fileCargos = new File("\\\\BRSA2K8FS01\\pep\\Cargos.txt");
		File filePEP = new File("\\\\BRSA2K8FS01\\pep\\PEP.txt");
		if(fileOrgaos.exists() 
				&& fileCargos.exists()
				&& filePEP.exists()) {
			return true;
			}
	
		return false;
	}
}
