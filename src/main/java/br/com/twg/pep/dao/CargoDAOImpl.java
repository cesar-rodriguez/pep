package br.com.twg.pep.dao;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.orm.jpa.EntityManagerFactoryUtils;

import br.com.twg.pep.entity.Cargo;
import br.com.twg.pep.util.DateUtil;

public class CargoDAOImpl implements CargoDAO{
	
	@PersistenceUnit(unitName="PEPPU")
	private EntityManagerFactory entityManagerFactory;
	
	@Override
	public Cargo buscaCargoPorId(Long id){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
		return entityManager.find(Cargo.class, id);
	}
	
	private void estruturaAtualizacao(String strUpdate){
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createNativeQuery(strUpdate);
    	query.executeUpdate();	
	}
	
	public void removeCargos(){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		estruturaAtualizacao("DELETE FROM "+schema+".CARGO");
	}
	
	public void recuperaDadosHistoricos(){
		estruturaAtualizacao(queryRecuperaHistorico());	
	}
	
	public void insereDadosHistoricos(Date horaInicioJob){
		estruturaAtualizacao(queryInsereHistorico(horaInicioJob));	
	}
	
	
	private String queryRecuperaHistorico(){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		StringBuilder sbInsert = new StringBuilder();
		sbInsert.append(" INSERT INTO "+schema+".CARGO (ID, NOME, DATA_EXTINCAO, TRATAMENTO_MASCULINO, TRATAMENTO_FEMININO) ")
				.append(" SELECT  ID, NOME, DATA_EXTINCAO, TRATAMENTO_MASCULINO, TRATAMENTO_FEMININO ")    
				.append(" FROM "+schema+".HISTORICO_CARGO  ")
				.append(" WHERE DATA_CRIACAO = (SELECT MAX(DATA_CRIACAO) FROM "+schema+".HISTORICO_CARGO)  ");
		
		return sbInsert.toString();
	}
	
	private String queryInsereHistorico(Date horaInicioJob){
		String schema = (String) entityManagerFactory.getProperties().get("hibernate.default_schema");
		StringBuilder sbQuery = new StringBuilder();

		sbQuery.append(
				" INSERT INTO "+schema+".HISTORICO_CARGO(ID, DATA_CRIACAO, NOME, DATA_EXTINCAO, TRATAMENTO_MASCULINO, TRATAMENTO_FEMININO)")
				.append(" SELECT ID, ")
				.append("'" + DateUtil.formataDataHistorico(horaInicioJob) +"'" + ", ") 
				.append(" NOME, DATA_EXTINCAO, TRATAMENTO_MASCULINO, TRATAMENTO_FEMININO")
				.append(" FROM "+schema+".CARGO ");

		return sbQuery.toString();
	}
}