package br.com.twg.pep.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class HistoricoPessoaPK implements Serializable{
	
	private static final long serialVersionUID = 5442363506725671430L;
	
	private Long id;
	private String tipoPessoa;
	private String cpfCnpj;
	private String digitoCpfCnpj;
	private String filial;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpfCnpj == null) ? 0 : cpfCnpj.hashCode());
		result = prime * result
				+ ((digitoCpfCnpj == null) ? 0 : digitoCpfCnpj.hashCode());
		result = prime * result + ((filial == null) ? 0 : filial.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((tipoPessoa == null) ? 0 : tipoPessoa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoPessoaPK other = (HistoricoPessoaPK) obj;
		if (cpfCnpj == null) {
			if (other.cpfCnpj != null)
				return false;
		} else if (!cpfCnpj.equals(other.cpfCnpj))
			return false;
		if (digitoCpfCnpj == null) {
			if (other.digitoCpfCnpj != null)
				return false;
		} else if (!digitoCpfCnpj.equals(other.digitoCpfCnpj))
			return false;
		if (filial == null) {
			if (other.filial != null)
				return false;
		} else if (!filial.equals(other.filial))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (tipoPessoa == null) {
			if (other.tipoPessoa != null)
				return false;
		} else if (!tipoPessoa.equals(other.tipoPessoa))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getDigitoCpfCnpj() {
		return digitoCpfCnpj;
	}

	public void setDigitoCpfCnpj(String digitoCpfCnpj) {
		this.digitoCpfCnpj = digitoCpfCnpj;
	}

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

	@Override
	public String toString() {
		return "PessoaPK [id=" + id + ", tipoPessoa=" + tipoPessoa
				+ ", cpfCnpj=" + cpfCnpj + ", digitoCpfCnpj=" + digitoCpfCnpj
				+ ", filial=" + filial + "]";
	}
}