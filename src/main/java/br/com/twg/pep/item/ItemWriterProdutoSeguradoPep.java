package br.com.twg.pep.item;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.twg.pep.dao.ProdutoSeguradoDAO;
import br.com.twg.pep.entity.ProdutoSegurado;

public class ItemWriterProdutoSeguradoPep implements ItemWriter<List<ProdutoSegurado>>{
	
	@Autowired
	private ProdutoSeguradoDAO dao;

	@Override
	public void write(List<? extends List<ProdutoSegurado>> items) throws Exception {
		List<ProdutoSegurado> auxProdutos = new ArrayList<ProdutoSegurado>();
		
		for(List<ProdutoSegurado> produtos: items){
			if(produtos != null && !produtos.isEmpty()){
				auxProdutos.addAll(produtos);
			}
		}
		if(auxProdutos != null && !auxProdutos.isEmpty()){
			dao.salvar(auxProdutos);
		}
	}
}