package br.com.twg.pep.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "HISTORICO_PESSOA")
@IdClass(HistoricoPessoaPK.class)
public class HistoricoPessoa implements Serializable {

	private static final long serialVersionUID = 6740537742673656156L;

	@Id
	@Column(name = "ID")
	private Long id;

	@Id
	@Column(name = "TIPO_PESSOA")
	private String tipoPessoa;

	@Id
	@Column(name = "CPF_CNPJ")
	private String cpfCnpj;

	@Id
	@Column(name = "DIGITO_CPF_CNPJ")
	private String digitoCpfCnpj;

	@Id
	@Column(name = "FILIAL")
	private String filial;

	@Column(name = "TITULARIDADE")
	private String titularidade;

	@Column(name = "NOME")
	private String nome;

	@Column(name = "DATA_NASCIMENTO")
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;
	
	@Column(name = "DATA_CRIACAO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCriacao;

	@Column(name = "SEXO")
	private String sexo;

	private HistoricoEndereco endereco;

	private HistoricoContato contato;

	@OneToMany(mappedBy = "pessoa", fetch = FetchType.LAZY)
	private List<HistoricoProdutoSegurado> produtos;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contato == null) ? 0 : contato.hashCode());
		result = prime * result + ((cpfCnpj == null) ? 0 : cpfCnpj.hashCode());
		result = prime * result
				+ ((dataNascimento == null) ? 0 : dataNascimento.hashCode());
		result = prime * result
				+ ((digitoCpfCnpj == null) ? 0 : digitoCpfCnpj.hashCode());
		result = prime * result
				+ ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((filial == null) ? 0 : filial.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((sexo == null) ? 0 : sexo.hashCode());
		result = prime * result
				+ ((tipoPessoa == null) ? 0 : tipoPessoa.hashCode());
		result = prime * result
				+ ((titularidade == null) ? 0 : titularidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoPessoa other = (HistoricoPessoa) obj;
		if (contato == null) {
			if (other.contato != null)
				return false;
		} else if (!contato.equals(other.contato))
			return false;
		if (cpfCnpj == null) {
			if (other.cpfCnpj != null)
				return false;
		} else if (!cpfCnpj.equals(other.cpfCnpj))
			return false;
		if (dataNascimento == null) {
			if (other.dataNascimento != null)
				return false;
		} else if (!dataNascimento.equals(other.dataNascimento))
			return false;
		if (digitoCpfCnpj == null) {
			if (other.digitoCpfCnpj != null)
				return false;
		} else if (!digitoCpfCnpj.equals(other.digitoCpfCnpj))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (filial == null) {
			if (other.filial != null)
				return false;
		} else if (!filial.equals(other.filial))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (sexo == null) {
			if (other.sexo != null)
				return false;
		} else if (!sexo.equals(other.sexo))
			return false;
		if (tipoPessoa == null) {
			if (other.tipoPessoa != null)
				return false;
		} else if (!tipoPessoa.equals(other.tipoPessoa))
			return false;
		if (titularidade == null) {
			if (other.titularidade != null)
				return false;
		} else if (!titularidade.equals(other.titularidade))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getDigitoCpfCnpj() {
		return digitoCpfCnpj;
	}

	public void setDigitoCpfCnpj(String digitoCpfCnpj) {
		this.digitoCpfCnpj = digitoCpfCnpj;
	}

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

	public String getTitularidade() {
		return titularidade;
	}

	public void setTitularidade(String titularidade) {
		this.titularidade = titularidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public HistoricoEndereco getEndereco() {
		return endereco;
	}

	@Embedded
	public void setEndereco(HistoricoEndereco endereco) {
		this.endereco = endereco;
	}

	public HistoricoContato getContato() {
		return contato;
	}

	@Embedded
	public void setContato(HistoricoContato contato) {
		this.contato = contato;
	}

	public List<HistoricoProdutoSegurado> getProdutos() {
		return produtos;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	@Embedded
	public void setProdutos(List<HistoricoProdutoSegurado> produtos) {
		this.produtos = produtos;
	}

	@Override
	public String toString() {
		return "HistoricoPessoa [id=" + id + ", tipoPessoa=" + tipoPessoa
				+ ", cpfCnpj=" + cpfCnpj + ", digitoCpfCnpj=" + digitoCpfCnpj
				+ ", filial=" + filial + ", titularidade=" + titularidade
				+ ", nome=" + nome + ", dataNascimento=" + dataNascimento
				+ ", sexo=" + sexo + ", endereco=" + endereco + ", contato="
				+ contato + ", produtos=" + produtos + "]";
	}
	
	
	
}