package br.com.twg.pep.report;

import java.util.List;

public class PessoaReport {

	private String nome;
	private String cpfCnpj;
	private String digitoCpfCnpj;
	private String tipoPessoa;
	private String codigoOrgao;
	private String nomeOrgao;
	private String codigoCargo;
	private String nomeCargo;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String municipio;
	private String uf;
	private String cep;
	private String ddd;
	private String telefone;	
	private String ramal;
	private List<ProdutoSeguradoReport> produtos;
	private List<MandatoReport> mandatos;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bairro == null) ? 0 : bairro.hashCode());
		result = prime * result + ((cep == null) ? 0 : cep.hashCode());
		result = prime * result
				+ ((codigoCargo == null) ? 0 : codigoCargo.hashCode());
		result = prime * result
				+ ((codigoOrgao == null) ? 0 : codigoOrgao.hashCode());
		result = prime * result
				+ ((complemento == null) ? 0 : complemento.hashCode());
		result = prime * result + ((cpfCnpj == null) ? 0 : cpfCnpj.hashCode());
		result = prime * result + ((ddd == null) ? 0 : ddd.hashCode());
		result = prime * result
				+ ((digitoCpfCnpj == null) ? 0 : digitoCpfCnpj.hashCode());
		result = prime * result
				+ ((logradouro == null) ? 0 : logradouro.hashCode());
		result = prime * result
				+ ((municipio == null) ? 0 : municipio.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((nomeCargo == null) ? 0 : nomeCargo.hashCode());
		result = prime * result
				+ ((nomeOrgao == null) ? 0 : nomeOrgao.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		result = prime * result + ((ramal == null) ? 0 : ramal.hashCode());
		result = prime * result
				+ ((telefone == null) ? 0 : telefone.hashCode());
		result = prime * result
				+ ((tipoPessoa == null) ? 0 : tipoPessoa.hashCode());
		result = prime * result + ((uf == null) ? 0 : uf.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaReport other = (PessoaReport) obj;
		if (bairro == null) {
			if (other.bairro != null)
				return false;
		} else if (!bairro.equals(other.bairro))
			return false;
		if (cep == null) {
			if (other.cep != null)
				return false;
		} else if (!cep.equals(other.cep))
			return false;
		if (codigoCargo == null) {
			if (other.codigoCargo != null)
				return false;
		} else if (!codigoCargo.equals(other.codigoCargo))
			return false;
		if (codigoOrgao == null) {
			if (other.codigoOrgao != null)
				return false;
		} else if (!codigoOrgao.equals(other.codigoOrgao))
			return false;
		if (complemento == null) {
			if (other.complemento != null)
				return false;
		} else if (!complemento.equals(other.complemento))
			return false;
		if (cpfCnpj == null) {
			if (other.cpfCnpj != null)
				return false;
		} else if (!cpfCnpj.equals(other.cpfCnpj))
			return false;
		if (ddd == null) {
			if (other.ddd != null)
				return false;
		} else if (!ddd.equals(other.ddd))
			return false;
		if (digitoCpfCnpj == null) {
			if (other.digitoCpfCnpj != null)
				return false;
		} else if (!digitoCpfCnpj.equals(other.digitoCpfCnpj))
			return false;
		if (logradouro == null) {
			if (other.logradouro != null)
				return false;
		} else if (!logradouro.equals(other.logradouro))
			return false;
		if (municipio == null) {
			if (other.municipio != null)
				return false;
		} else if (!municipio.equals(other.municipio))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (nomeCargo == null) {
			if (other.nomeCargo != null)
				return false;
		} else if (!nomeCargo.equals(other.nomeCargo))
			return false;
		if (nomeOrgao == null) {
			if (other.nomeOrgao != null)
				return false;
		} else if (!nomeOrgao.equals(other.nomeOrgao))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (ramal == null) {
			if (other.ramal != null)
				return false;
		} else if (!ramal.equals(other.ramal))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		if (tipoPessoa == null) {
			if (other.tipoPessoa != null)
				return false;
		} else if (!tipoPessoa.equals(other.tipoPessoa))
			return false;
		if (uf == null) {
			if (other.uf != null)
				return false;
		} else if (!uf.equals(other.uf))
			return false;
		return true;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public String getDigitoCpfCnpj() {
		return digitoCpfCnpj;
	}
	public void setDigitoCpfCnpj(String digitoCpfCnpj) {
		this.digitoCpfCnpj = digitoCpfCnpj;
	}
	public String getTipoPessoa() {
		return tipoPessoa;
	}
	public void setTipoPessoa(String tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}
	public String getCodigoOrgao() {
		return codigoOrgao;
	}
	public void setCodigoOrgao(String codigoOrgao) {
		this.codigoOrgao = codigoOrgao;
	}
	public String getCodigoCargo() {
		return codigoCargo;
	}
	public void setCodigoCargo(String codigoCargo) {
		this.codigoCargo = codigoCargo;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getDdd() {
		return ddd;
	}
	public void setDdd(String ddd) {
		this.ddd = ddd;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getRamal() {
		return ramal;
	}
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}
	public List<ProdutoSeguradoReport> getProdutos() {
		return produtos;
	}
	public void setProdutos(List<ProdutoSeguradoReport> produtos) {
		this.produtos = produtos;
	}
	public String getNomeOrgao() {
		return nomeOrgao;
	}
	public void setNomeOrgao(String nomeOrgao) {
		this.nomeOrgao = nomeOrgao;
	}
	public String getNomeCargo() {
		return nomeCargo;
	}
	public void setNomeCargo(String nomeCargo) {
		this.nomeCargo = nomeCargo;
	}
	
	public List<MandatoReport> getMandatos() {
		return mandatos;
	}
	public void setMandatos(List<MandatoReport> mandatos) {
		this.mandatos = mandatos;
	}
	@Override
	public String toString() {
		return "PessoaReport [nome=" + nome + ", cpfCnpj=" + cpfCnpj
				+ ", digitoCpfCnpj=" + digitoCpfCnpj + ", tipoPessoa="
				+ tipoPessoa + ", codigoOrgao=" + codigoOrgao + ", nomeOrgao="
				+ nomeOrgao + ", codigoCargo=" + codigoCargo + ", nomeCargo="
				+ nomeCargo + ", logradouro=" + logradouro + ", numero="
				+ numero + ", complemento=" + complemento + ", bairro="
				+ bairro + ", municipio=" + municipio + ", uf=" + uf + ", cep="
				+ cep + ", ddd=" + ddd + ", telefone=" + telefone + ", ramal="
				+ ramal + ", produtos=" + produtos + ", mandatos=" + mandatos
				+ "]";
	}
}