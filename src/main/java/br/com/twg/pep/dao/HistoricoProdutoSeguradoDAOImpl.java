package br.com.twg.pep.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;

import br.com.twg.pep.entity.HistoricoPessoa;
import br.com.twg.pep.entity.HistoricoProdutoSegurado;
import br.com.twg.pep.util.DateUtil;

public class HistoricoProdutoSeguradoDAOImpl implements HistoricoProdutoSeguradoDAO {

	@PersistenceUnit(unitName="PEPPU")
	private EntityManagerFactory entityManagerFactory;
	
	@PersistenceUnit(unitName="WLSPU")
	private EntityManagerFactory entityManagerFactoryWLS;
	
	@Autowired
	private CancelamentoDAO cancelamentoDao;

	@Override
	public List<HistoricoProdutoSegurado> buscaProdutoSeguradoPor(HistoricoPessoa pessoa, Date dataCriacao) {
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
    	Query query = entityManager.createQuery("select ps from HistoricoProdutoSegurado ps where ps.pessoa = :pessoa and year(ps.dataCriacao) = :ano and month(ps.dataCriacao) = :mes "); //colocar a data de criação
        query.setParameter("pessoa", pessoa);
        query.setParameter("ano", DateUtil.ano(dataCriacao));
        query.setParameter("mes", DateUtil.mes(dataCriacao));
        List<HistoricoProdutoSegurado> listaResultados = (List<HistoricoProdutoSegurado>)query.getResultList();
        return listaResultados;
	}
}