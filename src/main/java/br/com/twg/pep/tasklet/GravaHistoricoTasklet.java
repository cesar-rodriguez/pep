package br.com.twg.pep.tasklet;

import java.util.Date;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.twg.pep.dao.CargoDAO;
import br.com.twg.pep.dao.OrgaoDAO;
import br.com.twg.pep.dao.PessoaDAO;
import br.com.twg.pep.dao.PessoaTitularDependenteDAO;

public class GravaHistoricoTasklet implements Tasklet {

	@Autowired
	private OrgaoDAO orgaoDao;
	
	@Autowired
	private CargoDAO cargoDao;
	
	@Autowired
	private PessoaTitularDependenteDAO pessoaTitularDependenteDao;

	@Autowired
	private PessoaDAO pessoaDao;
	
	private Date startTime;
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

		orgaoDao.insereDadosHistoricos(startTime);
		cargoDao.insereDadosHistoricos(startTime);
		pessoaDao.insereDadosHistoricos(startTime);
		pessoaTitularDependenteDao.insereDadosHistoricos(startTime);

		return RepeatStatus.FINISHED;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
}