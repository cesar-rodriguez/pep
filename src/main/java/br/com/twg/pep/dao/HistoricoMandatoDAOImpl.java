package br.com.twg.pep.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.orm.jpa.EntityManagerFactoryUtils;

import br.com.twg.pep.entity.HistoricoMandato;
import br.com.twg.pep.entity.HistoricoPessoa;
import br.com.twg.pep.entity.Pessoa;
import br.com.twg.pep.util.DateUtil;

public class HistoricoMandatoDAOImpl implements HistoricoMandatoDAO {

	@PersistenceUnit(unitName = "PEPPU")
	private EntityManagerFactory entityManagerFactory;

	private Date startTime;

	private void estruturaUpdate(String strUpdate) {
		EntityManager entityManager = EntityManagerFactoryUtils
				.getTransactionalEntityManager(entityManagerFactory);
		Query query = entityManager.createNativeQuery(strUpdate);
		query.executeUpdate();
	}

	@Override
	public void removeHistoricoMandato() {
		String schema = (String) entityManagerFactory.getProperties().get(
				"hibernate.default_schema");
		estruturaUpdate("DELETE FROM " + schema
				+ ".HISTORICO_MANDATO HM WHERE HM.DATA_CRIACAO = '"
				+ DateUtil.formataDataHistorico(startTime) + "'");
	}

	
	
	@Override
	public List<HistoricoMandato> buscaHistoricoMandatoPor(Pessoa pessoa) {
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
		String schema = (String) entityManager.getEntityManagerFactory().getProperties().get("hibernate.default_schema");
		Query query = entityManager.createNativeQuery(nativeQueryBuscaPorPessoa(schema), HistoricoMandato.class);
		query.setParameter("pessoaId", pessoa.getId());
		query.setParameter("tipoPessoa", pessoa.getTipoPessoa());
		query.setParameter("cpfCnpj", pessoa.getCpfCnpj());
		query.setParameter("digitoCpfCnpj", pessoa.getDigitoCpfCnpj());
		query.setParameter("filial", pessoa.getFilial());
		return query.getResultList();
	}
	
	@Override
	public List<HistoricoMandato> buscaHistoricoMandatoPor(HistoricoPessoa pessoa, Date dataCriacao) {
		EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(entityManagerFactory);
		String schema = (String) entityManager.getEntityManagerFactory().getProperties().get("hibernate.default_schema");
		Query query = entityManager.createNativeQuery(nativeQueryBuscaPorPessoaEDataCriacao(schema), HistoricoMandato.class);
		query.setParameter("pessoaId", pessoa.getId());
		query.setParameter("tipoPessoa", pessoa.getTipoPessoa());
		query.setParameter("cpfCnpj", pessoa.getCpfCnpj());
		query.setParameter("digitoCpfCnpj", pessoa.getDigitoCpfCnpj());
		query.setParameter("filial", pessoa.getFilial());
		query.setParameter("dataCriacao", DateUtil.ultimoDiaMes(dataCriacao));
		return query.getResultList();
	}
	
	private String nativeQueryBuscaPorPessoaEDataCriacao(String schema) {

		StringBuilder query = new StringBuilder();

		query.append(" SELECT  ")
				.append(" HMA.PESSOA_ID, ")
				.append(" HMA.TIPO_PESSOA, ")
				.append(" HMA.CPF_CNPJ, ")
				.append(" HMA.DIGITO_CPF_CNPJ, ")
				.append(" HMA.FILIAL, ")
				.append(" HMA.ORGAO_ID, ")
				.append(" HMA.CARGO_ID, ")
				.append(" MAX(HMA.DATA_NOMEACAO) AS DATA_NOMEACAO, ")
				.append(" MAX(HMA.DATA_EXONERACAO) AS DATA_EXONERACAO, ")
				.append(" MAX(HMA.DATA_CRIACAO) AS DATA_CRIACAO ")
				.append(" FROM "+schema+".HISTORICO_MANDATO HMA ")
				.append(" INNER JOIN "+schema+".CARGO CAR ON (CAR.ID = HMA.CARGO_ID) ")
				.append(" INNER JOIN "+schema+".ORGAO ORG ON (ORG.ID = HMA.ORGAO_ID) ")
				.append(" INNER JOIN "+schema+".PESSOA PES ON (HMA.PESSOA_ID = PES.ID AND  HMA.TIPO_PESSOA = PES.TIPO_PESSOA AND HMA.CPF_CNPJ = PES.CPF_CNPJ AND HMA.DIGITO_CPF_CNPJ = PES.DIGITO_CPF_CNPJ  AND  HMA.FILIAL  = PES.FILIAL) ")
				.append(" WHERE HMA.DATA_CRIACAO IN ( ")
				.append(" SELECT  ")
				.append(" HMT.DATA_CRIACAO ")
				.append(" FROM "+schema+".HISTORICO_MANDATO HMT ")
				.append(" WHERE HMT.PESSOA_ID = PES.ID  ")
				.append(" AND  HMT.TIPO_PESSOA = PES.TIPO_PESSOA ")
				.append(" AND HMT.CPF_CNPJ = PES.CPF_CNPJ ")
				.append(" AND HMT.DIGITO_CPF_CNPJ = PES.DIGITO_CPF_CNPJ ")
				.append(" AND  HMT.FILIAL = PES.FILIAL ")
				.append(" AND DATE(HMA.DATA_CRIACAO) <= :dataCriacao ")
				.append(" ) ")
				.append(" AND HMA.PESSOA_ID = :pessoaId  ")
				.append(" AND HMA.TIPO_PESSOA = :tipoPessoa ")
				.append(" AND HMA.CPF_CNPJ = :cpfCnpj ")
				.append(" AND HMA.DIGITO_CPF_CNPJ = :digitoCpfCnpj ")
				.append(" AND HMA.FILIAL = :filial ")
				.append(" GROUP BY  HMA.PESSOA_ID , ")
				.append(" HMA.TIPO_PESSOA, HMA.CPF_CNPJ, ")
				.append(" HMA.DIGITO_CPF_CNPJ, HMA.FILIAL, ")
				.append(" HMA.ORGAO_ID, ").append(" HMA.CARGO_ID ");
			
		return query.toString();
	}

	private String nativeQueryBuscaPorPessoa(String schema) {

		StringBuilder query = new StringBuilder();

		query.append(" SELECT  ")
				.append(" HMA.PESSOA_ID,")
				.append(" HMA.TIPO_PESSOA,")
				.append(" HMA.CPF_CNPJ,")
				.append(" HMA.DIGITO_CPF_CNPJ,")
				.append(" HMA.FILIAL,")
				.append(" HMA.ORGAO_ID,")
				.append(" HMA.CARGO_ID,")
				.append(" HMA.DATA_NOMEACAO,")
				.append(" HMA.DATA_EXONERACAO,")
				.append(" HMA.DATA_CRIACAO")
				.append(" FROM "+schema+".HISTORICO_MANDATO HMA")
				.append(" INNER JOIN "+schema+".CARGO CAR ON (CAR.ID = HMA.CARGO_ID)")
				.append(" INNER JOIN "+schema+".ORGAO ORG ON (ORG.ID = HMA.ORGAO_ID)")
				.append(" INNER JOIN "+schema+".PESSOA PES ON (HMA.PESSOA_ID = PES.ID AND  HMA.TIPO_PESSOA = PES.TIPO_PESSOA AND HMA.CPF_CNPJ = PES.CPF_CNPJ AND HMA.DIGITO_CPF_CNPJ = PES.DIGITO_CPF_CNPJ  AND  HMA.FILIAL  = PES.FILIAL)")
				.append(" WHERE HMA.DATA_CRIACAO =   (SELECT MAX(HMT.DATA_CRIACAO) FROM "+schema+".HISTORICO_MANDATO HMT)")
				.append(" AND PES.ID = :pessoaId ")
				.append(" AND PES.TIPO_PESSOA = :tipoPessoa ")
				.append(" AND PES.CPF_CNPJ = :cpfCnpj ")
				.append(" AND PES.DIGITO_CPF_CNPJ = :digitoCpfCnpj ")
				.append(" AND PES.FILIAL = :filial ")
				.append(" ORDER BY  HMA.PESSOA_ID ,")
				.append(" HMA.TIPO_PESSOA,").append(" HMA.CPF_CNPJ,")
				.append(" HMA.DIGITO_CPF_CNPJ,").append(" HMA.FILIAL,")
				.append(" HMA.DATA_NOMEACAO DESC");
		
		return query.toString();
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
}
