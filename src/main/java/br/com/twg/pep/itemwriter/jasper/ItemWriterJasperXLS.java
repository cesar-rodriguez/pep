package br.com.twg.pep.itemwriter.jasper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.springframework.core.io.Resource;

import br.com.twg.pep.item.ItemWriterJasper;

public class ItemWriterJasperXLS implements ItemWriterJasper{

	@Override
	public void exportaArquivoJasper(final JasperPrint print, final Resource resourceExport) throws IOException, JRException {
		
		File arquivoExportado =  resourceExport.getFile();
		
		JRXlsExporter xls = new JRXlsExporter();  

		OutputStream outputfile = new FileOutputStream(arquivoExportado.getAbsolutePath());
		xls.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
		xls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputfile);
		xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
		xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		xls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		xls.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, Integer.decode("65000"));
		xls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
		xls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		xls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);

		xls.exportReport();	
		outputfile.close();
	}
}