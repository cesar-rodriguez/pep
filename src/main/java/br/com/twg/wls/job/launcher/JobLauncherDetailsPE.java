package br.com.twg.wls.job.launcher;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class JobLauncherDetailsPE extends QuartzJobBean {

	private static Log log = LogFactory.getLog(JobLauncherDetailsPE.class);
	
	private static final String JOB_NAME = "jobName";

	private JobLocator jobLocator;

	private JobLauncher jobLauncher;
	
	private JobExplorer jobExplorer;
	
//	@Value("file:C:/Planilha_Lei 13 170.xlsx")
//	private Resource excel;

	@SuppressWarnings("unchecked")
	protected void executeInternal(JobExecutionContext context) {
		Map<String, Object> jobDataMap = context.getMergedJobDataMap();
		String jobName = (String) jobDataMap.get(JOB_NAME);
		log.info("Quartz trigger firing with Spring Batch jobName="+jobName);
		try {
			JobParameters jobParameters = getJobParametersFromJobMap(jobDataMap);
			jobLauncher.run(jobLocator.getJob(jobName), jobParameters);
		}
		catch (JobExecutionException e) {
			log.error("Could not execute job.", e);
		}catch (IOException e){
			log.error("Could not execute job.", e);
		}
	}

	private JobParameters getJobParametersFromJobMap(Map<String, Object> jobDataMap) throws IOException {
		JobParametersBuilder builder = new JobParametersBuilder();
		builder.addLong("param", new Date().getTime());
//		builder.addString("listName", extrairNomesPlanilha());
		return builder.toJobParameters();
	}
	
//	private String extrairNomesPlanilha() throws IOException{
//		
//		File file = new File("R:/Projetos/Juridico/Planilha_Lei 13_170_fake.xlsx");
//		FileInputStream fis = new FileInputStream(file);
//		List<String> list = new ArrayList<String>();
//		XSSFSheet sheet = UtilsPoiExcel.readXLSXAllSheets(fis);
//		
//		XSSFRow row;
//		XSSFCell cell;
//		Iterator rows = sheet.rowIterator();
//		while (rows.hasNext()) {
//			row = (XSSFRow) rows.next();
//			Iterator cells = row.cellIterator();
//			int c = 0;
//			while (cells.hasNext()) {
//				cell = (XSSFCell) cells.next();				
//				if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
//					list.add(cell.getStringCellValue() + " ");
//				}
//				// A linha abaixo é apenas para fins de esclarecimento
//				if(c == 0){
//					c++;
//					break;
//				}
//			}
//		}
//		return StringUtils.collectionToCommaDelimitedString(list);
//	}
	
	public void setJobLocator(JobLocator jobLocator) {
		this.jobLocator = jobLocator;
	}

	public void setJobLauncher(JobLauncher jobLauncher) {
		this.jobLauncher = jobLauncher;
	}

	public void setJobExplorer(JobExplorer jobExplorer) {
		this.jobExplorer = jobExplorer;
	}
	
}